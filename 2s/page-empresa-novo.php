<?php
/**
 * Template Name: Empresa
 *
 * @package 2s
 * @subpackage 2s
 * @since 2s 0.1v
 */


 get_header(); ?>
 <div class="pagina-nova container-fluid pagina-empresa">
   <?php if (have_posts()): ?>
   <?php while ( have_posts() ) : the_post();
   ?>
   <div class="header3">
     <h1>
       <?php the_title(); ?>
     </h1>
   </div>
   <div class="container-fluid">
     <div class="row">
       <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');  ?>
       <div class="col-sm-12 foto-header wow fadeIn" style="background-image:url(<?php echo $featured_img_url; ?>)">

       </div>
     </div>
   </div>
   <!--=== Container Part ===-->
   <div class="container">
       <div class="row">
           <div class="col-md-12  wow fadeInDown">



                   <?php the_content(); ?>



           </div>
           <div class="row">
             <div class="col-sm-8">
               <div class="headline">
                 <h2>Nosso Foco</h2>
               </div>
               <ul class="lista-pontos">
                 <li class="wow fadeInRight">Apresentar soluções tecnologicamente inovadoras;</li>
                 <li class="wow fadeInRight">Otimizar a operação da área de TI, concentrando-se no aumento da produtividade e na redução de custos para os clientes</li>
                 <li class="wow fadeInRight">Promover o alinhamento dos objetivos de negócio com os da Área de TI;</li>
                 <li class="wow fadeInRight">Garantir alta disponibilidade e contingência para o negócio;</li>
                 <li class="wow fadeInRight">Reduzir riscos operacionais e promover ações de segurança;</li>
                 <li class="wow fadeInRight">Adequar os processos de negócios às normas internacionais;</li>
                 <li class="wow fadeInRight">Viabilizar de projetos de TI com soluções financeiras criativas</li>
               </ul>
               <div class="headline">
                 <h2>Conceitos</h2>
               </div>
               <ul class="lista">
                 <li class="wow fadeInRight">Todas as empresas investem em tecnologia, mas só as líderes tiram proveito do investimento realizado.</li>
                 <li class="wow fadeInRight">Tecnologia pode tornar sua empresa mais lucrativa.</li>
                 <li class="wow fadeInRight">Entendendo os negócios de nossos clientes podemos transformá-los com o uso da tecnologia</li>
                 <li class="wow fadeInRight">Focar em seus negócios e terceirizar processos de tecnologia pode trazer mais criatividade para o negócio e reduzir os custos da operação.</li>
                 <li class="wow fadeInRight">Utilizando nossas melhores práticas em TI pode aumentar a produtividade do seu negócio</li>
                 <li class="wow fadeInRight">Alinhar os objetivos do negócio com os objetivos de TI significa transformar a área de Tecnologia da Informação em estratégica para a sua empresa</li>
               </ul>
               <div class="headline">
                 <h2>Nossa Missão</h2>
               </div>
               <h2 class="wow zoomIn"><span style="color: #2977BA">Aprimorar</span> os negócios dos <span style="color: #2977BA">nossos clientes</span> com tecnologia</h2>

               <div class="headline">
                 <h2>Nossos Valores</h2>
               </div>

               <div class="empresa-valores">
                 <img src="<?php echo get_template_directory_uri(); ?>/assets/img/empresa-valores.png" class="img-responsive wow fadeIn" alt="Valores 2s" style="margin: 0 auto;">
               </div>

               <div class="container contato">
                 <div class="row">
                   <div class="col-sm-12">
                     <div class="headline">
                       <h2>Cadastre-se para ficar atualizado com a 2S</h2>
                     </div>
                     <div class="formulario">
                       <?php echo do_shortcode('[contact-form-7 id="948" title="Cadastro Verticais"]'); ?>
                     </div>
                   </div>
                 </div>
               </div>

             </div>
             <!-- End col-9 -->
             <div class="col-md-offset-1 col-md-3">
                 <?php get_sidebar(); ?>
             </div><!-- End col-3 -->
           </div>



       </div> <!-- End row-->
   </div>

   </div>
   <!--=== End Container Part ===-->
   <?php endwhile;?>
   <?php endif ?>
 </div>



<?php get_footer(); ?>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/plugins/wowScroll.js" charset="utf-8"></script>
<script>
new WOW().init();
</script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
        StyleSwitcher.initStyleSwitcher();
    });
</script>
