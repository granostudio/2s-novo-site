<?php get_header(); ?>

    <?php
    /*
    <div id="layerslider" style="width: 100%; height: 350px;">

             <!-- Iot -->
            <div class="ls-slide" data-ls="slidedelay:4500; transition2d:5;">
                <img src="/wp-content/themes/2s/assets/img/sliders/layer/bg-5-frentes.jpg"  class="ls-bg" alt="Slide background"/>

                <a href="/solucoes/iot" class="ls-s-1" style=" top:80px; slidedirection : top; slideoutdirection : bottom; durationin : 1500; durationout : 3500;">
                    <img src="/wp-content/themes/2s/assets/img/mockup/it-coisas.png" class="img-responsive" alt="Mobilidade">
                </a>

                <a href="/solucoes/seguranca" class="ls-s-1" style=" top:30px; left: 190px; slidedirection : bottom; slideoutdirection : top; durationin : 2000; durationout : 4000;">
                    <img src="/wp-content/themes/2s/assets/img/mockup/seguranca.png" class="img-responsive" alt="Colaboração">
                </a>

                <img src="/wp-content/themes/2s/assets/img/mockup/arrows-1.png" class="img-responsive ls-s-1" style=" top:150px; left: 170px; slidedirection : fade; slideoutdirection : fade; durationin : 2500; durationout : 50;" alt="Colaboração">

                <a href="/solucoes/colaboracao" class="ls-s-1" style=" top:80px; left: 390px; slidedirection : top; slideoutdirection : bottom; durationin : 2500; durationout : 4500;">
                    <img src="/wp-content/themes/2s/assets/img/mockup/colaboracao.png" class="img-responsive" alt="Internet das Coisas">
                </a>

                <img src="/wp-content/themes/2s/assets/img/mockup/arrows-2.png" class="img-responsive ls-s-1" style=" top:150px; left: 370px; slidedirection : fade; slideoutdirection : fade; durationin : 3500; durationout : 50;" alt="Colaboração">

                <a href="/solucoes/mobilidade" class="ls-s-1" style=" top:30px; left: 580px; slidedirection : bottom; slideoutdirection : top; durationin : 3000; durationout : 5000;">
                    <img src="/wp-content/themes/2s/assets/img/mockup/mobilidade.png" class="img-responsive" alt="Data Center">
                </a>


                <img src="/wp-content/themes/2s/assets/img/mockup/arrows-1.png" class="img-responsive ls-s-1" style=" top:150px; left: 560px; slidedirection : fade; slideoutdirection : fade; durationin : 4500; durationout : 50;" alt="Colaboração">

                <a href="/solucoes/datacenter" class="ls-s-1" style=" top:80px; left: 780px; slidedirection : top; slideoutdirection : bottom; durationin : 3500; durationout : 5500;">
                    <img src="/wp-content/themes/2s/assets/img/mockup/data-center.png" class="img-responsive" alt="Segurança">
                </a>

                <img src="/wp-content/themes/2s/assets/img/mockup/arrows-2.png" class="img-responsive ls-s-1" style=" top:150px; left: 760px; slidedirection : fade; slideoutdirection : fade; durationin : 5500; durationout : 50;" alt="Colaboração">

            </div>
            <!-- Iot -->

            <!-- 2S -->
            <div class="ls-slide" data-ls="slidedelay:4500; transition2d:5;">
                <img src="/wp-content/themes/2s/assets/img/sliders/layer/slide-bg-2.jpg" class="ls-bg" alt="Slide background">

                <span class="ls-s-1" style=" color: #fff; line-height:45px; font-weight: 200; font-size: 29px; top:100px; left: 290px; slidedirection : top; slideoutdirection : bottom; durationin : 1000; durationout : 1000; ">
                    Transforme sua empresa <br> com tecnologia.
                </span>

                <a class="btn-u btn-u ls-s-1" href="empresa.html" style=" padding: 9px 20px; font-size:25px; top:200px; left: 290px; slidedirection : bottom; slideoutdirection : bottom; durationin : 2000; durationout : 2000; ">
                    Conheça a 2S
                </a>

                <img src="/wp-content/themes/2s/assets/img/mockup/point.png" alt="Slider Image" class="ls-s-1" style=" top:0px; slidedirection : bottom; slideoutdirection : top; durationin : 1200; durationout : 3000; ">
            </div>
            <!-- 2S -->

            <!-- Cisco 2 -->
            <div class="ls-slide" data-ls="slidedelay:4500; transition2d:5;">
                <img src="/wp-content/themes/2s/assets/img/sliders/layer/slide-bg-10.jpg" class="ls-bg" alt="Slide background">

                <span class="ls-s-1" style=" color: #fff; line-height:45px; font-weight: 200; font-size: 29px; top:80px; left: 50px; slidedirection : top; slideoutdirection : bottom; durationin : 1000; durationout : 1000; ">
                    2S é o 1º parceiro CISCO a obter<br> a certificação de segurança no Brasil.
                </span>

                <a class="btn-u ls-s-1" href="/parcerias" style=" padding: 9px 20px; font-size:25px; top:180px; left: 50px; slidedirection : bottom; slideoutdirection : bottom; durationin : 2000; durationout : 2000; ">
                    Saiba Mais
                </a>

                <img src="/wp-content/themes/2s/assets/img/mockup/cisco_gold.png" alt="Slider Image" class="ls-s-1" style=" top:30px; left: 630px; slidedirection : right; slideoutdirection : bottom; durationin : 3000; durationout : 3000; ">
            </div>
            <!-- Cisco 2 -->

            <!-- Nossas Solucoes -->
            <div class="ls-slide" data-ls="slidedelay:4500; transition2d:5;">
                <img src="/wp-content/themes/2s/assets/img/sliders/layer/slide-bg-1.jpg"  class="ls-bg" alt="Slide background">

                <span class="ls-s-1" style=" color: #fff; line-height:45px; font-weight: 200; font-size: 24px; top:10px; left: 50px; slidedirection : top; slideoutdirection : bottom; durationin : 1000; durationout : 1000; ">
                    Nossos Serviços
                </span>

                <span class="ls-s-2" style="background: #424a59;padding: 5px 15px !important;border-left: 4px solid #146bac;  color: #fff; font-weight: 200; font-size: 16px; top:60px; left: 50px; slidedirection : top; slideoutdirection : bottom; durationin : 2400; durationout : 500; ">
                    <i class="fa fa-chevron-circle-right"></i> Manutenção
                </span>

                <span class="ls-s-2" style="background: #424a59;padding: 5px 15px !important;border-left: 4px solid #146bac;  color: #fff; font-weight: 200; font-size: 16px; top:100px; left: 50px; slidedirection : top; slideoutdirection : bottom; durationin : 2200; durationout : 500; ">
                    <i class="fa fa-chevron-circle-right"></i> Requisição de Serviços
                </span>

                <span class="ls-s-2" style="background: #424a59;padding: 5px 15px !important;border-left: 4px solid #146bac;  color: #fff; font-weight: 200; font-size: 16px; top:140px; left: 50px; slidedirection : top; slideoutdirection : bottom; durationin : 2000; durationout : 500; ">
                   <i class="fa fa-chevron-circle-right"></i> Monitoramento
                </span>

                <span class="ls-s-2" style="background: #424a59;padding: 5px 15px !important;border-left: 4px solid #146bac;  color: #fff; font-weight: 200; font-size: 16px; top:180px; left: 50px; slidedirection : top; slideoutdirection : bottom; durationin : 1800; durationout : 500; ">
                    <i class="fa fa-chevron-circle-right"></i> Gerenciamento Remoto e Onsite
                </span>

                <span class="ls-s-2" style="background: #424a59;padding: 5px 15px !important;border-left: 4px solid #146bac; color: #fff; font-weight: 200; font-size: 16px; top:220px; left: 50px; slidedirection : top; slideoutdirection : bottom; durationin : 1500; durationout : 500; ">
                    <i class="fa fa-chevron-circle-right"></i> Meraki as a Service (Nuvem)
                </span>

                <span class="ls-s-2" style="background: #424a59;padding: 5px 15px !important;border-left: 4px solid #146bac; color: #fff; font-weight: 200; font-size: 16px; top:260px; left: 50px; slidedirection : top; slideoutdirection : bottom; durationin : 1500; durationout : 500; ">
                    <i class="fa fa-chevron-circle-right"></i> Prime as a Service (Nuvem)
                </span>

                <a class="btn-u ls-s-1" href="servicos" style=" padding: 9px 20px; font-size:18px; top:300px; left: 50px; slidedirection : bottom; slideoutdirection : bottom; durationin : 2200; durationout : 2000; ">
                    Saiba Mais
                </a>

                <img src="/wp-content/themes/2s/assets/img/mockup/slider-man.png" alt="Slider Image" class="ls-s-1" style=" top:70px; left: 500px; slidedirection : bottom; slideoutdirection : bottom; durationin : 1500; durationout : 1500; ">
            </div>
            <!-- Nossas Solucoes -->
        </div>
    */
    ?>

    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("sidebar-9") ) : ?>
    <?php get_sidebar("sidebar-9"); ?>
    <?php endif; ?>


    <div class="container">

            <!-- <div class="headline">
              <h2 style="font-size:24px">DESTAQUES DA SEMANA</h2>
            </div> -->
            <div class="margin-bottom-60"></div>

            <?php
            $query= null;
            $query = new WP_Query(
            array(
            'post_type' => 'destaque',
            'post_per_page' => 6)
            );
            ?>

            <div class="row news-v2 margin-bottom-60">

            <?php
            if ( $query->have_posts() ) {
                while ( $query->have_posts() ) {
                $query->the_post();
                $custom             = get_post_custom($post->ID);
                $destaque_descricao    =   $custom["destaque_descricao"][0];
                $destaque_url          =   $custom["destaque_link"][0];
                $destaque_foto         =   $custom["destaque_foto"][0];
                // print_r($custom);

            ?>

                <div class="col-md-4" style="margin-bottom:20px;">
                    <div class="news-v2-badge">
                        <a href="<?php echo $destaque_url; ?>"><img class="img-responsive" src="<?php echo $destaque_foto; ?>" alt=""></a>
                        <p>
                            <img class="img-responsive" src="/wp-content/themes/2s/assets/img/simbol-2s-box.png" alt="">
                        </p>
                    </div>
                    <div class="news-v2-desc">
                        <h3><a href="<?php echo $destaque_url; ?>"><?php echo $destaque_descricao; ?></strong></a></h3>
                    </div>
                </div>

            <?php } } ?>
            </div>


            <!-- <div class="headline">
              <h2 style="font-size:24px">ÚLTIMOS POSTS</h2>
            </div> -->

            <!-- <div class="row news-v2 margin-bottom-60">

              <?php
                $args = array(
                    'posts_per_page' => 9,
                    'offset'=> 0,
                    'post_type' => 'post',
                    'post_status' => 'publish');
                $myposts = get_posts( $args );
                foreach($myposts as $post) : setup_postdata($post);
              ?> -->

                <!-- .post -->

                  <!-- <div class="col-md-4" style="margin-bottom:20px;">
                      <div class="news-v2-badge">
                          <a href="<?php the_permalink(); ?>">
                            <div class="img-responsive">
                              <?php if (has_post_thumbnail()): ?>
                                <?php the_post_thumbnail( 'blog-thumb' ); ?>
                              <?php endif ?>
                            </div>
                          </a>
                          <p>
                              <img class="img-responsive" src="/wp-content/themes/2s/assets/img/simbol-2s-box.png" alt="">
                          </p>
                      </div>
                      <div class="news-v2-desc">
                        <h2 class="entry-title" style="font-size: 19px;text-align: left;">
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a>
                        </h2>
                          <h3><?php the_excerpt_max_charlength(70); ?>
                          <a href="<?php the_permalink(); ?>"></a></strong></h3>
                      </div>
                  </div> -->

                <!-- .post -->

                <!-- <?php endforeach;;?>

            </div> -->


			<?php if (have_posts()): ?>
				<?php while ( have_posts() ) : the_post(); ?>


				<?php endwhile;?>
			<?php endif ?>
    </div>

<?php get_footer(); ?>

<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
        LayerSlider.initLayerSlider();
        StyleSwitcher.initStyleSwitcher();
        OwlCarousel.initOwlCarousel();
        OwlRecentWorks.initOwlRecentWorksV2();
    });
</script>
