<?php if (have_posts()): ?>
<?php while ( have_posts() ) : the_post(); ?>

<?php get_header(); ?>

<?php the_content(); ?>

<?php
	global $post;
	$slug = get_post( $post )->post_name;
	$tag = get_term_by('slug', $slug, 'post_tag');	 
	if ($tag) {
?>

<div class="container" style="margin-bottom:50px;">
    <div class="row">
        <div class="col-md-12">

			<div class="relatedposts" >
				<h1 class="tit7 text-center" style="margin-top:30px; margin-bottom:30px">Artigos Relacionados</h1>
			
				<hr style="background-color:#FF7B2F; margin-bottom:30px;" />

				<?php
				    $args=array(
				    	'tag__in' => $tag->term_id,
				    	'posts_per_page'=>3,
				    	'caller_get_posts'=>1
				    );
				     
				    $my_query = new wp_query( $args );
				 
				    while( $my_query->have_posts() ) {
				    $my_query->the_post();
				 ?>
				     
			    <div class="relatedthumb col-md-4 text-center">
			        <a rel="external" href="<? the_permalink()?>"><?php the_post_thumbnail(array(300, 225)); ?><br />
			        <h4 style="margin-top:20px; margin-bottom:20px;"><?php the_title(); ?></h4>
			        </a>
			        <hr style="background-color:#FF7B2F" style="margin-bottom:30px;" />
			    </div>
				     
			    <? }
			    $post = $orig_post;
			    wp_reset_query();
			    ?>
			</div>
		</div>
	</div>
</div>

<?php } ?>

<?php get_footer(); ?>

<?php endwhile;?>
<?php endif ?>