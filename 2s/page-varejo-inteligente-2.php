  <?php if (have_posts()): ?>
<?php while ( have_posts() ) : the_post(); ?>

<?php get_header(); ?>
<div class="pagina-nova">


<header>
  <h1>Varejo inteligente<img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/verticais/sacola.png" alt=""></h1>
</header>



  <div class="video-background" style="position:relative" height="500px;" id="video-background-id">

    <video autoplay loop muted width="100%" id="video-banner">
      <source src="<?php echo get_stylesheet_directory_uri();?>/2S-Transformação-Digital-Varejo.mp4" type="video/mp4">
    </video>


    <div class="btn-banner">
    </div>
    <div class="btn-banner2" onclick="myFunction()"><p style="text-align: center;color: white;font-size: 20px;padding-top: 7px;">Assista ao video</p></div>

  </div>






<!-- <div class="faixa-azul">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 col-sm-offset-2">
        <h2>Por que falar sobre transformação digital no varejo?</h2>
        <p style="font-size:15px;">Quando as empresas apontam o mapeamento dos hábitos do consumidor e a utilização inteligente de dados como o futuro do varejo, muitas vezes, não percebem a dimensão da mudança que anunciam. A verdade é que criar essa realidade e fidelizar o novo consumidor exigem uma reinvenção de modelos de negócio e gestão. Esse ponto de virada chama-se transformação digital.</p>
        <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/verticais/icon1.png" alt="">
      </div>
    </div>
  </div>
</div> -->

<div class="container">
  <div class="row col-sm-10 col-sm-offset-1 div1">
    <div class="col-sm-12">
      <div class="headline">
        <h2>Por que falar sobre transformação digital no varejo?</h2>
      </div>
    </div>
    <div class="col-sm-2 icone">
      <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/verticais/icon1.png" alt="">
    </div>
    <div class="col-sm-10">
      <p>Quando as empresas apontam o mapeamento dos hábitos do consumidor e a utilização inteligente de dados como o futuro do varejo, muitas vezes, não percebem a dimensão da mudança que anunciam. A verdade é que criar essa realidade e fidelizar o novo consumidor exigem uma reinvenção de modelos de negócio e gestão. Esse ponto de virada chama-se transformação digital.</p>
    </div>
  </div>
</div>

<div class="container">
  <div class="row col-sm-10 col-sm-offset-1">
    <div class="col-sm-12">
      <div class="headline">
        <h2 style="font-size:24;">Problema</h2>
      </div>
    </div>
    <div class="col-sm-6">
      <ul class="lista">
        <li>Corresponder às expectativas dos consumidores se tornou uma tarefa ainda mais difícil. Segundo dados da Cisco, 89% deles deixaram de comprar em varejistas em que tiveram <font color="#2977BA">experiências ruins de atendimento.</font> Para esse cliente - 86% deles, precisamente -, vale pagar mais para ter uma melhor experiência de compra.</li>
        <li>Em uma pesquisa com mil consumidores na América Latina, a fabricante constatou, ainda, que 23% deles gostariam de ter <font color="#2977BA">experiências não só personalizadas, mas adequadas ao seu momento de compra,</font> com interação e envio de descontos em tempo real, por exemplo.</li>
      </ul>
    </div>
    <div class="col-sm-6">
      <ul class="lista">
        <li>Conveniência e flexibilidade também estão entre as exigências desse público, que tem a seu favor as inúmeras opções e a concorrência acirrada entre marcas.</li>
        <li>Apesar disso, a Cisco mapeou, em um levantamento com 200 executivos de varejo no mundo, que 30% deles têm uma <font color="#2977BA">postura reativa à disrupção digital.</font> Investimentos mais eficientes em transformação digital poderiam movimentar mais de US$ 506 bilhões em todo o mundo.</li>
      </ul>
    </div>
  </div>
</div>

<div class="container">
  <div class="row col-sm-10 col-sm-offset-1">
    <div class="col-sm-12">
      <div class="headline">
        <h2>Soluções</h2>
      </div>
      <p>Do back office ao negócio, ajudamos empresas do varejo a construir modelos e processos de negócio digitais, sempre com eficiência, otimização de recursos e aplicação da tecnologia para geração de receitas. Conheça algumas de nossas soluções customizadas para o setor varejista:</p>
    </div>
    <div class="col-sm-12 box-opcoes">
      <div class="item item1">
        <h4>Atendimento<br/>Personalizado</h4>
        <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/verticais/atendimentopersonalizado.png" alt="">
      </div>
      <div class="item item2">
        <h4>Conheça<br/>seu Cliente</h4>
        <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/verticais/conhecaseucliente.png" alt="">
      </div>
      <div class="item item3">
        <h4>Varejo<br/>Inteligente</h4>
        <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/verticais/varejointeligente.png" alt="">
      </div>
      <div class="item item4">
        <h4>Store in<br/>a Box</h4>
        <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/verticais/storeinabox.png" alt="">
      </div>
      <div class="item item5">
        <h4>Back<br/>Office</h4>
        <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/verticais/backoffice.png" alt="">
      </div>
      <div class="content-out">
        <div class="content content1">
          <div class="triangulo"></div>
          <p>Em um mercado que se move com tanta velocidade, a infraestrutura de TI pode se tornar um gargalo para a expansão das lojas. Por isso, a 2S desenhou a solução ‘Store in a Box’, que garante uma infraestrutura única e convergente, gerenciamento centralizado, economias de recursos e conectividade imediata para as novas lojas, a partir da simples instalação de um equipamento.</p>
        </div>
        <div class="content content2">
          <div class="triangulo"></div>
          <p>Diante de consumidores cada vez mais esclarecidos e conectados, vendedores precisam se tornar verdadeiros consultores, tendo sempre informações valiosas à mão. Com o uso de ferramentas de RFID, beacons, bluetooth e outras, ampliamos as possibilidades de venda, a partir de ações para cross e upselling, rentabilização dos espaços das gôndolas e ilhas de produção e gestão inteligente de estoque. A solução ‘Varejo Inteligente’ inclui ainda recursos de mobilidade e colaboração, alocação de funcionários  conforme demanda e perfil de consumo e estratégias omnichannel, com a integração dos mundos físico e virtual de forma fluida e intuitiva.</p>
        </div>
        <div class="content content3">
          <div class="triangulo"></div>
          <p>A solução propõe a análise do comportamento dos consumidores , mostrando seções mais movimentadas da loja, fluxo das pessoas no ambiente, tempo que permanecem em cada departamento, impacto de promoções e campanhas, entre outros dados, a fim de utilizar essas informações a favor do negócio. Tudo isso em tempo real, com interação com o cliente durante a compra.</p>
        </div>
        <div class="content content4">
          <div class="triangulo"></div>
          <p>O cliente deve se sentir único. Por isso, um atendimento inovador e personalizado faz a diferença em suas escolhas. A solução de ‘Atendimento Personalizado’ possibilita diferentes formas de interação, como uma central compartilhada de especialistas que atendem o cliente, via vídeo e em tempo real, no PDV.</p>
        </div>
        <div class="content content5">
          <div class="triangulo"></div>
          <p>As soluções da 2S envolvem o uso inteligente da TI para engajamento e preparo do time, a partir de ambientes de trabalho flexíveis e colaborativos, treinamentos sob demanda e maior interação entre áreas e plantas; otimização de processos, reduzindo o uso de recursos e aumentando a disponibilidade de rede; controle de qualidade em tempo real e produção modular e flexível, por meio da integração e automação de processos; e ambientes mais seguros, protegidos de invasões e vazamentos de informação.</p>
        </div>
      </div>

    </div>
  </div>
</div>

<script>
  jQuery(' .item1 ').click(function() {
    $( ".content1" ).fadeIn( "slow" );
    $( ".content2" ).fadeOut( "slow" );
    $( ".content3" ).fadeOut( "slow" );
    $( ".content4" ).fadeOut( "slow" );
    $( ".content5" ).fadeOut( "slow" );
  });
  jQuery(' .item2 ').click(function() {
    $( ".content1" ).fadeOut( "slow" );
    $( ".content2" ).fadeIn( "slow" );
    $( ".content3" ).fadeOut( "slow" );
    $( ".content4" ).fadeOut( "slow" );
    $( ".content5" ).fadeOut( "slow" );
  });
  jQuery(' .item3 ').click(function() {
    $( ".content1" ).fadeOut( "slow" );
    $( ".content2" ).fadeOut( "slow" );
    $( ".content3" ).fadeIn( "slow" );
    $( ".content4" ).fadeOut( "slow" );
    $( ".content5" ).fadeOut( "slow" );
  });
  jQuery(' .item4 ').click(function() {
    $( ".content1" ).fadeOut( "slow" );
    $( ".content2" ).fadeOut( "slow" );
    $( ".content3" ).fadeOut( "slow" );
    $( ".content4" ).fadeIn( "slow" );
    $( ".content5" ).fadeOut( "slow" );
  });
  jQuery(' .item5 ').click(function() {
    $( ".content1" ).fadeOut( "slow" );
    $( ".content2" ).fadeOut( "slow" );
    $( ".content3" ).fadeOut( "slow" );
    $( ".content4" ).fadeOut( "slow" );
    $( ".content5" ).fadeIn( "slow" );
  });
  jQuery(' .btn-banner2 ').click(function() {
    $( ".btn-banner" ).remove();
    $( ".btn-banner2" ).remove();

  });

</script>

<script>
function myFunction() {
    document.getElementById("video-banner").controls = true;
    document.getElementById("video-banner").load();
    video.muted = remove();

}
</script>

<div class="container">
  <div class="row col-sm-10 col-sm-offset-1">
    <div class="col-sm-12">
      <div class="headline">
        <h2>Ganhos</h2>
      </div>
    </div>
    <div class="col-sm-6">
      <ul class="lista">
        <li>Adequação dinâmica de portfólio e equipe, com a agilidade que o varejo demanda;</li>
        <li>Mensuração do impacto do posicionamento de produtos, promoções e campanhas, o que amplia a rentabilidade com a venda de espaços publicitários direcionados no interior da loja;</li>
        <li>Criação de empresas realmente digitais, que respondem com mais agilidade às mudanças do setor e da sociedade e, assim, diferenciam-se dos concorrentes;</li>
        <li>Mais facilidade em apresentar o retorno dos investimentos das ações de marketing;</li>
        <li>Controle integrado e eficiente da segurança da informação, permitindo a continuidade do negócio e a construção de uma boa reputação interna e externa.</li>
      </ul>
  </div>
    <div class="col-sm-6">
      <ul class="lista">
        <li>Maior qualidade e diferenciação do atendimento ao consumidor, aumentando o valor percebido pelo cliente e propiciando, assim, a conquista e a fidelização dos públicos mais exigentes;</li>
        <li>Incremento nas vendas e maior ticket médio por cliente, a partir da implementação de estratégias omnichannel (presença junto ao consumidor, onde ele estiver) e ações de cross e upselling;</li>
        <li>Redução de turnover e de custos com viagens, recrutamento e treinamento, além de maior transparência na comunicação com o time;</li>
        <li>Ambientes físicos mais seguros, com redução de acidentes de trabalho e de perdas relacionadas a roubos, furtos e problemas operacionais;</li>
      </ul>
    </div>
    <div class="col-sm-12 video-titulo">
      <p style="font-size:16px;"> <strong>Assista ao vídeo a seguir e saiba mais sobre como a tecnologia auxilia o varejo.</strong> </p>
    </div>
    <div class="col-sm-6">
      <!-- <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/verticais/video.png" class="img-responsive" alt=""> -->
      <div class="myIframe">
        <iframe src="//www.youtube.com/embed/VY1DMCx4XLg#action=share" allowfullscreen></iframe>
      </div>
    </div>
    <div class="col-sm-6 video-lista">

      <ul>
        <p style="font-size:16px;"><strong>Esse vídeo fala sobre:</strong></p>
        <li>Tecnologia + Marketing</li>
        <li>Conexão com clientes</li>
        <li>Transformação Digital</li>
        <li>Varejo Inovador</li>
        <li>Infraestrutura de TI</li>
        <li>Atendimento personalizado</li>
      </ul>
      <div style="clear:both;"></div>
    </div>
    <div class="col-sm-12 video-saibamais">
      <p style="font-size:16px;"><strong>Saiba mais</strong></p>
      <ul>
        <li>
          <div class="row">
            <div class="col-xs-8"><a href="http://www.2s.com.br/conteudo/e-book-transformacao-digital-o-futuro-das-empresas-e-as-empresas-do-futuro/" target="_blank">Ebook exclusivo: transformação digital e o varejo conectado</a></div>
            <div class="col-xs-4"><a href="" class="btn">Download</a></div>
          </div>
          </li>
        <li>
          <div class="row">
            <div class="col-xs-8"><a href="http://www.2s.com.br/transformacao-digital-3-frentes-de-atuacao-no-varejo/" target="_blank">Transformação digital: 3 frentes de atuação no varejo</a></div>
            <!-- <div class="col-xs-4"><a href="" class="btn">Download</a></div> -->
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>

<div class="divideo">
  <div class="faixa"></div>
</div>

<div class="container">
  <div class="row col-sm-10 col-sm-offset-1" style="background-color:#FBFAFB;">
    <div class="col-sm-12">
      <div class="headline">
        <h2>Experiência, engajamento e relevância</h2>
      </div>
      <p>Os tempos mudaram e os consumidores também. É preciso sair das campanhas de marketing passiva e utilizar toda tecnologia possível para conhecer e se conectar com os clientes e consumidores. Isso é tecnologia aplicada ao negócio, isto é Transformação Digital .</p>
      <p><strong>Vamos transformar o seu negócio com tecnologia.</strong></p>

        <div class="video">
          <video width="100%" id="video">
            <source src="<?php echo get_stylesheet_directory_uri();?>/mapa_varejo-animar_2.webm" type="video/webm">
          </video>
          <div class="voltar-video">Voltar</div>

          <video autoplay loop muted width="100%" id="video2">
            <source src="<?php echo get_stylesheet_directory_uri();?>/Tela-1.webm" type="video/webm">
          </video>
          <div class="bullet-video bullet-video2-1">+</div>
          <div class="bullet-video bullet-video2-2">+</div>
          <div class="bullet-video bullet-video2-3">+</div>
          <div class="bullet-video bullet-video2-4">+</div>

          <video autoplay loop muted width="100%" id="video3">
            <source src="<?php echo get_stylesheet_directory_uri();?>/Tela-2.webm" type="video/webm">
          </video>
          <div class="bullet-video bullet-video3-1">+</div>
          <div class="bullet-video bullet-video3-2">+</div>
          <div class="bullet-video bullet-video3-3">+</div>
          <div class="bullet-video bullet-video3-4">+</div>

          <video autoplay loop muted width="100%" id="video4">
            <source src="<?php echo get_stylesheet_directory_uri();?>/Tela-3.webm" type="video/webm">
          </video>
          <div class="bullet-video bullet-video4-1">+</div>
          <div class="bullet-video bullet-video4-2">+</div>
          <div class="bullet-video bullet-video4-3">+</div>

          <img src="<?php echo get_stylesheet_directory_uri();?>/mapa_varejo-animar.png" width="100%;" class="mapa_varejo">
          <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/verticais/infografico1.png" width="100%;" class="img-varejo">
          <div class="bullet experiencia bullet-novo">+</div>
          <div class="texto exp"></div>

          <div class="bullet perfil bullet-novo">+</div>
          <div class="texto per"></div>

          <div class="bullet varejo bullet-novo">+</div>
          <div class="texto var"></div>

        </div>

        <div class="row textoresp">
          <ul>
            <li>
              <div class="col-sm-6 ">
                <p style="color:black;margin-bottom:0px;">Experiências personalizadas</p>
                <p>Comunicações, pesquisas e promoções direcionadas ao perfil e localização do cliente.</p>
              </div>
            </li>
            <li>
              <div class="col-sm-6 ">
                <p style="color:black;margin-bottom:0px;">Shopper Analytcs</p>
                <p>Mapeamento das seções mais visitadas, fluxo dos visitantes no interior da loja e impacto de promoções e campanhas.</p>
              </div>
            </li>
            <li>
              <div class="col-sm-6 ">
                <p style="color:black;margin-bottom:0px;">Posicionamento inteligente de produtos</p>
                <p>Mais vendas, cross - selling e aumento do ticket médico das compras.</p>
              </div>
            </li>
            <li>
              <div class="col-sm-6 ">
                <p style="color:black;margin-bottom:0px;">Estratégias omnichannel</p>
                <p>Do físico ao virtual sem perder a venda.</p>
              </div>
            </li>
            <li>
              <div class="col-sm-6 ">
                <p style="color:black;margin-bottom:0px;">Localização Indoor</p>
                <p>Mapas e rotas do estabelecimento na palma da mão do cliente.</p>
              </div>
            </li>
            <li>
              <div class="col-sm-6 ">
                <p style="color:black;margin-bottom:0px;">Perfil do Cliente</p>
                <p>Nome, idade, gostos, frequência - tudo sobre o cliente.</p>
              </div>
            </li>
            <li>
              <div class="col-sm-6 ">
                <p style="color:black;margin-bottom:0px;">Conectividade e Informação</p>
                <p>Jornadas e interações customizadas de acordo com o perfil do cliente.</p>
              </div>
            </li>
            <li>
              <div class="col-sm-6 ">
                <p style="color:black;margin-bottom:0px;">Atendimento Interativo</p>
                <p>Especialistas prontos para esclarecer dúvidas e dar soluções.</p>
              </div>
            </li>
            <li>
              <div class="col-sm-6 ">
                <p style="color:black;margin-bottom:0px;">Varejo Inteligente</p>
                <p>Mobilidade para gestão de território e agilidade para expansão dos negócios.</p>
              </div>
            </li>
            <li>
              <div class="col-sm-6 ">
                <p style="color:black;margin-bottom:0px;">Time engajado e eficiente</p>
                <p>Treinamentos on demand e comunicação fluida.</p>
              </div>
            </li>
            <li>
              <div class="col-sm-6 ">
                <p style="color:black;margin-bottom:0px;">Domínio do Ambiente</p>
                <p>Visibilidade de rede e controle de acessos.</p>
              </div>
            </li>
          </ul>
        </div>

      <script type="text/javascript">

      // var video = document.getElementById('video'), fraction = 0.8;

      jQuery( "document" ).ready(function(){
        var target = $(".faixa"),
            posTarget = target.offset(),
            video = $("#video"),
            posVideo = video.offset().top;

        $(window).scroll(function(){
          posTarget = target.offset().top;
          if(posTarget >= posVideo){
            video.get(0).play();
            if(($(window).width() > 768)) {
              $(".bullet").fadeIn(5000);
              $(".texto").fadeIn(5000);
            }
          }
        });

        $(".experiencia").click(function(){
          $("#video").fadeOut(1000);
          $(".texto").fadeOut(1000);
          $(".bullet-video2-1").fadeIn(3000);
          $(".bullet-video2-2").fadeIn(3000);
          $(".bullet-video2-3").fadeIn(3000);
          $(".bullet-video2-4").fadeIn(3000);
          $("#video2").fadeIn(1000);
          $(".voltar-video").fadeIn(2000);           
          $("#video3").fadeOut(1000);
          $("#video4").fadeOut(1000);
          $(".bullet").fadeOut(1000);
          $("div").removeClass("divideo");
          $("div").removeClass("faixa");
        });

        $(".perfil").click(function(){
          $("#video").fadeOut(1000);
          $(".texto").fadeOut(1000);
          $("#video3").fadeIn(1000);
          $(".voltar-video").fadeIn(2000);
          $("#video2").fadeOut(1000);
          $("#video4").fadeOut(1000);
          $(".bullet").fadeOut(1000);
          $(".bullet-video3-1").fadeIn(3000);
          $(".bullet-video3-2").fadeIn(3000);
          $(".bullet-video3-3").fadeIn(3000);
          $(".bullet-video3-4").fadeIn(3000);
          $("div").removeClass("divideo");
          $("div").removeClass("faixa");
        });

        $(".varejo").click(function(){
          $("#video").fadeOut(1000);
          $(".texto").fadeOut(1000);
          $("#video4").fadeIn(1000);   
          $(".voltar-video").fadeIn(1000);       
          $("#video2").fadeOut(1000);
          $("#video3").fadeOut(1000);
          $(".bullet").fadeOut(1000);
          $(".bullet-video4-1").fadeIn(3000);
          $(".bullet-video4-2").fadeIn(3000);
          $(".bullet-video4-3").fadeIn(3000);
          $("div").removeClass("divideo");
          $("div").removeClass("faixa");
        });

        $(".voltar-video").click(function(){
          $("#video2").fadeOut(2000);
          $("#video3").fadeOut(2000);
          $("#video4").fadeOut(2000);
          $(".voltar-video").fadeOut(1000);
          $(".bullet-video").fadeOut(500);
          $("#video").fadeIn(2000);
          $(".bullet").fadeIn(2000);
          $(".texto").fadeIn(2000);
        }); 

      });



      </script>

    </div>
  </div>
</div>

<div class="container porcentagem">
  <div class="row col-sm-10 col-sm-offset-1" style="background-color:#FBFAFB;">
    <div class="col-sm-3 azul">
      <span>59%</span>
      <p>Das lojas físicas serão impactadas pelas internet em 2018.</p>
    </div>
    <div class="col-sm-3 verde">
      <span>79%</span>
      <p>Planejam investir na verificação de inventário automatizada.</p>
    </div>
    <div class="col-sm-3 laranja">
      <span>85%</span>
      <p>Planejam usar a tecnologia para personalizar a visita à loja.</p>
    </div>
    <div class="col-sm-3 vermelho">
      <span>75%</span>
      <p>Oferecerão serviços baseados em localização até 2021.</p>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <div class="relatedposts">
        <h1 class="tit7 text-center" style="margin-top:30px; margin-bottom:30px">Artigos Relacionados</h1>
      </div>
    </div>
  </div>
</div>

<div class="container">

    <div class="margin-bottom-60"></div>

    <?php
    $query= null;
    $query = new WP_Query(
    array(
    'post_type' => 'destaque',
    'posts_per_page' => 6)
    );
    ?>

    <div class="row news-v2 margin-bottom-80">

    <?php
    if ( $query->have_posts() ) {
        while ( $query->have_posts() ) {
        $query->the_post();
        $custom             = get_post_custom($post->ID);
        $destaque_descricao    =   $custom["destaque_descricao"][0];
        $destaque_url          =   $custom["destaque_link"][0];
        $destaque_foto         =   $custom["destaque_foto"][0];
        // print_r($custom);

    ?>

        <div class="col-md-4" style="margin-bottom:20px;">
          <a href="<?php echo $destaque_url; ?>" style="text-decoration:none">
            <div class="news-v2-badge">
                <img class="img-responsive" style="width:100%;height: 227px;" src="<?php echo $destaque_foto; ?>" alt="">
                <p>
                    <img class="img-responsive" src="/wp-content/themes/2s/assets/img/simbol-2s-box.png" alt="">
                </p>
            </div>
            <div class="news-v2-desc2 post-border" id="post-border">
              <h2 style="margin-bottom:-50px"><?php echo $destaque_descricao; ?></strong></h2>
              <!-- <h2 class="entry-title" style="font-size: 19px;text-align: left;">
                  <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a>
              </h2> -->
            </div>
          </a>
        </div>

    <?php } } ?>
    </div>
</div>

<div class="container contato">
  <div class="row">
    <div class="col-sm-12">
      <div class="headline">
        <h2>Cadastre-se para ficar atualizado com a 2S</h2>
      </div>
      <?php echo do_shortcode('[contact-form-7 id="948" title="Cadastro Verticais"]'); ?>
    </div>
  </div>
</div>

</div>
<?php the_content(); ?>
<?php get_footer(); ?>

<style>
#video2{
  display: none;
}
#video3{
  display: none;
}
#video4{
  display: none;
}
.voltar-video{
  position: absolute;
  top: 10%;
  right: -1%;
  width: 50px;
  height: 30px;
  background-color: #1F71B7;
  cursor: pointer;
  z-index: 10;
  padding: 5px 4px;
  color: white;
  display: none;
}
.bullet-video{
  display: none;
  background-color: #1F71B7;
  border-radius: 25px;
  width: 35px;
  height: 35px;
  color: white;
  position: absolute;
  font-size: 20px;
  padding: 1px 12px;
  cursor: pointer;
  box-shadow: 0 0 0 rgba(243,168,58,.6);
  animation: pulse 2s infinite;
}

@-webkit-keyframes bullet-video {
  0% {
    -webkit-box-shadow: 0 0 0 0 rgba(204,169,44, 0.6);
  }
  70% {
      -webkit-box-shadow: 0 0 0 10px rgba(204,169,44, 0);
  }
  100% {
      -webkit-box-shadow: 0 0 0 0 rgba(204,169,44, 0);
  }
}
@keyframes bullet-video {
  0% {
    -moz-box-shadow: 0 0 0 0 rgba(204,169,44, 0.6);
    box-shadow: 0 0 0 0 rgba(204,169,44, 0.4);
  }
  70% {
      -moz-box-shadow: 0 0 0 10px rgba(204,169,44, 0);
      box-shadow: 0 0 0 10px rgba(204,169,44, 0);
  }
  100% {
      -moz-box-shadow: 0 0 0 0 rgba(204,169,44, 0);
      box-shadow: 0 0 0 0 rgba(204,169,44, 0);
  }
}

.bullet-video:before{
  content: '';
  display: none;
  background-color: #1F71B7;
  border-radius: 25px;
  width: 35px;
  height: 35px;
  color: white;
  position: absolute;
  font-size: 20px;
  padding: 1px 12px;
  cursor: pointer;
}
.bullet-video:hover:after{
  background-color: #1F71B7; 
  border-radius: 15px;
  width: 0;
  height: 35px;
  content: '';
  left: 0%;
  position: absolute;
  top: 0%;
  font-size: 14px;
  text-align: center;
  z-index: 2;
  transition-property: -webkit-transform, opacity;
  transition-duration: 1.0s;
  transition-timing-function: ease-out;
}
.bullet-video2-1{
   display: none;
   top: 82%;
   right: 39%;
}
.bullet-video2-1:hover:after{
  content: 'Localização Indoor';
  padding: 5px 5px;
  width: 150px;
  height: 35px;
  margin-left: -55px;
}
.bullet-video2-2{
    display: none;  
    top: 70%;
    right: 80%;
}
.bullet-video2-2:hover:after{
  content: 'Shopper Analytics';
  padding: 5px 5px;
  width: 150px;
  height: 35px;
  margin-left: -55px;
}
.bullet-video2-3{
    display: none;
    top: 52%;
    right: 13%;
}
.bullet-video2-3:hover:after{
  content: 'Sortimento e Posicionamento Inteligente';
  padding: 0px 5px;
  width: 150px;
  height: auto;
  margin-left: -55px;
}
.bullet-video2-4{
    display: none;
    top: 80%;
    right: 60%;
}
.bullet-video2-4:hover:after{
  content: 'Estratégia Omnichannel';
  padding: 0px 5px;
  width: 150px;
  height: auto;
  margin-left: -55px;
}
.bullet-video3-1{
    top: 58%;
    right: 50%;
}
.bullet-video3-1:hover:after{
  content: 'Experiência Personalizada';
  padding: 5px 5px;
  width: 170px;
  height: auto;
  margin-left: -55px;
}
.bullet-video3-2{
    top: 52%;
    right: 13%;
}
.bullet-video3-2:hover:after{
  content: 'Perfil do Cliente Aparecendo no Computador';
  padding: 5px 5px;
  width: 170px;
  height: auto;
  margin-left: -55px;
}
.bullet-video3-3{
    top: 55%;
    right: 69%;
}
.bullet-video3-3:hover:after{
  content: 'Conectividade e Interação no Celular do Cliente';
  padding: 5px 5px;
  width: 170px;
  height: auto;
  margin-left: -55px;
}
.bullet-video3-4{
    top: 69%;
    right: 25%;
}
.bullet-video3-4:hover:after{
  content: 'Time engajado no Caixa';
  padding: 5px 5px;
  width: 170px;
  height: 35px;
  margin-left: -55px;
}
.bullet-video4-1{
    top: 33%;
    right: 61%;
}
.bullet-video4-1:hover:after{
  content: 'Atendimento Interativo';
  padding: 5px 5px;
  width: 170px;
  height: auto;
  margin-left: -55px;
}
.bullet-video4-2{
    top: 57%;
    right: 24%;
}
.bullet-video4-2:hover:after{
  content: 'Verejo Inteligente';
  padding: 5px 5px;
  width: 170px;
  height: 35px;
  margin-left: -55px;
}
.bullet-video4-3{
    top: 65%;
    right: 31%;
}
.bullet-video4-3:hover:after{
  content: 'Domínio Ambiente';
  padding: 5px 5px;
  width: 170px;
  height: 35px;
  margin-left: -55px;
}
.bullet-novo{
  transform: rotate(180deg);
  border: none;
  background-color: #1F71B7;
  border-radius: 25px;
  width: 35px;
  height: 35px;
  color: white;
  font-size: 1.5em;
  padding: 0px 11px;
  cursor: pointer;
  box-shadow: 0 0 0 rgba(243,168,58,.5);
  animation: pulse 2s infinite;
}

@-webkit-keyframes bullet-novo {
  0% {
    -webkit-box-shadow: 0 0 0 0 rgba(204,169,44, 0.6);
  }
  70% {
      -webkit-box-shadow: 0 0 0 10px rgba(204,169,44, 0);
  }
  100% {
      -webkit-box-shadow: 0 0 0 0 rgba(204,169,44, 0);
  }
}
@keyframes bullet-novo {
  0% {
    -moz-box-shadow: 0 0 0 0 rgba(204,169,44, 0.6);
    box-shadow: 0 0 0 0 rgba(204,169,44, 0.4);
  }
  70% {
      -moz-box-shadow: 0 0 0 10px rgba(204,169,44, 0);
      box-shadow: 0 0 0 10px rgba(204,169,44, 0);
  }
  100% {
      -moz-box-shadow: 0 0 0 0 rgba(204,169,44, 0);
      box-shadow: 0 0 0 0 rgba(204,169,44, 0);
  }
}

@media(max-width: 768px){
  .bullet-novo{
    display: none;
  }
}
</style>

<?php endwhile;?>
<?php endif ?>
