<?php get_header(); ?>


    <div class="banner-home">
      <p>A transformação digital é agora - e para TODOS! Saiba como a tecnologia aplicada ao seu negócio pode aumentar a competitividade e acelerar o crescimento de sua empresa.
      </p>
      <a href="http://www.2s.com.br/contato/"><button type="button" name="button">Fale com a 2S</button></a>
    </div>


  <div id="inicio-posts" class="home-posts">

    <div class="container">

      <!-- MENU CATEGORIAS -->
      <?php  include get_template_directory(). '/menu-categorias.php'; ?>


            <div class="headline">
              <h2 style="font-size:25px">DESTAQUES DA SEMANA</h2>
            </div>
            <div class="margin-bottom-60"></div>

            <?php
            $query= null;
            $query = new WP_Query(
            array(
            'post_type' => 'destaque',
            'posts_per_page' => 6)
            );
            ?>

            <div class="row news-v2 margin-bottom-80">

            <?php
            if ( $query->have_posts() ) {
                while ( $query->have_posts() ) {
                $query->the_post();
                $custom             = get_post_custom($post->ID);
                $destaque_descricao    =   $custom["destaque_descricao"][0];
                $destaque_url          =   $custom["destaque_link"][0];
                $destaque_foto         =   $custom["destaque_foto"][0];
                // print_r($custom);

            ?>

                <div class="col-md-4" style="margin-bottom:20px;">
                  <a href="<?php echo $destaque_url; ?>" style="text-decoration:none">
                    <div class="news-v2-badge">
                        <img class="img-responsive" style="width: 100%;height: 227px;" src="<?php echo $destaque_foto; ?>" alt="">
                        <p>
                            <img class="img-responsive" src="/wp-content/themes/2s/assets/img/simbol-2s-box.png" alt="">
                        </p>
                    </div>
                    <div class="news-v2-desc2 post-border" id="post-border">
                      <h2 style="margin-bottom:-50px"><?php echo $destaque_descricao; ?></strong></h2>
                      <!-- <h2 class="entry-title" style="font-size: 19px;text-align: left;">
                          <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a>
                      </h2> -->
                    </div>
                  </a>
                </div>

            <?php } } ?>
            </div>


            <div class="headline" style="margin-bottom:50px">
              <h2 style="font-size:25px">ÚLTIMOS POSTS</h2>
            </div>

            <div class="row news-v2 margin-bottom-60">

              <?php

                $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                $args = array( 'post_type' => 'post', 'posts_per_page' => 9, 'paged' => $paged, 'page' => $paged);
                $loop = new WP_Query( $args );

                if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>


                <!-- .post -->

                  <div class="col-md-4" style="margin-bottom:20px;">
                    <a href="<?php the_permalink(); ?>" style="text-decoration:none">
                      <div class="news-v2-badge">
                            <div class="img-responsive" style="height: 227px;">
                              <?php if (has_post_thumbnail()): ?>
                                <?php the_post_thumbnail( 'blog-thumb' ); ?>
                              <?php endif ?>
                            </div>
                          <!-- </a> -->
                          <p>
                              <img class="img-responsive" src="/wp-content/themes/2s/assets/img/simbol-2s-box.png" alt="">
                          </p>
                      </div>
                      <!-- <a href="<?php the_permalink(); ?>"> -->
                        <div class="news-v2-desc2 post-border" id="post-border">
                          <h2 class="entry-title" style="font-size: 19px;text-align: left;">
                              <?php the_title(); ?>
                          </h2>
                          <p>
                            <?php the_excerpt_max_charlength(70); ?></strong>
                          </p>
                        </div>
                        </a>
                  </div>

                <!-- .post -->

              <?php endwhile; ?>
              <?php endif; ?>

              <!-- <div class="">
                    <ul class="pager">
                      <li class="previous"><?php next_posts_link( '<i class="fa fa-chevron-left fa-lg" aria-hidden="true"></i> Posts Anteriores', $loop->max_num_pages); ?></li>
                      <li class="next"><?php previous_posts_link( ' Próximos Posts <i class="fa fa-chevron-right fa-lg" aria-hidden="true"></i>', $loop->max_num_pages ); ?></li>
                    </ul>
                  </div>   -->

            </div>


		 </div>
    <?php get_footer(); ?>
  </div>


<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
        LayerSlider.initLayerSlider();
        StyleSwitcher.initStyleSwitcher();
        OwlCarousel.initOwlCarousel();
        OwlRecentWorks.initOwlRecentWorksV2();
    });

    $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();
    });
</script>
