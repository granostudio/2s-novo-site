
<?php get_header(); ?>
    <!--=== Breadcrumbs v3 ===-->
    <div class="breadcrumbs breadcrumbs-light img-cases">
        <div class="container">
            <div class="titulos-breadcrumbs">
                <h1>Vídeo e E-books</h1>
            </div>
        </div>
    </div>
    <!--=== End Breadcrumbs v3 ===-->

    <?php if (have_posts()): ?>
    <?php while ( have_posts() ) : the_post();
    ?>

    <!--=== Container Part ===-->
    <div class="container">
        <div class="row">
            <div class="col-md-9">


                <div class="headline">
                    <h2><?php echo the_field("conteudo_titulo"); ?></h2>
                </div>

                <div class="row cases">
                    <div style="float:left;">
                        <img class="img-responsive cases" src="<?php the_field("conteudo_imagem"); ?>">
                    </div>

                    <?php echo the_field("conteudo_texto"); ?>

                </div>
                <div class="row cases">

                    <div class="headline"><h2>Preencha o formulário a seguir</h2></div>

                    <div class="col-md-12">
                        <div class="panel textoInterno" id="resposta"></div>

                        <form id="formEdit" method="POST" class="sky-form contact-style">
                        <input type="hidden" value="bruna.gaspar@2s.com.br" name="destinatario" />
                        <input type="hidden" value="<?php echo the_field("conteudo_titulo"); ?>" name="conteudo_titulo"/ />
                        <input type="hidden" value="<?php echo the_field("conteudo_ebook"); ?>" name="conteudo_ebook" />
                        <input type="hidden" value="<?php echo the_field("conteudo_video"); ?>" name="conteudo_video" />
                        <input type="hidden" value="true" name="contato" />


                            <fieldset id="formulario" class="no-padding">
                                <label>Nome <span class="color-red">*</span></label>
                                <div class="row sky-space-20">
                                    <div class="col-md-11 col-md-offset-0">
                                        <div>
                                            <input type="text" name="nome" id="nome" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <label>E-mail <span class="color-red">*</span></label>
                                <div class="row sky-space-20">
                                    <div class="col-md-11 col-md-offset-0">
                                        <div>
                                            <input type="text" name="email" id="email" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <label>Empresa <span class="color-red">*</span></label>
                                <div class="row sky-space-20">
                                    <div class="col-md-11 col-md-offset-0">
                                        <div>
                                            <input type="text" name="empresa" id="empresa" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <label>Telefone <span class="color-red">*</span></label>
                                <div class="row sky-space-20">
                                    <div class="col-md-6 col-md-offset-0">
                                        <div>
                                            <input type="text" name="telefone" id="telefone" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div id="capatcha" style="margin-top: 15px;">
                                    <div class="g-recaptcha" data-sitekey="6LcrUQcUAAAAAJl_CzudrmPQYNTh79IqHxwYwNCx"></div>
                                </div>
                                <br>
                                <p><button type="submit" class="btn-u">Enviar</button></p>
                            </fieldset>
                        </form>


                        <div class="margin-bottom-40"></div>
                        <a href="/conteudos" class="btn btn-link" title="Voltar para página de Cases">Voltar para página de Conteúdos</a>
                        <div class="margin-bottom-40"></div>
                    </div>
                </div>


            </div><!-- End col-9 -->
            <div class="col-md-3">
                <?php get_sidebar(); ?>
            </div><!-- End col-3 -->

        </div> <!-- End row-->
    </div>

    </div>
    <!--=== End Container Part ===-->
    <?php endwhile;?>
    <?php endif ?>

    <?php get_footer(); ?>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            App.init();
            StyleSwitcher.initStyleSwitcher();

            $("#formEdit").submit(function(event) {
                event.preventDefault();

                if($("#g-recaptcha-response").val()){
                     $("#resposta").html("<img src='/wp-content/themes/2s/assets/img/loader.gif' align='absmiddle'> Mensagem sendo enviada, por favor aguarde... ");

                     $.post("/wp-content/themes/2s/sendcontent.php", $("#formEdit").serialize(), function(data) {
                        $('#resposta').html(data);
                     });
                } else {
                    $("#resposta").html("<div class='panel-body'><div class='alert alert-danger fade in text-center' style='margin:0px;'><h4 style='margin:0px;'>Preencha os campos e marque o checkbox para acessar o conteúdo.</h4></div></div>");
                }
                return false;
            });
    });
    </script>
