<?php get_header(); ?>

<!--=== Breadcrumbs v3 ===-->
<div class="breadcrumbs breadcrumbs-light img-cases">
    <div class="container">
        <div class="titulos-breadcrumbs">
            <h1>
              <?php $categories = get_the_category();

              if ( ! empty( $categories ) ) {
                  // echo esc_html( $categories[0]->name );
                  echo get_category_parents($categories[0]->cat_ID, true, ' / ' );
              }

               ?>
            </h1>
        </div>
    </div>
</div>
<!--=== End Breadcrumbs v3 ===-->

<?php if (have_posts()): ?>
<?php while ( have_posts() ) : the_post(); ?>

  <!--=== Container Part ===-->
  <div class="container">
      <div class="row">
          <div class="col-md-9">


              <div class="headline">
                  <h2><?php echo get_the_title(); ?></h2>
              </div>

              <span class="entry-date"><p style="margin-bottom: 20px">Publicado em <?php echo get_the_date(); ?></p></span>

              <div class="row cases">
                  <div style="float:left;">

                    <?php if ( has_post_thumbnail() ) {
                    	 the_post_thumbnail('post-thumbnail', ['class' => 'img-responsive cases '] );
                     }   ?>
                  </div>
                  <!-- .entry-content -->

                      <?php the_content()?>

              </div>


          </div><!-- End col-9 -->
          <div class="col-md-3">
              <?php get_sidebar(); ?>
          </div><!-- End col-3 -->

      </div> <!-- End row-->
  </div>


<?php get_footer(); ?>

<?php endwhile;?>
<?php endif ?>
