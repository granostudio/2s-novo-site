<?php if (have_posts()): ?>
<?php while ( have_posts() ) : the_post(); ?>

<?php get_header(); ?>
<?php the_content(); ?>


<?php
	global $post;
	$slug = get_post( $post )->post_name;

	$tag = get_term_by('slug', $slug, 'post_tag');
	 
	if ($tag) {
?>


<div class="apresenta">
	<h1 class="titcima">Colaboração</h1>
	<hr class="cor1" />
	<h4 class="titcima2">Uma nova fonte de inteligência, negócios, eficiência e geração de receitas. </h4>
</div>
<div class="TabControl">
	<div id="header1">
		<div id="icone"><img src="http://www.2s.com.br/wp-content/themes/2s/assets/img/solutions/colaboracao.png" alt="Colaboração" /></div>
	</div>
</div>

<div class="container solucao">
    <div class="row">
        <div class="col-md-9">

			

			<div class="internascont">		
				<div class="texto">
					<div class="colacentral">
						<h1 class="tit1">Proposta de valor</h1>
						<p>Partimos sempre do princípio de que é preciso criar a melhor experiência para o cliente, com a agilidade e a qualidade necessárias.</p>
						<hr style="background-color:#FFC229; width:80px" />
						<p>Investigamos a realidade do cliente: como é a sua conta de telecomunicação? Quais os gargalos de produtividade? A partir da identificação do problema, construímos a solução.</p>
						<hr style="background-color:#FFC229; width:80px"  />
						<p>Ajudamos a empresa a detectar as ferramentas mais eficazes para o seu contexto. A telepresença imersiva é fantástica, mas para uma universidade, por exemplo, não funciona, por causa do número de pessoas.</p>
						<hr style="background-color:#FFC229; width:80px" />
						<p>Diagnosticamos e dimensionamos o ambiente: a empresa tem todos os pré-requisitos para o funcionamento da solução? A infraestrutura de redes está adequada? A resposta a essas questões irá favorecer o resultado.</p>
						<hr style="background-color:#FFC229; width:80px" />
						<p>Orientamos o cliente sobre as melhores práticas para criação de uma cultura de uso das ferramentas e propagação das informações.</p>
					</div>
				</div>
			</div>


			<div class="row tabelas-colaboracao">
				<div class="internascont text-center col-sm-7">
					<span class="imgs">
	                    <img class="img-responsive" src="http://www.2s.com.br/wp-content/themes/2s/assets/img/solutions/main-banner-colaboracao.jpg" />
	                </span>
					<div class="texto">
						<h1 class="tit1">Desafios</h1>
						<p>Muitas empresas já aderiram às ferramentas de colaboração, pelos benefícios óbvios que elas proporcionam. Não raramente, porém, os resultados prometidos ficam apenas no discurso de venda do fornecedor.</p>
						<p>É o caso da empresa que adota uma solução repleta de funcionalidades, porém não as utiliza plenamente. Isso pode ocorrer em função da falta de:</p>
					</div>
				</div>
				<div class="col-sm-4 div-quadrado">Especialização e compromisso do fornecedor para implementar os recursos que a solução oferece</div>
				<div class="col-sm-4 div-quadrado">Mapeamento dos pré-requisitos necessários na empresa para que a tecnologia funcione integralmente</div>
				<div class="col-sm-4 div-quadrado">Consultoria estratégica para identificar as funcionalidades mais aderentes a cada público da organização</div>
				<div class="col-sm-4 div-quadrado">Treinamento e criação de uma cultura de uso dessas ferramentas</div>
			</div>


			<div class="slidercolab">
				<span class="caixaslide">

					<div id="carousel" class="carousel slide" data-ride="carousel">
						<!-- Indicators -->
						<ol class="carousel-indicators">
							<li data-target="#carousel" data-slide-to="0" class="active"></li>
							<li data-target="#carousel" data-slide-to="1"></li>
							<li data-target="#carousel" data-slide-to="2"></li>
							<li data-target="#carousel" data-slide-to="3"></li>
						</ol>
						<!-- Wrapper for slides -->

							<div class="carousel-header">
								<h2 class="tit1">
									Ganhos
								</h2>
								<hr style="background-color:#FFC229" />
								<div class="carousel-inner">
									<div class="item active">
										<div class="carousel-content">
											<div>
												<h3>Redução de custos</h3>
												<p>Não só por diminuir a frequência de viagens e deslocamentos, mas por permitir aos colaboradores gerenciarem mais clientes do que antes conseguiam.</p>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="carousel-content">
											<div>
												<h3>Aumento de produtividade</h3>
												<p>Colaborador conectado, ativo e motivado pelo modelo flexível de trabalho.</p>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="carousel-content">
											<div>
												<h3>Melhoria da experiência do cliente final</h3>
												<p>No varejo ou setor financeiro, por exemplo, especialistas podem estar disponíveis virtualmente para tirar dúvidas, dar dicas e manter canal de relacionamento com o consumidor.</p>
											</div>
										</div>
									</div>

									<div class="item">
										<div class="carousel-content">
											<div>
												<h3>Incremento de receita</h3>
												<p>É a mera consequência dos itens anteriores.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
				</span>
			</div>

		</div>
		<div class="hidden-xs col-md-3">
			<?php echo do_shortcode('[shortcode_sidebar]'); ?>
		</div><!-- End col-3 -->

    </div> <!-- End row--> 
</div>

<div class="col-md-10 portfoliocolab">
	<h1 class="tit7">Portfólio</h1>
	<hr style="background-color:#fff" />
	<p>Telefonia IP: ligação custo zero entre as localidades</p>
	<p style="color:#FFF">•</p>
	<p>Mensagem Instantânea e Presença</p>
	<p style="color:#FFF">•</p>
	<p>Telepresença (videoconferência)</p>
	<p style="color:#FFF">•</p>
	<p>Telepresença Imersiva (com a simulação realista de uma sala de reunião)</p>
	<p style="color:#FFF">•</p>
	<p>Web Conference (com recursos para compartilhamento de conteúdo e interatividade)</p>
	<p style="color:#FFF">•</p>
	<p>Unified message</p>
	<p style="color:#FFF">•</p>
	<p>Contact Center</p>
</div>


<div class="container" style="margin-bottom:50px;">
    <div class="row">
        <div class="col-md-12">

			<div class="relatedposts" >
				<h1 class="tit7 text-center" style="margin-top:30px; margin-bottom:30px">Artigos Relacionados</h1>
			
				<hr style="background-color:#FFC229; margin-bottom:30px;" />

				<?php
				    $args=array(
				    	'tag__in' => $tag->term_id,
				    	'posts_per_page'=>3,
				    	'caller_get_posts'=>1
				    );
				     
				    $my_query = new wp_query( $args );
				 
				    while( $my_query->have_posts() ) {
				    $my_query->the_post();
			    ?>
				     
				    <div class="relatedthumb col-md-4 text-center">
				        <a rel="external" href="<? the_permalink()?>"><?php the_post_thumbnail(array(300, 225)); ?><br />
				        <h4 style="margin-top:20px; margin-bottom:20px;"><?php the_title(); ?></h4>
				        </a>
				        <hr style="background-color:#FFC229" style="margin-bottom:30px;" />
				    </div>
				     
				    <? }
				    $post = $orig_post;
				    wp_reset_query();
			    ?>
			</div>
		</div>
	</div>
</div>


<script>
$(document).ready(function(){
$("#conteudos div:nth-child(1)").show(); $(".abas li:first div").addClass("selected"); $(".aba").click(function(){ $(".aba").removeClass("selected"); $(this).addClass("selected"); var indice = $(this).parent().index(); indice++; $("#conteudos div").hide(); $("#conteudos div:nth-child("+indice+")").fadeIn('slow'); }); $(".aba").hover( function(){$(this).addClass("ativa")},function(){$(this).removeClass("ativa")} );

        jQuery(window).scroll(function() {
		  if (jQuery(window).scrollTop() > 100) {
			jQuery('.header-fixed .header-sticky').addClass('header-fixed-shrink');
		  } else {
			jQuery('.header-fixed .header-sticky').removeClass('header-fixed-shrink');
		  }
		});
    });
</script>


<style>

 .div-quadrado{
 	width: 255px;
 	height: 16em;
    text-align: center;
    font-weight: 100;
    padding: 80px 15px;
    border: solid 2px #FFC229;
    vertical-align: middle;
    font-size: 1.25em;
    margin: 10px;
 }

@media (max-width: 767.98px){
	.div-quadrado{
		width: auto;	
	}	
}
@media (min-width: 768px) and (max-width: 991.98px){
	.div-quadrado{
		width: 230px;	
		padding: 60px 15px;
	}	
}

.apresenta {background-color:#F1F1F2; height:22em;background:url(http://www.2s.com.br/wp-content/themes/2s/assets/img/solutions/bg-solucoes.jpg)  no-repeat;
    background-size: cover;
    background-position: center center;}
.abaixoresolve {background-color:#F1F1F2; width:100%;}
.abaixoresolve {width:100%; overflow:hidden; text-align:center}
.abaixoresolve h4 {font-weight:normal} 
.abaixoresolve .cinza {font-weight:normal; color: #808285}
.abaixoresolve .vermelho {color:#FF5A44}
.abaixoresolve .azul {color:#0071BB}
.servicosabaixo {text-align:center; padding-bottom:1.87em}
.servicosabaixo #servicoscont{ max-width:81.25em; margin: 0 auto; padding-top:1.87em }
.servicosabaixo a:link, a:visited, a:active {text-decoration: none; color:#000}
.servicosabaixo a:hover {text-decoration: underline; color: #0071BB}
.servicosicos {max-width:7.5em; padding-left:1.5em; padding-right:1.5em}
.servicosabaixo .icos {display:inline-block; padding-left:0%;}
.servicosabaixo .icos li {list-style-type:none; float:left;}

.TabControl { margin-top:-50px; padding-bottom: 30px;}
.TabControl #icone {width:120px; margin: 0 auto}
.TabControl #header1{ max-width:71.875em; margin: 0 auto; }
.TabControl #header{ width:100%; overflow:hidden; cursor:pointer}
.TabControl #content{ width:100%;overflow:hidden;}
.TabControl .abas {display:inline-block; padding-left:0%;}
.TabControl .abas li {list-style-type:none; float:left;}
.aba{padding-left:3.43em; padding-right:3.43em}
.ativa{padding-left:3.43em; padding-right:3.43em;}
.ativa span, .selected span{background-color:#F1F1F2}
.TabControl .conteudo{max-width:68.75em; padding-top:3.75em; background:#fff; display:none; margin:0 auto}
.TabControl .contimg{display:inline; float:left; padding-right:3.75em; max-width:37.1875em}
.TabControl .contxt{display:inline; float:left; padding-right:3.75em; max-width:24.0625em}

.internascont{ margin-right: 40px;display: table; }

@media (max-width: 767.98px){
	.internascont{
		margin-right: 0;	
	}	
}
@media (min-width: 768px) and (max-width: 991.98px){
	.internascont{
		margin-right: 33px;	
	}	
}

.internascont .imgs img{
	max-width: unset;
}

.internascont .imgs{display:inline; float:left;padding-right: 0;max-width: 31.1875em;}
.internascont .texto {}
.internascont .texto2 {margin-top:100px;margin:0 auto}
.internascont .colacentral {text-align:center;}
.internascont .colacentral p {margin:30px 0px;font-size: 1.2em; font-weight: 100;}

.internascont .texto h1 {padding-top:0; margin-top: 20px}

.texto p, .texto2 p {margin:30px 0px;font-size: 1.2em; font-weight: 100;}

.internascont .caixas {max-width:100%; margin: 0 auto; text-align:center; display:table}

.internascont .caixas .linhaseg {width:46.875em; height:4.375em; padding: 0px 10px 0px 10px; border:solid 2px #00B59D; display:table-cell;  vertical-align:middle}
.internascont .caixas .linhaiot {width:46.875em; height:4.375em; padding: 0px 10px 0px 10px; border:solid 2px #004B5B; display:table-cell;  vertical-align:middle}
.internascont .caixas .linhadata {width:46.875em; height:4.375em; padding: 0px 10px 0px 10px; border:solid 2px #FF5A44; display:table-cell;  vertical-align:middle}
.internascont .caixas .linhamob {width:46.875em; height:4.375em; padding: 0px 10px 0px 10px; border:solid 2px #FF7B2F; display:table-cell;  vertical-align:middle}
.internascont .caixas .linhacola2 {width:46.875em; height:4.375em; padding: 0px 10px 0px 10px; border:solid 2px #FFC229; display:table-cell;  vertical-align:middle}
.internascont .caixas .linhacola {

    width: 22em;
    height: 16em;
    font-weight: 100;
    padding: 0px 15px;
    border: solid 2px #FFC229;
    display: table-cell;
    vertical-align: middle;
    font-size: 1.25em;
}
.slidercolab {margin:0 auto; display:table;  margin-bottom:60px; margin-top:60px}
.slidercolab .caixaslide{width:550px; height:350px;;  border:solid #FC0; text-align:center; vertical-align:middle; display:table-cell}
.portfoliocolab {width:100%;background-color:#FFB822; background:url(http://www.2s.com.br/wp-content/themes/2s/assets/img/solutions/bg-colaboracao.jpg) no-repeat; background-size: cover; background-position: center center;padding-top:10px; padding-bottom:10px; text-align:center}

.portfoliocolab p {font-size:17px; margin:30px 0;}
.portfoliocolab h1 {margin:30px 0;}


.licola {margin:0px 5px 0px 5px; float:left}
.licolaul {list-style-type:none; padding-left:0%}
.internascont ul {list-style-type:none; margin:0px 40px;}
.internascont li {margin-bottom:1.625em}
.parceiros{max-width:740px; background:#fff; text-align:center; margin:0 auto}
.parceiros .imgs{display:inline; float:left; padding-right:3.75em; max-width:37.1875em}
.parceiros .texto {max-width:385px; text-align:left; margin:0 auto}
.parceiros ul {list-style-type:none; padding-left:0%;}
.parceiros li {margin-bottom:1.625em}
.parceiros .caixas {max-width:440px; margin: 0 auto; text-align:left; display:table}
.parceiros .caixas .linha {height:6em; padding: 0px 10px 0px 20px; font-weight:bold; border-left:solid 2px #00ADEF; display:table-cell;  vertical-align:middle}
.autorizacao {max-width:100%px; background:#fff; text-align:center; margin:0 auto}
.autorizacao ul {list-style-type:none; padding-left:0%;}
.autorizacao li {margin-bottom:0.625em}
.autorizacao .caixas {margin: 0 auto; text-align:center; display:table}
.autorizacao .caixas .linhaaut {width:170px; height:175px; font-weight:bold; border:solid 4px #00ADEF; display:table-cell;  vertical-align:middle}
.premios {width:100%; height:200px; padding-top:20px; background-color:#F2F2F2; text-align:center}
.especializacoes {width:100%; height:200px; padding-top:20px; background-color:#FFF; text-align:center}
.selected{padding-left:3.43em; padding-right:3.43em}

.titcima {padding:30px 0px; margin-top: 0px; text-align:center; padding-top: 60px}
.titcima2 {padding:30px 0px; font-size:1.2em; font-style:italic; text-align:center; font-weight: 300;}

.tit1 {color:#FFC229}
.tit2 {color:#004B5B}
.tit3 {color:#FF7B2F}
.tit4 {color:#00B59D}
.tit5 {color:#FF5A44}
.tit6 {color:#00ADEF}
.tit7 {color:#000; font-size:32px}

.solucao button {border:0; color:#fff; width:290px; height:60px; border-radius: 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px; margin-top:50px; cursor:pointer; font-size: 1.1em;}

.botao1, .cor1 {background-color:#FFC229 !important; }
.botao2, .cor2 {background-color:#004B5B !important; }
.botao3, .cor3 {background-color:#FF7B2F !important; }
.botao4, .cor4 {background-color:#00B59D !important; }
.botao5, .cor5 {background-color:#FF5A44 !important; }
.botao6, .cor6 {background-color:#00ADEF !important; }

hr {
		height:1px; border:none; width:215px; text-align:center; margin: 0 auto;
}

.quadros {max-width:1050px; margin: 0 auto; text-align:center; display:table}
.quadros h1 {color: #00ADEF}
.quadrosbrancos {width:30%; height: 190px; border:10px solid #F1F1F2; background-color:#FFF; padding: 0px 50px 0px 50px; display: table-cell; vertical-align:middle;}
.lateralmenu {max-width:264px; float:right;}
.lateralmenu a:link, a:visited, a:active {text-decoration: none; color:#000}
.lateralmenu a:hover {text-decoration: underline; color: #0071BB}
.lateralmenu ul {list-style-type:none; float:left; padding-left:0%;}
.lateralmenu hr {border: none; background-color: #00ADEF; height: 1px; width:264px; margin-bottom:0%; float:left}
.lateralmenu .tabelamenu {width:264px; margin: 0 auto; text-align:left; display:table}
.lateralmenu .tabelamenu .iconecell {width:41px; display:table-cell; padding-top:1.25em; vertical-align:middle}
.lateralmenu .tabelamenu .menucell {width:100%; display:table-cell; padding-left:1.25em; padding-top:1.25em; vertical-align:middle}
.servicos {max-width:1280px; padding-top: 40px; text-align: center; margin: 0 auto }
.servicos ul {list-style-type:none; float:left; padding-left:0%; margin-top:0px; margin-bottom:5px}
.servicos hr {border: none; background-color: #00ADEF; height: 1px; width:264px; margin-bottom:0%; float:left}
.servicos .tabelaserv {max-width:860px; margin: 0 auto; text-align:left; display:table; float:left  }
.servicos .tabelaserv .iconecell {width:120px; display:table-cell; float:left; vertical-align:middle}
.servicos .tabelaserv .txtcell { display:table-cell; padding-left:1.25em; padding-top:1.25em; vertical-align:middle}
.servicos .tabelaserv h2{margin-bottom:0px}
.servicos .cinza {font-weight:normal; margin-top:0px; color: #808285}
.servicos .tabelaserv .txtcell li { list-style-image:url(imagens/square.gif)}
.servicos .titulo {max-width:820px; padding-left: 145px; float:left}

.carousel-header  {
    text-align:center;

}

.carousel-header h2 {
	padding: 30px 0;
}
#carousel {
	    height:400px;

}

.carousel-content {
    color:black;
    display:flex;
    /*align-items:center;*/
    text-align:center;
    height:250px;
    padding:20px;
}

.carousel-content > div {width: 100%}

.carousel-content h3 {
  margin-bottom:60px;
}

.carousel-content p {
  font-size:1.2em;
  font-weight:100;
}

/* Carousel Styles */
.carousel-indicators li {
    border:1px solid #cdcdcd;
    background-color: #fff;
}

/* Carousel Styles */
.carousel-indicators .active {
    background-color: #FFC229;
}

.icon {
    display: block;
    position: absolute;
    left: 0;
    right: 0;
    width: 31px;
    height: 17px;
    margin: 0 auto;
    text-align: center;
    cursor: pointer;
top:740px;
}
.icon.icon--arrow:before {
    content: '';
    display: block;
    position: absolute;
    left: 0;
    top: 6px;
    width: 20px;
    height: 5px;
    background: #ffffff;
    border-radius: 5px;
-webkit-transform: rotate(45deg);
    -moz-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    -o-transform: rotate(45deg);
    transform: rotate(45deg)
}

.icon.icon--arrow:after {
    content: '';
    display: block;
    position: absolute;
    right: 0;
    top: 6px;
    width: 20px;
    height: 5px;
    background: #ffffff;
    border-radius: 5px;
-webkit-transform: rotate(-45deg);
    -moz-transform: rotate(-45deg);
    -ms-transform: rotate(-45deg);
    -o-transform: rotate(-45deg);
    transform: rotate(-45deg);
}

@media screen and (max-width: 640px) {
.internascont {
padding-top:0px;
}

.internascont .imgs {padding:0px;}

	.aba{padding-left:0; padding-right:0}
	.ativa{padding-left:0; padding-right:0}
}

</style>


<?php } ?>

<?php get_footer(); ?>

<?php endwhile;?>
<?php endif ?>