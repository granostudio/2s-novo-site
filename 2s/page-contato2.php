<div class="col-md-7 mb-margin-bottom-30">
    <div class="headline"><h2>Formulário de Contato</h2></div>
    <p>Preencha o formulário de forma correta para que possamos entrar em contato o mais rápido possível.<br><small>* Itens obrigatórios</small></p>
    
    <div class="textoInterno" id="resposta"></div>
    <form id="formEdit" method="POST" class="sky-form contact-style">
    <input type="hidden" value="analyticsda2s@gmail.com" name="destinatario" />
    <input type="hidden" value="true" name="contato" />
        <fieldset id="formulario" class="no-padding">
            <label>Nome <span class="color-red">*</span></label>
            <div class="row sky-space-20">
                <div class="col-md-11 col-md-offset-0">
                    <div>
                        <input type="text" name="nome" id="nome" class="form-control">
                    </div>
                </div>
            </div>

            <label>E-mail <span class="color-red">*</span></label>
            <div class="row sky-space-20">
                <div class="col-md-11 col-md-offset-0">
                    <div>
                        <input type="text" name="email" id="email" class="form-control">
                    </div>
                </div>
            </div>

            <label>Telefone <span class="color-red">*</span></label>
            <div class="row sky-space-20">
                <div class="col-md-6 col-md-offset-0">
                    <div>
                        <input type="text" name="telefone" id="telefone" class="form-control">
                    </div>
                </div>
            </div>

            <label>Assunto <span class="color-red">*</span></label>
            <div class="row sky-space-20">
                <div class="col-md-6 col-md-offset-0">
                    <div>
                        <input type="text" name="assunto" id="assunto" class="form-control">
                    </div>
                </div>
            </div>

            <label>Mensagem <span class="color-red">*</span></label>
            <div class="row sky-space-20">
                <div class="col-md-11 col-md-offset-0">
                    <div>
                        <textarea rows="8" name="mensagem" id="mensagem" class="form-control"></textarea>
                    </div>
                </div>
            </div>
    <div id="capatcha">
    <div class="g-recaptcha" data-sitekey="6LcrUQcUAAAAAJl_CzudrmPQYNTh79IqHxwYwNCx"></div>
    </div>
            <br>
            <p><button type="submit" class="btn-u">Enviar</button></p>
        </fieldset>
    </form>
</div><!--/col-md-9-->

<div class="col-md-5">
    <!-- Localização -->
    <div class="headline"><h2>Localização</h2></div>
        <!-- Google Map -->
        <div id="map" class="map" style="height: 250px"></div><!---/map-->
        <!-- End Google Map -->
    <!-- End Localização -->

    <!-- Contacts -->
    <div class="headline"><h2>Contatos</h2></div>
    <ul class="list-unstyled margin-bottom-30">
        <li><i class="fa fa-home"></i> Rua Arizona, 1366, 10º andar</li>
        <li><i class="fa fa-home"></i> CEP 04567-900 | São Paulo - SP</li>
        <li><i class="fa fa-envelope"></i> atendimento@2s.com.br</li>
        <li><i class="fa fa-phone"></i> +55 11 3305.1200</li>
    </ul>

    <div class="headline"><h2>Redes Sociais</h2></div>
    <ul class="list-inline">
        <li>
            <a href="https://www.facebook.com/2SInovacoesTecnologicas" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook" target="_blank">
                <i class="fa fa-2x fa-facebook-square"></i>
            </a>
        </li>
        <li>
            <a href="https://www.youtube.com/user/2sinovacoes" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Youtube" target="_blank">
                <i class="fa fa-2x fa-youtube-square"></i>
            </a>
        </li>
        <li><a href="https://www.linkedin.com/company/2s-inova-es-tecnol-gicas" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Linkedin" target="_blank"><i class="fa fa-2x fa-linkedin-square"></i></a></li>
    </ul>
    <!-- End Contacts -->

    <!-- Trabalhe Conosco -->
    <div class="headline"><h2>Trabalhe Conosco</h2></div>
    <div class="thumbnails thumbnail-style">
        <a class="img-responsive" title="Trabalhe Conosco" href="/trabalhe-conosco">
            <img class="img-responsive" src="/wp-content/themes/2s/assets/img/main/trabalhe_conosco.png" alt="">
        </a>
    </div>
    <!-- End Trabalhe Conosco -->
</div><!--/col-md-3-->