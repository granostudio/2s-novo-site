<?php if (have_posts()): ?>
<?php while ( have_posts() ) : the_post(); ?>

<?php get_header(); ?>

<!--=== Breadcrumbs v3 ===-->
    <!--=== Breadcrumbs v3 ===-->
    <div class="hidden-xs breadcrumbs breadcrumbs-light img-parceria">&#160;
        <span class="icon icon--arrow icon--arrow-down"></span>
    </div>
    <!--=== End Breadcrumbs v3 ===-->
<!--=== End Breadcrumbs v3 ===-->

<?php the_content(); ?>
<?php get_footer(); ?>

<?php endwhile;?>
<?php endif ?>