<?php get_header(); ?>


    <?php if (have_posts()): ?>
    <?php while ( have_posts() ) : the_post(); 
    ?>

    <!--=== Breadcrumbs v3 ===-->
    <div class="breadcrumbs breadcrumbs-light img-<?php the_slug(); ?>">
        <div class="container">
            <div class="titulos-breadcrumbs">
                <h1><?php the_title() ?></h1>
            </div>
        </div>
    </div>
    <!--=== End Breadcrumbs v3 ===-->


    <!--=== Content Part ===-->
    <div class="container">
        <div class="row margin-bottom-30">
            
        <?php the_content(); ?>

        </div><!--/row-->

       
    </div><!--/container-->
    <!--=== End Content Part ===-->


    <?php endwhile;?>
    <?php endif ?>

			
<?php get_footer(); ?>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
        StyleSwitcher.initStyleSwitcher();
    });
</script>
