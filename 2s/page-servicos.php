<?php get_header(); ?>
    <!--=== Breadcrumbs v3 ===-->
    <div class="hidden-xs breadcrumbs breadcrumbs-light img-servicos">&#160;
        <span class="icon icon--arrow icon--arrow-down"></span>
    </div>
    <!--=== End Breadcrumbs v3 ===-->

    <?php if (have_posts()): ?>
    <?php while ( have_posts() ) : the_post(); ?>

    <!--=== Container Part ===-->
    <div class="container">
        <div class="row">
            <div class="col-md-9">

                <div class="row">
                    
                    <?php the_content(); ?>

                    <br/>
                    <div class="margin-bottom-40"></div>
                </div>


            </div><!-- End col-9 -->
            <div class="col-md-3">
<?php
/*
<style>
.headline.solucoes {margin: 0px 0px 25px 0;}
.lateralmenu {width:264px; }
.lateralmenu a:link, .lateralmenu a:visited, .lateralmenu a:active {text-decoration: none; color:#000}
.lateralmenu a:hover {text-decoration: underline; color: #0071BB}
.lateralmenu ul {list-style-type:none; float:left; padding-left:0%;}
.lateralmenu hr {border: none; background-color: #00ADEF; height: 1px; width:264px; margin-bottom:0%; float:left}
.lateralmenu .tabelamenu {width:264px; margin: 0 auto; text-align:left; display:table}
.lateralmenu .tabelamenu .iconecell {width:41px; display:table-cell; padding-top:1.25em; vertical-align:middle}
.lateralmenu .tabelamenu .menucell {width:100%; display:table-cell; padding-left:1.25em; padding-top:1.25em; vertical-align:middle}
</style>
<div class="lateralmenu">
<div class="headline solucoes">
<h2>SOLUÇÕES</h2>
</div>
<div class="tabelamenu">
<ul>
<li>
<div class="iconecell"> <a href="/solucoes/colaboracao"><img src="/wp-content/themes/2s/assets/img/icocolabora.gif" alt="Colaboração" /></a></div>
<div class="menucell"> <a href="/solucoes/colaboracao">Colaboração</a></div></li>
<li>
<div class="iconecell"> <a href="/solucoes/iot"><img src="/wp-content/themes/2s/assets/img/icoiot.gif" alt="Internet das coisas" /></a></div>
<div class="menucell"> <a href="/solucoes/iot">Internet das coisas</a></div></li>
<li>
<div class="iconecell"> <a href="/solucoes/mobilidade"><img src="/wp-content/themes/2s/assets/img/icomobilidade.gif" alt="Mobilidade" /></a></div>
<div class="menucell"> <a href="/solucoes/mobilidade">Mobilidade</a></div></li>
<li>
<div class="iconecell"> <a href="/solucoes/seguranca"><img src="/wp-content/themes/2s/assets/img/icoseguranca.gif" alt="Segurança" /></a></div>
<div class="menucell"> <a href="/solucoes/seguranca">Segurança</a></div></li>
<li>
<div class="iconecell"> <a href="/solucoes/datacenter"><img src="/wp-content/themes/2s/assets/img/icodatacenter.gif" alt="Datacenter" /></a></div>
<div class="menucell"> <a href="/solucoes/datacenter">Datacenter</a></div></li>
</ul></div>

</div>
*/
?>

                <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Sidebar Serviços") ) : ?>
                <?php get_sidebar("Sidebar Serviços"); ?> 
                <?php endif; ?>
            </div><!-- End col-3 -->

        </div> <!-- End row--> 
    </div>
    
    </div>
    <!--=== End Container Part ===-->
    <?php endwhile;?>
    <?php endif ?>

    <?php
        global $post;
        $slug = get_post( $post )->post_name;
        $tag = get_term_by('slug', $slug, 'post_tag');   
        if ($tag) {
    ?>

    <div class="container" style="margin-bottom:50px;">
        <div class="row">
            <div class="col-md-12">

                <div class="relatedposts" >
                    <h1 class="tit7 text-center" style="margin-top:30px; margin-bottom:30px">Artigos Relacionados</h1>
                
                    <hr style="background-color:#006ebe; margin-bottom:30px;" />

                    <?php
                        $args=array(
                            'tag__in' => $tag->term_id,
                            'posts_per_page'=>3,
                            'caller_get_posts'=>1
                        );
                         
                        $my_query = new wp_query( $args );
                     
                        while( $my_query->have_posts() ) {
                        $my_query->the_post();
                     ?>
                         
                    <div class="relatedthumb col-md-4 text-center">
                        <a rel="external" href="<? the_permalink()?>"><?php the_post_thumbnail(array(300, 225)); ?><br />
                        <h4 style="margin-top:20px; margin-bottom:20px;"><?php the_title(); ?></h4>
                        </a>
                        <hr style="background-color:#006ebe" style="margin-bottom:30px;" />
                    </div>
                         
                    <? }
                    $post = $orig_post;
                    wp_reset_query();
                    ?>
                </div>
            </div>
        </div>
    </div>

    <?php } ?>



<?php get_footer(); ?>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
        StyleSwitcher.initStyleSwitcher();
    });
</script>
