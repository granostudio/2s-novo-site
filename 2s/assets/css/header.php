<?php
    global $page, $paged, $user_ID, $user_identity, $current_user, $wp_roles, $post, $category;
    get_currentuserinfo();

    $category = get_the_category();

    if (! (is_page("trabalhe-conosco") || is_page("blog")/* || is_category() || is_single()*/ ) || (is_single() && is_singular('clientes') ) || (is_single() && is_singular('conteudos') )  || (is_single() && is_singular('clippings') )  || (is_single() && is_singular('noticias') )  ) {
?>
    <!DOCTYPE html>
    <!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
    <!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
    <!--[if !IE]><!--> <html lang="pt-BR"> <!--<![endif]-->
    <head>
        <title><?php
    		wp_title( '|', true, 'right' );
    		bloginfo( 'name' );
        ?></title>


        <!-- Meta -->
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="2S Tecnologia" />
        <meta name="google-site-verification" content="kxWpeZnTRR-qFKcqZJr4IOcD7EWDNXIUAAF7WH1aYrM" />
        <meta name="author" content="2s.com.br" />
        <meta name="robots" content="index, follow">

        <?php if (is_front_page()) { ?>

            <meta content="2S, Gestão do Parque de TI, Infraestrutura de TI, Mobilidade, IoT, Internet das Coisas, Segurança, Datacenter, Serviço infraestrutura, Manutenção de hardware e software, Requisição de Serviços, Monitoramento – NOC, Gerenciamento remoto, Gerenciamento onsite, Meraki as a Service, Prime as a Service" name="keywords" />

            <meta content="A 2S é uma integradora de soluções de infraestrutura, sendo uma das principais parceiras Gold e 1st Tier Cisco no Brasil." name="description" />
            <meta property="og:locale" content="pt_BR">
            <meta content="Transforme sua empresa com tecnologia | 2S" property="og:title" />
            <meta content="http://www.2s.com.br" property="og:url" />
            <meta content="2S" property="og:site_name" />
            <meta content="http://www.2s.com.br/wp-content/uploads/2016/10/video-entrevista.jpg" property="og:image" />


        <?php } else { ?>

            <meta content="<?php
                $post_tags = get_the_tags();

                if ( $post_tags ) {
                    foreach( $post_tags as $tag ) {
                    echo $tag->name . ', ';
                    }
                }
            ?>" name="keywords" />

            <meta content="<?php echo get_the_excerpt( $post ) ?>" name="description" />

            <meta property="og:locale" content="pt_BR">
            <meta content="<?php echo get_the_title();?>" property="og:title" />
            <meta content="<?php echo get_permalink(); ?>" property="og:url" />
            <meta content="<?php wp_title( '|', true, 'right' ); bloginfo( 'name' );?>" property="og:site_name" />
            <meta content="<?php echo (wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' )[0]) ? wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' )[0] : "http://www.2s.com.br/wp-content/uploads/2016/10/video-entrevista.jpg" ; ?>" property="og:image" />


        <?php } ?>

        <meta content="<?php
            $post_tags = get_the_tags();

            if ( $post_tags ) {
                foreach( $post_tags as $tag ) {
                echo $tag->name . ', ';
                }
            }
        ?>" name="keywords" />

        <meta content="<?php echo get_the_excerpt( $post ) ?>" name="description" />

        <meta property="og:locale" content="pt_BR">
        <meta content="<?php echo get_the_title();?>" property="og:title" />
        <meta content="<?php echo get_permalink(); ?>" property="og:url" />
        <meta content="<?php wp_title( '|', true, 'right' ); bloginfo( 'name' );?>" property="og:site_name" />
        <meta content="<?php echo (wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' )[0]) ? wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' )[0] : "http://www.2s.com.br/wp-content/uploads/2016/10/video-entrevista.jpg" ; ?>" property="og:image" />


        <!-- Favicon -->
        <link rel="shortcut icon" href="<?php bloginfo("template_url") ?>/assets/img/simbol-2s.png">

        <!-- Web Fonts -->
        <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

        <!-- CSS Global Compulsory -->
        <link rel="stylesheet" href="<?php bloginfo("template_url") ?>/assets/plugins/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="<?php bloginfo("template_url") ?>/assets/css/style.css">
        <link rel="stylesheet" href="<?php bloginfo("template_url") ?>/style.css">
        <link rel="stylesheet" href="<?php bloginfo("template_url") ?>/style-3.css?v=3" >

        <!-- CSS Header and Footer -->
        <link rel="stylesheet" href="<?php bloginfo("template_url") ?>/assets/css/headers/header-v6.css">
        <link rel="stylesheet" href="<?php bloginfo("template_url") ?>/assets/css/footers/footer-v1.css">

        <!-- CSS Implementing Plugins - Home -->
        <link rel="stylesheet" href="<?php bloginfo("template_url") ?>/assets/plugins/animate.css">
        <link rel="stylesheet" href="<?php bloginfo("template_url") ?>/assets/plugins/line-icons/line-icons.css">
        <link rel="stylesheet" href="<?php bloginfo("template_url") ?>/assets/plugins/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php bloginfo("template_url") ?>/assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="<?php bloginfo("template_url") ?>/assets/plugins/layer-slider/layerslider/css/layerslider.css">

        <!-- CSS Implementing Plugins - Pages -->
        <link rel="stylesheet" href="<?php bloginfo("template_url") ?>/assets/plugins/cube-portfolio/cubeportfolio/css/cubeportfolio.min.css">
        <link rel="stylesheet" href="<?php bloginfo("template_url") ?>/assets/plugins/cube-portfolio/cubeportfolio/custom/custom-cubeportfolio.css">

        <!-- CSS Theme -->
        <link rel="stylesheet" href="<?php bloginfo("template_url") ?>/assets/css/theme-colors/dark-blue.css" id="style_color">
        <!-- <link rel="stylesheet" href="assets/css/theme-skins/dark.css"> -->

        <!-- CSS Customization -->
        <link rel="stylesheet" href="<?php bloginfo("template_url") ?>/assets/css/custom.css">
        <link rel="stylesheet" href="<?php bloginfo("template_url") ?>/assets/css/queries.css">

        <?php wp_head(); ?>
    </head>

    <body class="header-fixed header-fixed-space">

    <div class="wrapper">
        <!--=== Header v6 ===-->
        <div class="header-v6 header-classic-white header-sticky">
            <!-- Navbar -->
            <div class="navbar mega-menu" role="navigation">
                <div class="container container-space">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="menu-container">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <!-- Navbar Brand -->
                        <div class="navbar-brand">
                            <a href="/">
                                <img class="shrink-logo" src="<?php bloginfo("template_url") ?>/assets/img/logo-2s.png" alt="Logo">
                            </a>
                        </div>
                        <!-- ENd Navbar Brand -->
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse navbar-responsive-collapse">
                        <?php
                        /*
                        <div class="menu-container">
                        <ul class="nav navbar-nav">
                        <li><a href="/empresa">Empresa</a></li>
                        <li><a href="/solucoes">Soluções</a></li>
                        <li><a href="/servicos">Serviços</a></li>
                        <li><a href="/parcerias">Parcerias</a></li>
                        <li><a href="/cases">Cases</a></li>
                        <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        Conteúdos
                        </a>
                        <ul class="dropdown-menu">
                        <li><a href="/blog">Blog</a></li>
                        <li><a href="/noticias">Notícias</a></li>
                        <li><a href="/conteudos">Vídeos e E-books</a></li>
                        </ul>
                        </li>
                        <li><a href="/contato">Contato</a></li>
                        </ul>

                        </div>
                        */
                        ?>

                        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("sidebar-8") ) : ?>
                        <?php get_sidebar("sidebar-8"); ?>
                        <?php endif; ?>

                    </div><!--/navbar-collapse-->
                </div>
            </div>
            <!-- End Navbar -->
        </div>
        <!--=== End Header v6 ===-->

<?php } elseif(is_page("trabalhe-conosco")) { ?>

<!DOCTYPE html>

<html>
<head>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet'>
    <meta charset="utf-8" />
    <meta name="google-site-verification" content="kxWpeZnTRR-qFKcqZJr4IOcD7EWDNXIUAAF7WH1aYrM" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title><?php
            wp_title( '|', true, 'right' );
            bloginfo( 'name' );
        ?></title>
    <meta name="description" content="Trabalhe na 2S" />
    <link rel="shortcut icon" href="http://www.2s.com.br/wp-content/themes/2s/assets/img/simbol-2s.png"  type="image/x-icon">

    <link href="<?php bloginfo("template_url") ?>/oportunidades/css/plugins.min.css" rel="stylesheet">
    <link href="<?php bloginfo("template_url") ?>/oportunidades/css/main.css" rel="stylesheet">
</head>
<body>


<?php } else { ?>

    <!doctype html>
    <html data-fixed-nav="true">

    <head>

        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="google-site-verification" content="kxWpeZnTRR-qFKcqZJr4IOcD7EWDNXIUAAF7WH1aYrM" />
        <meta name="author" content="2s.com.br" />

        <meta name="robots" content="index, follow">

        <meta content="<?php
            $post_tags = get_the_tags();

            if ( $post_tags ) {
                foreach( $post_tags as $tag ) {
                echo $tag->name . ', ';
                }
            }
        ?>" name="keywords" />

        <meta content="<?php echo (get_the_excerpt( $post )!='') ? get_the_excerpt( $post ) : 'Blog da 2S' ; ?>" name="description" />

        <meta property="og:locale" content="pt_BR">
        <meta content="<?php echo get_the_title();?>" property="og:title" />
        <meta content="<?php echo get_permalink(); ?>" property="og:url" />
        <meta content="<?php wp_title( '|', true, 'right' ); bloginfo( 'name' );?>" property="og:site_name" />
        <meta content="<?php echo (wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' )[0]) ? wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' )[0] : "http://www.2s.com.br/wp-content/uploads/2016/10/video-entrevista.jpg" ; ?>" property="og:image" />


        <title>
            <?php

            wp_title( '|', true, 'right' );
            echo "Blog 2S";

            ?></title>
        </title>

        <!-- Favicon -->
        <link rel="shortcut icon" href="<?php bloginfo("template_url") ?>/assets/img/simbol-2s.png">


        <!-- FONTS -->
        <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Clicker+Script' rel='stylesheet' type='text/css'>

        <!--[if lte IE 9]>
            <script src="<?php bloginfo("template_url") ?>/blog/js/html5shiv.js"></script>
            <script src="<?php bloginfo("template_url") ?>/blog/js/selectivizr-min.js"></script>
        <!--[endif]-->

        <!-- STYLES -->
        <link rel="stylesheet" type="text/css" media="print" href="<?php bloginfo("template_url") ?>/blog/css/print.css">
        <link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url") ?>/blog/css/grid.css">
        <link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url") ?>/blog/css/style.css">
        <link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url") ?>/blog/css/normalize.css">
        <link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url") ?>/blog/css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url") ?>/blog/js/google-code-prettify/prettify.css">
        <link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url") ?>/blog/css/uniform.default.css">
        <link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url") ?>/blog/css/main.css">
        <link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url") ?>/blog/css/flexslider.css">

        <link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url") ?>/blog/js/mediaelement/mediaelementplayer.css">
        <link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url") ?>/blog/css/jquery.fancybox-1.3.4.css">


        <link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url") ?>/blog/css/custom.css">

        <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>">

        <?php wp_head(); ?>
    </head>

    <body>
    <!-- #PAGE -->
    <div id="page" class="hfeed site">

      <!-- .site-header -->
      <header class="site-header wrapper" role="banner">

        <!-- .row -->
        <div class="row">



              <!-- .site-title -->
              <hgroup>
                <h1 class="site-title">
                <a href="/" title="Read" rel="home"><img class="shrink-logo" src="<?php bloginfo("template_url") ?>/assets/img/logo2s-blog.png" alt="Logo"></a> /
                <a href="/blog" title="Read" rel="home">Blog</a>
<?php
            if ($category) {
                echo " / " . $category[0]->name;
            }
?>

                </h1>
              </hgroup>
              <!-- .site-title -->



             <!-- social icons -->
             <ul class="social">
                <li><a class="facebook" href="https://www.facebook.com/2SInovacoesTecnologicas" target="_blank">&#88;</a></li>
                <li><a class="youtube" href="https://www.youtube.com/user/2sinovacoes" target="_blank">&#39;</a></li>
                <li><a class="linkedin" href="https://www.linkedin.com/company/2s-inova-es-tecnol-gicas" target="_blank">&#118;</a></li>
            </ul>
            <!-- social icons -->


            <!-- #site-navigation -->

              <nav id="site-navigation" class="main-navigation clearfix" role="navigation">
                <ul>
                  <li><a href="/blog/colaboracao"
                    <?php if ($category[0]->slug=="colaboracao"): ?>
                      class="active"
                    <?php endif ?>
                  >Colaboração</a>
                  </li>
                  <li><a href="/blog/iot"
                    <?php if ($category[0]->slug=="iot"): ?>
                      class="active"
                    <?php endif ?>
                  >IoT</a>
                  </li>
                  <li><a href="/blog/mobilidade"
                    <?php if ($category[0]->slug=="mobilidade"): ?>
                      class="active"
                    <?php endif ?>
                  >Mobilidade</a>
                  </li>
                  <li><a href="/blog/seguranca"
                    <?php if ($category[0]->slug=="seguranca"): ?>
                      class="active"
                    <?php endif ?>
                  >Segurança</a>
                  </li>
                  <li><a href="/blog/datacenter"
                    <?php if ($category[0]->slug=="datacenter"): ?>
                      class="active"
                    <?php endif ?>
                  >Datacenter</a></li>
                  <li><a href="/blog/institucional"
                    <?php if ($category[0]->slug=="institucional"): ?>
                      class="active"
                    <?php endif ?>
                  >Institucional</a></li>
                  <li><a href="/blog/servicos"
                    <?php if ($category[0]->slug=="servicos"): ?>
                      class="active"
                    <?php endif ?>
                  >Serviços</a></li>
                </ul>
              </nav>
              <!-- #site-navigation -->

        </div>
        <!-- .row -->

      </header>
      <!-- .site-header -->

<?php } ?>
