<?php if (have_posts()): ?>
<?php while ( have_posts() ) : the_post(); ?>

<?php get_header(); ?>

<div class="pagina-nova">

  <div class="header3">
    <h1>Papel e Celulose</h1>
  </div>

  <!-- <div class="banner-papel-celulose">
      <p>
        O Brasil é um dos maiores produtores de papel e celulose do mundo. Falamos de uma indústria em plena expansão, e esse crescimento depende fundamentalmente da tecnologia e da criação de níveis elevados de conectividade e automação de negócios.
      </p>
  </div> -->

  <div class="video-background" style="position:relative" height="500px;" id="video-background-id">

    <video autoplay loop muted width="100%" id="video-banner">
      <source src="<?php echo get_stylesheet_directory_uri();?>/2S-Transformação-Digital-Papel-e-Celulose.mp4" type="video/mp4">
    </video>


    <div class="btn-banner">
    </div>
    <div class="btn-banner2"><p style="text-align: center;color: white;font-size: 20px;padding-top: 7px;">Assista ao video</p></div>

  </div>

  <div class="container">
    <div class="row col-sm-10 col-sm-offset-1">
      <div class="col-sm-12" style="margin-top: 50px;">
        <p>O Brasil é um dos maiores produtores de papel e celulose do mundo. Falamos de uma indústria em plena expansão, e esse crescimento depende fundamentalmente
        da tecnologia e da criação de níveis elevados de conectividade e automação de negócios.</p>

        <!-- Sessão problemas  -->
        <div class="headline" style="margin-top: 50px;">
          <h2 style="font-size:24;">Problema</h2>
        </div>
        <ul class="lista">
          <li>Nos próximos cinco anos, 72% das empresas do setor esperam atingir <font color="#2977BA">níveis avançados de digitalização</font> (hoje, 38% afirmam que já vivenciam esse estágio). É o que diz o estudo “2016 Global Industry 4.0” da consultoria PwC, realizado com 73 altos executivos do segmento em 26 países. Trata-se de um movimento desafiador, que exige uma relação estreita entre a TI e as áreas de negócio da companhia.</li>
          <li>Depender de um único mercado consumidor é uma das maiores ameaças ao crescimento das indústrias de papel e celulose. Por isso, a <font color="#2977BA">diversificação de portfólio</font> mostra-se um caminho essencial para o crescimento, incluindo o uso de novas fibras e o desenvolvimento de papéis especiais, por exemplo.</li>
          <li>Assim como em outras grandes indústrias, a criação de <font color="#2977BA">ambientes de trabalho seguros</font>, a gestão das cadeias de fornecimento e logística, a <font color="#2977BA">eficiência de processos</font> de produção e as pressões regulatórias também são desafios constantes para os players do setor.</li>
        </ul>
      </div>
        <!-- Sessão problemas  -->

        <!-- Sessão soluções  -->
      <div class="col-sm-12">
        <div class="headline">
          <h2>Soluções</h2>
        </div>
        <p>As soluções tecnológicas da 2S para a vertical de papel e celulose foram pensadas a partir do entendimento dos principais pilares e desafios de negócio do setor, de forma a tornar a <font color="#2977BA">tecnologia e a transformação digital propulsoras do crescimento</font> dessas empresas:</p>
        <div class="col-sm-12 box-opcoes">
          <div class="item-p item1">
            <h4>Logística Interna<br/>Inteligente</h4>
            <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/papel-e-celulose/logistica.png" alt="">
          </div>
          <div class="item-p item2">
            <h4>Ambiente<br/>Seguro</h4>
            <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/papel-e-celulose/ambiente.png" alt="">
          </div>
          <div class="item-p item3">
            <h4>Floresta<br/>Conectada</h4>
            <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/papel-e-celulose/floresta.png" alt="">
          </div>
          <div class="item-p item4">
            <h4>Back<br/>Office</h4>
            <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/papel-e-celulose/back-office.png" alt="">
          </div>
          <div class="content-out3">
            <div class="content content1">
              <div class="triangulo"></div>
              <p>Da extração dos eucaliptos na floresta até a distribuição dessa matéria-prima, passando pela entrada nas fábricas e, finalmente, pela gestão das empilhadeiras que embarcam os blocos de celulose, já como produto final, nos trens e caminhões, a solução de “Logística Interna Inteligente” permite a <font color="#2072B7">inteligência de gestão de todos os veículos e máquinas</font>, próprios ou de terceiros.</p>
              <p>Com a solução, os condutores passam a ter visibilidade total das frentes em que trabalham, de suas atividades e do trajeto que devem seguir. Além disso, podem receber treinamentos onboard e interagir com gestores por meio de voz e texto.</p>
              <p>Os benefícios dessa solução incluem a <font color="#2072B7">otimização da utilização dos recursos</font>, a diminuição dos <font color="#2072B7">custos de combustível e manutenção</font>, a <font color="#2072B7">contenção de paradas</font> não programadas e a <font color="#2072B7">redução de filas e acidentes</font> nas fábricas e nos centros de distribuição. Tudo isso a partir da geração de informações e relatórios em tempo real.</p>
            </div>
            <div class="content content2">
              <div class="triangulo"></div>
              <p>Pessoas, máquinas, empilhadeiras e veículos, todos se movimentando pelo mesmo ambiente. Essa é uma realidade das indústrias de papel e celulose, seja na poda dos eucaliptos, na fabricação do produto final ou no embarque das mercadorias nos trens e caminhões. Por isso, a solução que batizamos de “Workplace Safety” busca <font color="#FF7C2F">minimizar os riscos e acidentes de trabalho</font>.</p>
              <p>Por meio da identificação e do rastreamento em tempo real de pessoas, veículos e máquinas, a solução permite a <font color="#FF7C2F">visibilidade total da planta e dos colaboradores em áreas de risco</font>, criando alertas de aproximação, visuais e sonoros, e outras ações conforme a necessidade da operação. A partir de tecnologias com a de RFID, os gestores podem restringir o acesso dos funcionários, saber quanto tempo permaneceram no ambiente e se estão utilizando dos os EPIs necessários.</p>
            </div>
            <div class="content content3">
              <div class="triangulo"></div>
              <p>A necessidade de conectividade e automação chegou ao campo. Para os gestores das empresas do setor, <font color="#01B59D">obter informações em tempo real, da silvicultura à colheita</font>, é fundamental para uma gestão eficiente da principal matéria-prima utilizada nessas indústrias.</p>
              <p>Com a solução “Floresta Conectada”, conseguimos extrair e integrar dados durante toda a fase de plantio e poda, identificando, por exemplo, a rota de operação das máquinas e os motivos de posseiveis desvios, a quantidade e as áreas em que os fertilizantes e defensivos foram depositados, o tempo de utilização das máquinas, as condições do solo, as dimensões das árvores, entre outros. Para isso, uma série de <font color="#01B59D">sensores e outros dispositivos</font> são estrategicamente agregados à floresta.</p>
              <p>A cada projeto, contamos com um ecossistema de parceiros que fornecem softwares e aplicações, além de <font color="#01B59D">conectividade por satélites</font> em áreas sem cobertura wi-fi e celular, o que permite a extração e gestão dos dados obtidos de forma dinâmica e inteligente.</p>
            </div>
            <div class="content content4">
              <div class="triangulo"></div>
              <p>Além das soluções de negócio, a 2S olha para dentro da casa do cliente, a fim de compor projetos para <font color="#FF5A45">otimização dos recursos internos, eficiência de processos e qualidade da tomada de decisão.</font> As soluções incluem:</p>
              <p><font color="#FF5A45">Engajamento de times,</font> a partir da criação de ambientes de trabalho colaborativos e flexíveis, treinamentos on demand e comunicação estreita entre gestores e times; eficiência dos processos de recrutamento e seleção; e mais segurança em ambientes restritos.</p>
              <p><font color="#FF5A45">Domínio do ambiente,</font> com soluções que otimizam processos, a utilização dos recursos e a disponibilidade de rede; permitem a visão, em tempo real, da variação de produtos e processos, o planejamento end-to-end e a colaboração entre áreas; entre outras funções.</p>
              <p><font color="#FF5A45">Ambientes mais seguros,</font> por meio de soluções de segurança que bloqueiam invasões e vazamento de informações; monitoram e gerenciam as ameaças, reduzindo custos e esforços; e garantem segurança à inovação, aos processos e às estratégias da empresa.</p>
            </div>
          </div>

        </div>
      </div>
      <!-- Sessão soluções  -->

       </div>

       <!-- Sessão Ganhos -->
       <div class="row col-sm-10 col-sm-offset-1">
       <div class="col-sm-12">
         <div class="headline">
           <h2 style="font-size:24;">Ganhos</h2>
         </div>
       </div>
       <div class="col-sm-6">
         <ul class="lista">
           <li><font color="#2977BA">Otimização e redução do tempo</font> ocioso de veículos e máquinas, com rastreamento e monitoramento da frota em tempo real;</li>
           <li>Maior <font color="#2977BA">previsibilidade ao negócio</font>, a partir da geração de informações, em tempo real, de todos os processos produtivos, do plantio à extração da matéria-prima e fabricação do produto final;</li>
           <li><font color="#2977BA">Controle de custos e produção</font>, garantindo eficiência dos processos e da tomada de decisão pelos gestores da empresa;</li>
           <li><font color="#2977BA">Redução de acidentes de trabalho</font> e criação de ambientes seguros, com controle de acesso e rastreamento de colaboradores em áreas de risco;</li>
         </ul>
       </div>
       <div class="col-sm-6">
         <ul class="lista">
           <li>Criação de <font color="#2977BA">empresas realmente digitais</font>, que respondem com mais agilidade às mudanças do setor e da sociedade e, assim, diferenciam-se dos concorrentes;</li>
           <li>Redução de turnover e de custos com viagens, recrutamento e treinamento, além de maior <font color="#2977BA">transparência na comunicação </font>com o time;</li>
           <li>Ambientes físicos mais seguros, com <font color="#2977BA">redução de acidentes de trabalho e de perdas </font>relacionadas a problemas operacionais;</li>
           <li>Controle integrado e eficiente da <font color="#2977BA">segurança da informação</font>, permitindo a continuidade do negócio e a construção de uma boa reputação interna e externa.</li>
         </ul>
       </div>
       <!-- Sessão Ganhos -->

       <!-- <div class="col-sm-12 video-titulo">
         <p style="font-size:16px;"> <strong>Assista ao vídeo a seguir e saiba mais sobre como a tecnologia auxilia a área de papel e celulose</strong> </p>
       </div>
       <div class="col-sm-6">
         <div class="myIframe">
           <iframe src="//www.youtube.com/embed/VY1DMCx4XLg#action=share" allowfullscreen></iframe>
         </div>
       </div>
       <div class="col-sm-6 video-lista">

         <ul>
           <p style="font-size:16px;"><strong>Esse vídeo fala sobre:</strong></p>
           <li>Tecnologia + Marketing</li>
           <li>Conexão com clientes</li>
           <li>Transformação Digital</li>
           <li>Varejo Inovador</li>
           <li>Infraestrutura de TI</li>
           <li>Atendimento personalizado</li>
         </ul>
         <div style="clear:both;"></div>
       </div> -->


       <!-- <div class="col-sm-12 video-saibamais">
         <p style="font-size:16px;"><strong>Saiba mais</strong></p>
         <ul>
           <li>
             <div class="row">
               <div class="col-xs-8"><a href="http://www.2s.com.br/conteudo/e-book-transformacao-digital-o-futuro-das-empresas-e-as-empresas-do-futuro/" target="_blank">Ebook exclusivo: transformação digital e o varejo conectado</a></div>
               <div class="col-xs-4"><a href="" class="btn">Download</a></div>
             </div>
             </li>
           <li>
             <div class="row">
               <div class="col-xs-8"><a href="http://www.2s.com.br/transformacao-digital-3-frentes-de-atuacao-no-varejo/" target="_blank">Transformação digital: 3 frentes de atuação no varejo</a></div>
             </div>
           </li>
         </ul>
       </div> -->
     </div>

      </div>







<script>
  jQuery(' .item1 ').click(function() {
    $( ".content1" ).fadeIn( "slow" );
    $( ".content2" ).fadeOut( "slow" );
    $( ".content3" ).fadeOut( "slow" );
    $( ".content4" ).fadeOut( "slow" );
    $( ".content5" ).fadeOut( "slow" );
  });
  jQuery(' .item2 ').click(function() {
    $( ".content1" ).fadeOut( "slow" );
    $( ".content2" ).fadeIn( "slow" );
    $( ".content3" ).fadeOut( "slow" );
    $( ".content4" ).fadeOut( "slow" );
    $( ".content5" ).fadeOut( "slow" );
  });
  jQuery(' .item3 ').click(function() {
    $( ".content1" ).fadeOut( "slow" );
    $( ".content2" ).fadeOut( "slow" );
    $( ".content3" ).fadeIn( "slow" );
    $( ".content4" ).fadeOut( "slow" );
    $( ".content5" ).fadeOut( "slow" );
  });
  jQuery(' .item4 ').click(function() {
    $( ".content1" ).fadeOut( "slow" );
    $( ".content2" ).fadeOut( "slow" );
    $( ".content3" ).fadeOut( "slow" );
    $( ".content4" ).fadeIn( "slow" );
    $( ".content5" ).fadeOut( "slow" );
  });
  jQuery(' .item5 ').click(function() {
    $( ".content1" ).fadeOut( "slow" );
    $( ".content2" ).fadeOut( "slow" );
    $( ".content3" ).fadeOut( "slow" );
    $( ".content4" ).fadeOut( "slow" );
    $( ".content5" ).fadeIn( "slow" );
  });

  jQuery(' .btn-banner2 ').click(function() {
    $( ".btn-banner" ).remove();
    $( ".btn-banner2" ).remove();
    document.getElementById("video-banner").controls = true;
    document.getElementById("video-banner").currentTime = 0;
    document.getElementById("video-banner").muted = false;

  });
</script>

<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <div class="relatedposts">
        <h1 class="tit7 text-center" style="margin-top:30px; margin-bottom:30px">Artigos Relacionados</h1>
      </div>
    </div>
  </div>
</div>

<div class="container">

    <div class="margin-bottom-60"></div>

    <?php
    $query= null;
    $query = new WP_Query(
    array(
    'post_type' => 'destaque',
    'posts_per_page' => 6)
    );
    ?>

    <div class="row news-v2 margin-bottom-80">

    <?php
    if ( $query->have_posts() ) {
        while ( $query->have_posts() ) {
        $query->the_post();
        $custom             = get_post_custom($post->ID);
        $destaque_descricao    =   $custom["destaque_descricao"][0];
        $destaque_url          =   $custom["destaque_link"][0];
        $destaque_foto         =   $custom["destaque_foto"][0];
        // print_r($custom);

    ?>

        <div class="col-md-4" style="margin-bottom:20px;">
          <a href="<?php echo $destaque_url; ?>" style="text-decoration:none">
            <div class="news-v2-badge">
                <img class="img-responsive" style="width:100%;height: 227px;" src="<?php echo $destaque_foto; ?>" alt="">
                <p>
                    <img class="img-responsive" src="/wp-content/themes/2s/assets/img/simbol-2s-box.png" alt="">
                </p>
            </div>
            <div class="news-v2-desc2 post-border" id="post-border">
              <h2 style="margin-bottom:-50px"><?php echo $destaque_descricao; ?></strong></h2>
              <!-- <h2 class="entry-title" style="font-size: 19px;text-align: left;">
                  <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a>
              </h2> -->
            </div>
          </a>
        </div>

    <?php } } ?>
    </div>
</div>

<div class="container contato">
  <div class="row">
    <div class="col-sm-12">
      <div class="headline">
        <h2>Cadastre-se para ficar atualizado com a 2S</h2>
      </div>
      <div class="formulario">
        <?php echo do_shortcode('[contact-form-7 id="948" title="Cadastro Verticais"]'); ?>
      </div>
    </div>
  </div>
</div>

</div>

<?php the_content(); ?>
<?php get_footer(); ?>

<?php endwhile;?>
<?php endif ?>
