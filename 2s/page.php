<?php get_header(); ?>


    <?php if (have_posts()): ?>
    <?php while ( have_posts() ) : the_post(); 
    ?>

    <!--=== Breadcrumbs v3 ===-->
    <div class="breadcrumbs breadcrumbs-light img-<?php the_slug(); ?>">
        <div class="container">
            <div class="titulos-breadcrumbs">
                <h1><?php the_title() ?></h1>
            </div>
        </div>
    </div>
    <!--=== End Breadcrumbs v3 ===-->


    <!--=== Container Part ===-->
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                
                    <?php the_content(); ?>

            </div><!-- End col-9 -->
            <div class="col-md-3">
                <?php get_sidebar(); ?> 
            </div><!-- End col-3 -->

        </div> <!-- End row--> 
    </div>
    
    </div>
    <!--=== End Container Part ===-->
    <?php endwhile;?>
    <?php endif ?>

			
<?php get_footer(); ?>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
        StyleSwitcher.initStyleSwitcher();
    });
</script>