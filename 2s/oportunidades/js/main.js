$(function() {
    "use strict";




    /* ==========================================================================
   Preload
   ========================================================================== */

    $(window).load(function() {

        $("#status").fadeOut();

        $("#preloader").delay(1000).fadeOut("slow");
    });


    /* ==========================================================================
   On Scroll animation
   ========================================================================== */

    if ($(window).width() > 992) {
        new WOW().init();
    }

    /* ==========================================================================
   Smooth Scroll
   ========================================================================== */

    $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: (target.offset().top - 40)
                }, 1000);
                return false;
            }
        }
    });



    /* ==========================================================================
   App screenshot slider
   ========================================================================== */

    $(".screenshot-slider").owlCarousel({
        autoPlay: 3000, //Set AutoPlay speed
        items: 4,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [979, 3]
    });

    /* ==========================================================================
   Tweet
   ========================================================================== */

    /*
    $('.tweet').twittie({
        username: 'envatomarket', // change username here
        dateFormat: '%b. %d, %Y',
        template: '{{tweet}} {{user_name}}',
        count: 10
    }, function() {
        var item = $('.tweet ul');

        item.children('li').first().show().siblings().hide();
        setInterval(function() {
            item.find('li:visible').fadeOut(500, function() {
                $(this).appendTo(item);
                item.children('li').first().fadeIn(500);
            });
        }, 5000);
    });
    */

    /* ==========================================================================
   countdown
   ========================================================================== */
    /*
    $('.countdown').downCount({
        date: '12/15/2017 12:00:00' // m/d/y
    });
    */

    /* ==========================================================================
     sub form
     ========================================================================== */
    /*
    function loading() {
        $('#mc-notification').show().html('Sending...');
    }

    function formResult(data) {
        $('#mc-notification').html(data);
        $('#mc-form input').val('');
    }

    function onSubmit() {
        $('#mc-form').submit(function() {
            var action = $(this).attr('action');
            loading();
            $.ajax({
                url: action,
                type: 'POST',
                data: {
                    email: $('#mailchimp-email').val(),
                },
                success: function(data) {
                    formResult(data);
                },
                error: function(data) {
                    formResult(data);
                }
            });
            return false;
        });
    }
    onSubmit();
    */

    /* ==========================================================================
       Contact Form
       ========================================================================== */


    $('#contact-form').validate({
        rules: {
            name: {
                required: true,
                minlength: 2
            },
            email: {
                required: true,
                email: true
            },

            message: {
                required: true,
                minlength: 10
            }
        },
        messages: {
            name: "<i class='fa fa-exclamation-triangle'></i>Preencha seu nome.",
            email: {
                required: "<i class='fa fa-exclamation-triangle'></i>Precisamos do seu e-mail.",
                email: "<i class='fa fa-exclamation-triangle'></i>Por favor, entre com e-mail válido."
            },
            message: "<i class='fa fa-exclamation-triangle'></i>Selecione sua mensagem"
        },
        submitHandler: function(form) {

            if($("#g-recaptcha-response").val()){

                $(form).ajaxSubmit({
                    type: "POST",
                    data: $(form).serialize(),
                    url: "/wp-content/themes/2s/oportunidades/php/contact-me.php",
                    success: function() {
                        $('#contact-form :input').attr('disabled', 'disabled');
                        $('#contact-form').fadeTo("slow", 0.15, function() {
                            $(this).find(':input').attr('disabled', 'disabled');
                            $(this).find('label').css('cursor', 'default');
                            $('.success-cf').fadeIn();
                        });
                        $('#contact-form')[0].reset();
                    },
                    error: function() {
                        $('#contact-form').fadeTo("slow", 0.15, function() {
                            $('.error-cf').fadeIn();
                        });
                    }
                });
            }
        }
    });

    /* ==========================================================================
   ScrollTop Button
   ========================================================================== */


    $(window).scroll(function() {
        if ($(this).scrollTop() > 200) {
            $('.scroll-top a').fadeIn(200);
        } else {
            $('.scroll-top a').fadeOut(200);
        }
    });


    $('.scroll-top a').click(function(event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: 0
        }, 1000);
    });

    /* ==========================================================================
   parallax scrolling
   ========================================================================== */



		if (!(/Android|iPhone|iPad|iPod|BlackBerry|Windows Phone/i).test(navigator.userAgent || navigator.vendor || window.opera)) {
        		if($(window).width()>992){skrollr.init({forceHeight:false})}$(window).on("resize",function(){if($(window).width()<=	992){skrollr.init().destroy()}});$(window).on("resize",function(){if($(window).width()>992){skrollr.init({forceHeight:false})}});
    }

    /* ==========================================================================
   sticky nav
   ========================================================================== */



    var menu = $('.navbar');

    var stickyNav = menu.offset().top;

    $(window).scroll(function() {
        if ($(window).scrollTop() > $(window).height()) {
            menu.addClass('stick');
        } else {
            menu.removeClass('stick');

        }
    });


    /* ==========================================================================
   Fade
   ========================================================================== */

    $(window).scroll(function(e) {
        var s = $(window).scrollTop(),
        d = $(document).height(),
        c = $(window).height(),
        opacityVal = (s / 400);
        $('.main .overlay').css('opacity', opacityVal);
    });


	/* ==========================================================================
	   Collapse nav bar
	   ========================================================================== */
	$(".navbar-nav li a").on('click', function() {
	    $(".navbar-collapse").collapse('hide');
	});


  var videos = document.getElementsByTagName("video");

  function checkScroll() {

    for(var i = 0; i < videos.length; i++) {

    var video = videos[i];

    var x = video.offsetLeft, y = video.offsetTop, w = video.offsetWidth, h = video.offsetHeight, r = x + w, //right
        b = y + h, //bottom
        visibleX, visibleY, visible;

        visibleX = Math.max(0, Math.min(w, window.pageXOffset + window.innerWidth - x, r - window.pageXOffset));
        visibleY = Math.max(0, Math.min(h, window.pageYOffset + window.innerHeight - y, b - window.pageYOffset));

        visible = visibleX * visibleY / (w * h);

        if (visible > fraction) {
            video.play();
        } else {
            video.pause();
        }
      }
  }

  window.addEventListener('scroll', checkScroll, false);
  window.addEventListener('resize', checkScroll, false);


});
