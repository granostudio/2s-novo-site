<?php get_header(); ?>

      <section id="slideshow">
        <div id="slideshow-main"  class="row row-fluid ">
          <?php 
          echo do_shortcode('[easingslider id="303"]');
          ?>
        </div>
      </section>
      
      <!-- #main -->
      <section id="main" class="middle wrapper">

        <div class="row row-fluid">
        
        
        
                
                
                <!-- #primary -->
                <div id="primary" class="site-content">
                    
                    <!-- #content -->
                    <div id="content" role="main">
                                            
                                        
                        <!-- BLOG MASONRY -->
                        <div class="blog-masonry">
                            
                            
                          <?php 
                            $args = array( 
                                'posts_per_page' => 100, 
                                'offset'=> 0,
                                'post_type' => 'post',
                                'post_status' => 'publish');
                            $myposts = get_posts( $args );
                            foreach($myposts as $post) : setup_postdata($post);
                          ?>

        

                            <!-- .post -->										
                            <article class="post type-post hentry">
                                
                                <!-- .entry-header -->
                                <header class="entry-header">
                                    <h1 class="entry-title">
                                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a>
                                    </h1>
                                </header>
                                <!-- .entry-header -->
                                 
                                 
                                <!-- .entry-meta --> 
                                <footer class="entry-meta">
                                    Postado em 
                                    <?php
                                      foreach((get_the_category()) as $category) { 
                                          echo '<a href="/blog/'.$category->slug.'">'. $category->cat_name . ' </a>'; 
                                      } 
                                      ?>
                                </footer>
                                <!-- .entry-meta -->  
                                
                                <!-- .featured-image --> 
                                <div class="featured-image">
                                    <?php if (has_post_thumbnail()): ?>
                                    <?php the_post_thumbnail( 'blog-thumb' ); ?>
                                    <?php endif ?>
                                </div>
                                <!-- .featured-image --> 
                                    
                                
                                <!-- .entry-content -->     
                                <div class="entry-content">
                                    <p><?php the_excerpt_max_charlength(140); ?>
                                    <a href="<?php the_permalink(); ?>" class="more-link">Continue lendo <span class="meta-nav">→</span></a></p>
                                </div>
                                <!-- .entry-content -->
                                
                                
                            </article>
                            <!-- .post -->	
                            
                            <?php endforeach;;?>
                          
                        </div>
                        <!-- BLOG MASONRY -->
                        
                        
                        <!-- post nav -->
                        <!--
                        <nav class="navigation" role="navigation">
                            <div class="nav-previous"><a href="#"><span class="meta-nav">←</span> Older posts</a></div>
                        </nav>
                        -->
                        <!-- post nav -->
                                        
                   
                        
                        
                    </div>
                    <!-- #content -->
                    
                </div>
                <!-- #primary -->
                    
                
          
         
         
         </div>
      </section>
      <!-- #main --> 

 

<?php get_footer(); ?>

