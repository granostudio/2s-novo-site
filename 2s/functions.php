<?php
	remove_filter( 'the_content', 'wpautop' );
	remove_filter( 'the_excerpt', 'wpautop' );

	@ini_set( 'upload_max_size' , '64M' );
	@ini_set( 'post_max_size', '64M');
	@ini_set( 'max_execution_time', '300' );

	function my_taxonomies() {
	    register_taxonomy_for_object_type('category', 'page');
	    register_taxonomy_for_object_type('post_tag', 'page');
	}
	add_action('init', 'my_taxonomies');

	/* Settings for Contact Form 7 */
	define('WPCF7_LOAD_CSS', false);

	add_action('login_head', '_custom_logo');
	function _custom_logo() {
	    echo '<style type="text/css">h1 a { background-size: 200px !important; width:200px !important; height:50px!important; background-image:url('. get_template_directory_uri() .'/assets/img/logo-2s.png) !important; }</style>';
	}

	add_filter('show_admin_bar', '__return_false');

	function the_slug($echo=true){
	  $slug = basename(get_permalink());
	  do_action('before_slug', $slug);
	  $slug = apply_filters('slug_filter', $slug);
	  if( $echo ) echo $slug;
	  do_action('after_slug', $slug);
	  return $slug;
	}

	add_action( 'after_setup_theme', '_setup' );
	if ( ! function_exists( '_setup' ) ):
	function _setup() {
		include( 'post-types/clientes.php' );
		include( 'post-types/oportunidades.php' );
		include( 'post-types/conteudos.php' );
		include( 'post-types/destaque.php' );
		include( 'post-types/noticias.php' );

		add_theme_support( 'post-thumbnails' );

		add_image_size( 'cases-featured', 570, 450, true );
		add_image_size( 'clientes-featured', 570, 450, true );
		add_image_size( 'destaque-featured', 360, 228 );
		add_image_size( 'blog-thumb', 348, 261, true );
		add_image_size( 'blog-thumb-related', 300, 225, true );
		add_image_size( 'blog-featured', 400, 300 );

	}
	endif;

	add_action( 'init', 'cmb_initialize_cmb_meta_boxes', 9999 );
	function cmb_initialize_cmb_meta_boxes() {

	if ( ! class_exists( 'cmb_Meta_Box' ) )
		require_once 'lib/metabox/init.php';
	}


	function my_password_form() {
	    global $post;
	    $o = '';
	    return $o;
	}
	add_filter( 'the_password_form', 'my_password_form' );

	function the_title_trim($title) {
		$title = attribute_escape($title);
		$findthese = array(
			'#Protegido:#',
			'#Privado:#'
		);
		$replacewith = array(
			'', // What to replace "Protected:" with
			'' // What to replace "Private:" with
		);
		$title = preg_replace($findthese, $replacewith, $title);
		return $title;
	}
	add_filter('the_title', 'the_title_trim');

	add_action( 'widgets_init', 'theme_slug_widgets_init' );
	function theme_slug_widgets_init() {
	    register_sidebar(
	    array(
	        'name' => __( 'Main Sidebar', 'theme-slug' ),
	        'id' => 'sidebar-1',
	        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
	        'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	    ));

		register_sidebar(
	    array(
	        'name' => __( 'Sidebar Serviços', 'theme-slug' ),
	        'id' => 'sidebar-2',
	        'description' => __( 'Widgets podem ser adicionados a este sidebar para página de serviços.', 'theme-slug' ),
	        'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	    ));

	    register_sidebar(
	    array(
	        'name' => __( 'Footer - Sobre 2S', 'theme-slug' ),
	        'id' => 'sidebar-3',
	        'description' => __( 'Widgets podem ser adicionados ao footer sobre a 2S.', 'theme-slug' ),
	        'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	    ));

	    register_sidebar(
	    array(
	        'name' => __( 'Footer - Great Place To Work', 'theme-slug' ),
	        'id' => 'sidebar-4',
	        'description' => __( 'Widgets podem ser adicionados ao footer sobre Great Place To Work.', 'theme-slug' ),
	        'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	    ));

	    register_sidebar(
	    array(
	        'name' => __( 'Footer - Redes Sociais', 'theme-slug' ),
	        'id' => 'sidebar-5',
	        'description' => __( 'Widgets podem ser adicionados ao footer sobre Redes Sociais.', 'theme-slug' ),
	        'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	    ));

		register_sidebar(
	    array(
	        'name' => __( 'Footer - Contato', 'theme-slug' ),
	        'id' => 'sidebar-6',
	        'description' => __( 'Widgets podem ser adicionados ao footer sobre informações de Contato.', 'theme-slug' ),
	        'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	    ));

	    register_sidebar(
	    array(
	        'name' => __( 'Sidebar Soluções', 'theme-slug' ),
	        'id' => 'sidebar-7',
	        'description' => __( 'Widgets podem ser adicionados a sidebar de soluções.', 'theme-slug' ),
	        'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	    ));

 		register_sidebar(
	    array(
	        'name' => __( 'Menu', 'theme-slug' ),
	        'id' => 'sidebar-8',
	        'description' => __( 'HTML que representa o menu superior do site', 'theme-slug' ),
	        'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	    ));

	    register_sidebar(
	    array(
	        'name' => __( 'HTML Slider Home', 'theme-slug' ),
	        'id' => 'sidebar-9',
	        'description' => __( 'HTML que representa o slider da página inicial', 'theme-slug' ),
	        'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	    ));
	}

	add_shortcode('shortcode_sidebar', 'shortcode_sidebar_action');
	function shortcode_sidebar_action($atts){
		ob_start();
		dynamic_sidebar('sidebar-7');
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}


	// THIS THEME USES wp_nav_menu() IN TWO LOCATIONS FOR CUSTOM MENU.
	if ( function_exists( 'register_nav_menus' ) ) {
		register_nav_menus(
			array(
			  'primary' => 'Header Menu',
			  'foot_menu' => 'Footer Menu'
			)
		);
	}

	function the_excerpt_max_charlength($charlength) {
		$excerpt = get_the_excerpt();
		$charlength++;

		if ( mb_strlen( $excerpt ) > $charlength ) {
			$subex = mb_substr( $excerpt, 0, $charlength - 5 );
			$exwords = explode( ' ', $subex );
			$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
			if ( $excut < 0 ) {
				echo mb_substr( $subex, 0, $excut );
			} else {
				echo $subex;
			}
			echo '[...]';
		} else {
			echo $excerpt;
		}
	}

	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );

	wp_enqueue_script( 'isInViewport', get_bloginfo( 'stylesheet_directory' ) . '/js/isInViewport.js', array( 'jquery' ), CHILD_THEME_VERSION );
?>
