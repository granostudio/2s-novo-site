<?php
add_action( 'init', 'codex_clippings_custom_init' );
function codex_clippings_custom_init() {
  $labels = array(
    'name' => _x('clipping', 'clippings'),
    'singular_name' => _x('clippings', 'clippings'),
    'add_new' => _x('Adicionar novo', 'clipping'),
    'add_new_item' => __('Adicionar novo clipping'),
    'edit_item' => __('Editar clipping'),
    'new_item' => __('Novo clipping'),
    'all_items' => __('Todos clippings'),
    'view_item' => __('Visualizar clipping'),
    'search_items' => __('Procurar clipping'),
    'not_found' =>  __('Nenhum clipping encontrado'),
    'not_found_in_trash' => __('Nenhum clipping encontrado na lixeira'), 
    'parent_item_colon' => '',
    'menu_name' => __('Clippings 2S')
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => array('slug' => 'clipping'),
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'supports' => array( 'title', 'revisions' )
  );
  register_post_type('clippings',$args);
}

add_filter( 'cmb_meta_boxes', 'cmb_clippings_metaboxes' );
function cmb_clippings_metaboxes( array $meta_boxes ) {

	$prefix = 'clipping_';
	$meta_boxes[] = array(

		'id'         => 'clipping_metabox',
		'title'      => 'Informa&ccedil;&otilde;es do Clipping',
		'pages'      => array( 'clippings', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, 
		'fields'     => array(

			array(
				'name' => 'URL do clipping',
				'desc' => 'URL do clipping na mídia externa',
				'id'   => $prefix . 'titulo',
				'type' => 'text',
			),
			
			array(
				'name'    => 'Descrição do clipping',
				'desc'    => 'Este texto é a descrição do clipping',
				'id'      => $prefix . 'texto',
				'type'    => 'textarea',
			),		

			array(
				'name' => 'Imagem de destaque',
				'desc' => 'Realize o upload ou entre com a URL do destaque. Proporção/Tamanho imagem: 570px de largura / 450px de altura',
				'id'   => $prefix . 'imagem',
				'type' => 'file',
			),


		),
	);
	
	// Add other metaboxes as needed
	return $meta_boxes;
}
add_action( 'init', 'cmb_initialize_cmb_meta_boxes', 9999 );
?>
