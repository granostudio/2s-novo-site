<?php
add_action( 'init', 'codex_destaque_custom_init' );
function codex_destaque_custom_init() {
  $labels = array(
    'name' => _x('Destaque', 'destaque'),
    'singular_name' => _x('Destaque', 'destaque'),
    'add_new' => _x('Adicionar novo', 'destaque'),
    'add_new_item' => __('Adicionar novo destaque'),
    'edit_item' => __('Editar destaque'),
    'new_item' => __('Novo destaque'),
    'all_items' => __('Todos destaques'),
    'view_item' => __('Visualizar destaque'),
    'search_items' => __('Procurar destaque'),
    'not_found' =>  __('Nenhum destaque encontrado'),
    'not_found_in_trash' => __('Nenhum destaque encontrado na lixeira'), 
    'parent_item_colon' => '',
    'menu_name' => __('Destaques 2S')
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => array('slug' => 'destaques'),
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'supports' => array( 'title' )
  );
  register_post_type('destaque', $args);
}

add_filter( 'cmb_meta_boxes', 'cmb_destaque_metaboxes' );
function cmb_destaque_metaboxes( array $meta_boxes ) {

	$prefix = 'destaque_';
	$meta_boxes[] = array(

		'id'         => 'destaque_metabox',
		'title'      => 'Informa&ccedil;&otilde;es do Destaque',
		'pages'      => array( 'destaque', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, 
		'fields'     => array(

			array(
				'name' => 'Foto do destaque',
				'desc' => 'Realize o upload ou entre com a URL do destaque. Tamanho permitido: 360px de largura / 228px de altura',
				'id'   => $prefix . 'foto',
				'type' => 'file',
			),

			array(
				'name' => 'Link',
				'desc' => 'Digite o link que deseja adicionar ao destaque.',
				'id'   => $prefix . 'link',
				'type' => 'text',
			),

			array(
				'name'    => 'Resumo',
				'desc'    => 'Texto descritivo sobre o destaque.',
				'id'      => $prefix . 'descricao',
				'type'    => 'wysiwyg',
				'options' => array(	'textarea_rows' => 3, ),
			),
			
		),
	);
	
	// Add other metaboxes as needed
	return $meta_boxes;
}
add_action( 'init', 'cmb_initialize_cmb_meta_boxes', 9999 );

?>
