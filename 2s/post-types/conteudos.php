<?php
add_action( 'init', 'codex_conteudos_custom_init' );
function codex_conteudos_custom_init() {
  $labels = array(
    'name' => _x('Conteúdo', 'conteudos'),
    'singular_name' => _x('Conteúdos', 'conteudos'),
    'add_new' => _x('Adicionar novo', 'conteudo'),
    'add_new_item' => __('Adicionar novo conteúdo'),
    'edit_item' => __('Editar conteúdo'),
    'new_item' => __('Novo conteúdo'),
    'all_items' => __('Todos conteudos'),
    'view_item' => __('Visualizar conteúdo'),
    'search_items' => __('Procurar conteúdo'),
    'not_found' =>  __('Nenhum conteúdo encontrado'),
    'not_found_in_trash' => __('Nenhum conteúdo encontrado na lixeira'), 
    'parent_item_colon' => '',
    'menu_name' => __('Conteúdos 2S')
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => array('slug' => 'conteudo'),
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'supports' => array( 'title', 'revisions', 'excerpt', 'thumbnail', 'author' ),
    'taxonomies' => array( 'post_tag' )
  );
  register_post_type('conteudos',$args);
}

add_filter( 'cmb_meta_boxes', 'cmb_conteudos_metaboxes' );
function cmb_conteudos_metaboxes( array $meta_boxes ) {

	$prefix = 'conteudo_';
	$meta_boxes[] = array(

		'id'         => 'conteudo_metabox',
		'title'      => 'Informa&ccedil;&otilde;es do Conteúdo (e-book ou vídeo)',
		'pages'      => array( 'conteudos', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, 
		'fields'     => array(

			array(
				'name' => 'Título do conteúdo',
				'desc' => 'Este título do nome do conteúdo',
				'id'   => $prefix . 'titulo',
				'type' => 'text',
			),
			
			array(
				'name'    => 'Descrição do conteúdo',
				'desc'    => 'Este texto é a descrição do conteúdo a ser oferecido',
				'id'      => $prefix . 'texto',
				'type'    => 'textarea',
			),		

			array(
				'name' => 'Imagem de destaque',
				'desc' => 'Realize o upload ou entre com a URL do destaque. Proporção/Tamanho imagem: 570px de largura / 450px de altura',
				'id'   => $prefix . 'imagem',
				'type' => 'file',
			),

			array(
				'name' => 'E-book',
				'desc' => 'Realize o upload ou entre com a URL do e-book. ',
				'id'   => $prefix . 'ebook',
				'type' => 'file',
			),

			array(
				'name'    => 'Vídeo',
				'desc'    => 'Coloque aqui o embed do vídeo (Youtube, Vimeo, etc)',
				'id'      => $prefix . 'video',
				'type'    => 'textarea_small',
			),		

		),
	);
	
	// Add other metaboxes as needed
	return $meta_boxes;
}
add_action( 'init', 'cmb_initialize_cmb_meta_boxes', 9999 );
?>
