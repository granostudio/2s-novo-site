<?php
add_action( 'init', 'codex_cases_custom_init' );
function codex_cases_custom_init() {
  $labels = array(
    'name' => _x('Cases', 'cases'),
    'singular_name' => _x('Cases', 'cases'),
    'add_new' => _x('Adicionar novo', 'case'),
    'add_new_item' => __('Adicionar novo case'),
    'edit_item' => __('Editar case'),
    'new_item' => __('Novo case'),
    'all_items' => __('Todos cases'),
    'view_item' => __('Visualizar case'),
    'search_items' => __('Procurar case'),
    'not_found' =>  __('Nenhum case encontrado'),
    'not_found_in_trash' => __('Nenhum case encontrado na lixeira'), 
    'parent_item_colon' => '',
    'menu_name' => __('Cases')
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => array('slug' => 'case'),
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'supports' => array( 'title', 'editor', 'revisions' )
  );
  register_post_type('cases',$args);
}

add_filter( 'cmb_meta_boxes', 'cmb_cases_metaboxes' );
function cmb_cases_metaboxes( array $meta_boxes ) {

	$prefix = 'cases_';
	$meta_boxes[] = array(

		'id'         => 'case_metabox',
		'title'      => 'Informa&ccedil;&otilde;es do Case',
		'pages'      => array( 'cases', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, 
		'fields'     => array(

			array(
				'name' => 'Título do case',
				'desc' => 'Este título do nome do case',
				'id'   => $prefix . 'titulo_case',
				'type' => 'text',
			),
			

			array(
				'name'    => 'Texto destaque página listagem',
				'desc'    => 'Este texto aparece quando usuário passa o mouse sobre o case na lista de cases',
				'id'      => $prefix . 'texto_destaque',
				'type'    => 'textarea_small',
			),

			array(
				'name' => 'Foto de destaque',
				'desc' => 'Realize o upload ou entre com a URL do destaque. Proporção/Tamanho imagem: 570px de largura / 450px de altura',
				'id'   => $prefix . 'foto',
				'type' => 'file',
			),


			array(
				'name'    => 'Legenda foto destaque',
				'desc'    => 'Este texto aparece abaixo da imagem',
				'id'      => $prefix . 'texto_legenda',
				'type'    => 'textarea_small',
			),		

		),
	);
	
	// Add other metaboxes as needed
	return $meta_boxes;
}
add_action( 'init', 'cmb_initialize_cmb_meta_boxes', 9999 );
?>
