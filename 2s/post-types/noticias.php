<?php
add_action( 'init', 'codex_noticias_custom_init' );
function codex_noticias_custom_init() {
  $labels = array(
    'name' => _x('noticia', 'noticias'),
    'singular_name' => _x('noticias', 'noticias'),
    'add_new' => _x('Adicionar novo', 'noticia'),
    'add_new_item' => __('Adicionar novo noticia'),
    'edit_item' => __('Editar noticia'),
    'new_item' => __('Novo noticia'),
    'all_items' => __('Todos noticias'),
    'view_item' => __('Visualizar noticia'),
    'search_items' => __('Procurar noticia'),
    'not_found' =>  __('Nenhum noticia encontrado'),
    'not_found_in_trash' => __('Nenhum noticia encontrado na lixeira'), 
    'parent_item_colon' => '',
    'menu_name' => __('Noticias 2S')
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => array('slug' => 'noticia'),
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'supports' => array( 'title', 'revisions', 'excerpt', 'thumbnail', 'author' ),
    'taxonomies' => array( 'post_tag' )
  );
  register_post_type('noticias',$args);
}

add_filter( 'cmb_meta_boxes', 'cmb_noticias_metaboxes' );
function cmb_noticias_metaboxes( array $meta_boxes ) {

	$prefix = 'noticia_';
	$meta_boxes[] = array(

		'id'         => 'noticia_metabox',
		'title'      => 'Informa&ccedil;&otilde;es da Notícia',
		'pages'      => array( 'noticias', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, 
		'fields'     => array(

			array(
				'name' => 'URL da notícia',
				'desc' => 'URL dda notícia na mídia externa',
				'id'   => $prefix . 'titulo',
				'type' => 'text',
			),
			
			array(
				'name'    => 'Descrição da notícia',
				'desc'    => 'Este texto é a descrição da notícia',
				'id'      => $prefix . 'texto',
				'type'    => 'textarea',
			),		

			array(
				'name' => 'Imagem de destaque',
				'desc' => 'Realize o upload ou entre com a URL do destaque. Proporção/Tamanho imagem: 570px de largura / 450px de altura',
				'id'   => $prefix . 'imagem',
				'type' => 'file',
			),


		),
	);
	
	// Add other metaboxes as needed
	return $meta_boxes;
}
add_action( 'init', 'cmb_initialize_cmb_meta_boxes', 9999 );
?>
