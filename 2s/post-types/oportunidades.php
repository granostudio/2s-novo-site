<?php
add_action( 'init', 'codex_oportunidades_custom_init' );
function codex_oportunidades_custom_init() {
  $labels = array(
    'name' => _x('oportunidades', 'oportunidades'),
    'singular_name' => _x('oportunidades', 'oportunidades'),
    'add_new' => _x('Adicionar nova', 'oportunidade'),
    'add_new_item' => __('Adicionar nova oportunidade'),
    'edit_item' => __('Editar oportunidade'),
    'new_item' => __('Nova oportunidade'),
    'all_items' => __('Todos oportunidades'),
    'view_item' => __('Visualizar oportunidade'),
    'search_items' => __('Procurar oportunidade'),
    'not_found' =>  __('Nenhuma oportunidade encontrada'),
    'not_found_in_trash' => __('Nenhuma oportunidade encontrada na lixeira'), 
    'parent_item_colon' => '',
    'menu_name' => __('Vagas 2S')
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => array('slug' => 'oportunidade'),
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'supports' => array( 'title', 'editor', 'revisions' )
  );
  register_post_type('oportunidades',$args);
}

add_filter( 'cmb_meta_boxes', 'cmb_oportunidades_metaboxes' );
function cmb_oportunidades_metaboxes( array $meta_boxes ) {

	$prefix = 'oportunidade_';
	$meta_boxes[] = array(

		'id'         => 'oportunidade_metabox',
		'title'      => 'Informa&ccedil;&otilde;es da oportunidade',
		'pages'      => array( 'oportunidades', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, 
		'fields'     => array(

			array(
				'name' => 'Título utilizado para oportunidade',
				'desc' => 'Este é o nome do cargo/oportunidade',
				'id'   => $prefix . 'titulo',
				'type' => 'text',
			),

			array(
				'name' => 'Resumo da oportunidade',
				'desc' => 'Este é o resumo do cargo/oportunidade',
				'id'   => $prefix . 'resumo',
				'type' => 'textarea_small',
			),

			array(
				'name' => 'Nome do projeto',
				'desc' => 'Este é o nome do projeto',
				'id'   => $prefix . 'projeto',
				'type' => 'text',
			),


		),
	);
	
	// Add other metaboxes as needed
	return $meta_boxes;
}
add_action( 'init', 'cmb_initialize_cmb_meta_boxes', 9999 );
?>
