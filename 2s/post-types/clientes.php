<?php
add_action( 'init', 'codex_clientes_custom_init' );
function codex_clientes_custom_init() {
  $labels = array(
    'name' => _x('clientes', 'clientes'),
    'singular_name' => _x('clientes', 'clientes'),
    'add_new' => _x('Adicionar novo', 'cliente'),
    'add_new_item' => __('Adicionar novo cliente'),
    'edit_item' => __('Editar cliente'),
    'new_item' => __('Novo cliente'),
    'all_items' => __('Todos clientes'),
    'view_item' => __('Visualizar cliente'),
    'search_items' => __('Procurar cliente'),
    'not_found' =>  __('Nenhum cliente encontrado'),
    'not_found_in_trash' => __('Nenhum cliente encontrado na lixeira'), 
    'parent_item_colon' => '',
    'menu_name' => __('Clientes 2S')
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => array('slug' => 'cliente'),
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'supports' => array( 'title', 'editor', 'revisions', 'excerpt', 'thumbnail', 'author' ),
    'taxonomies' => array( 'post_tag' )
  );
  register_post_type('clientes',$args);
}

add_filter( 'cmb_meta_boxes', 'cmb_clientes_metaboxes' );
function cmb_clientes_metaboxes( array $meta_boxes ) {

	$prefix = 'clientes_';
	$meta_boxes[] = array(

		'id'         => 'cliente_metabox',
		'title'      => 'Informa&ccedil;&otilde;es do cliente',
		'pages'      => array( 'clientes', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, 
		'fields'     => array(

			array(
				'name' => 'Título utilizado sobre o cliente',
				'desc' => 'Este título do nome do cliente',
				'id'   => $prefix . 'titulo_cliente',
				'type' => 'text',
			),
			

			array(
				'name'    => 'Texto destaque página listagem',
				'desc'    => 'Este texto aparece quando usuário passa o mouse sobre o cliente na lista de clientes',
				'id'      => $prefix . 'texto_destaque',
				'type'    => 'textarea_small',
			),

			array(
				'name' => 'Foto de destaque',
				'desc' => 'Realize o upload ou entre com a URL do destaque. Proporção/Tamanho imagem: 570px de largura / 450px de altura',
				'id'   => $prefix . 'foto',
				'type' => 'file',
			),


			array(
				'name'    => 'Legenda foto destaque',
				'desc'    => 'Este texto aparece abaixo da imagem',
				'id'      => $prefix . 'texto_legenda',
				'type'    => 'textarea_small',
			),		

		),
	);
	
	// Add other metaboxes as needed
	return $meta_boxes;
}
add_action( 'init', 'cmb_initialize_cmb_meta_boxes', 9999 );
?>
