<?php
    if($_POST['contato']){
        $erro = 0;
        echo '<div class="row"><div class="col-sm-6 col-sm-offset-0">';
        if($_POST['nome'] == ''){
            echo '<span class="label label-danger msg-erro-form">Informe um nome</span>';
            $erro = 1;
        }
        if($_POST['email'] == ''){
            echo '<span class="label label-danger msg-erro-form">Informe um email</span>';
            $erro = 1;
        }else{
            $validacao = preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/" ,$_POST['email']); 
            if($validacao == 0){
                echo '<span class="label label-danger msg-erro-form">Informe um email v&aacute;lido</span>';
                $erro = 1;
            }
        }
        if($_POST['telefone'] == ''){
            echo '<span class="label label-danger msg-erro-form">Informe um Telefone</span>';
            $erro = 1;
        }

        if($_POST['assunto'] == ''){
            echo '<span class="label label-danger msg-erro-form">Informe o assunto</span>';
            $erro = 1;
        }
        if($_POST['mensagem'] == ''){
            echo '<span class="label label-danger msg-erro-form">Informe uma mensagem</span>';
            $erro = 1;
        }

        if($erro == 0){
            $destinatario = "atendimento@2s.com.br";

            $nome = $_POST['nome'];
            $email = $_POST['email'];
            $telefone = $_POST['telefone'];
            $assunto = $_POST['assunto'];
            $mensagem = $_POST['mensagem'];
            $ip = $_SERVER['REMOTE_ADDR'];
            $navegador = $_SERVER['HTTP_USER_AGENT'];

            $headers = "MIME-Version: 1.1\n";
            $headers .= "Content-Type: text/html;\r\n charset=\"iso-8859-1\"\r\n";
            $headers .= "From: $nome <$email>\n"; // remetente
            $headers .= "Return-Path: $nome <$email>\n"; // return-path

            $subject = 'Contato Site 2S Tecnologia';

            $mensagem = '
            <html>
            <title>Contato Site 2S Tecnologia</title>
            <body>
                
            

            <table width="500px">
                <thead>
                    <tr>
                        <th colspan="2">
                            <img src="http://leoaraujo.com/yodesign/2s/assets/img/logo-2s.png" alt="2s-logo">
                        </th>
                    </tr>
                    <tr>
                        <th colspan="2" style="font-size: 25px; padding: 10px 0;">Contato Site 2S Tecnologia</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="100px" style="font-size:16px; font-weight: bold">Nome:</td>
                        <td style="padding:2px 10px">'.$nome.'</td>
                    </tr>
                    <tr>
                        <td width="100px" style="font-size:16px; font-weight: bold">E-mail:</td>
                        <td style="padding:2px 10px">'.$email.'</td>
                    </tr>
                    <tr>
                        <td width="100px" style="font-size:16px; font-weight: bold">Telefone:</td>
                        <td style="padding:2px 10px">'.$telefone.'</td>
                    </tr>
                    <tr>
                        <td width="100px" style="font-size:16px; font-weight: bold">Assunto:</td>
                        <td style="padding:2px 10px">'.$assunto.'</td>
                    </tr>
                    <tr>
                        <td colspan="2" width="100px" style="font-size:16px; font-weight: bold">Mensagem:</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align:justify">'.$mensagem.'</td>
                    </tr>
                </tbody>
            </table>


            
            </body>                        
            </html>
            ';
            if(mail($destinatario,$subject,$mensagem,$headers)){
            ?>
            <script language="JavaScript">
                $('#formulario').remove();
            </script>
            <?
                echo '<div class="alert alert-success fade in text-center"><h4>Mensagem enviada com sucesso!</h4></div>';
            } else {
                echo '<div class="alert alert-danger fade in text-center"><h4>Erro ao enviar a mensagem</h4></div>';
            }
            echo '</div></div>';    
        }
    }
?>