<?php
/**
* Category Page
*/

get_header(); ?>


    <div class="banner-home">
      <p>A transformação digital é agora - e para TODOS! Saiba como a tecnologia aplicada ao seu negócio pode aumentar a competitividade e acelerar o crescimento de sua empresa.
      </p>
      <a href="http://www.2s.com.br/contato/"><button type="button" name="button">Fale com a 2S</button></a>
    </div>

  <div id="inicio-posts" class="home-posts">

    <div class="container">

      <div class="menu-categorias">
        <ul>
          <a href="/blog/tecnologia#inicio-posts"><li style="background-color: #1F72B6;">Tecnologias</li></a>
          <a href="/blog/negocio"><li style="background-color: #01B59C;">Negócios</li></a>
          <a href="/blog/inovacao"><li style="background-color: #FFCB27;">Inovação</li></a>
          <a href="/blog/servicos"><li style="background-color: #FF7C2E;">Serviços</li></a>
          <a href="/blog/acontece-na-2s"><li style="background-color: #FF5A46;margin-right: 0px;">Acontece na 2s</li></a>
        </ul>
      </div>
      <div class="menu-subcategorias">
        <?php
        echo "<ul>"
        $get_parent_cats = array(
            'parent' => '0' //get top level categories only
        );

        $all_categories = get_categories( $get_parent_cats );//get parent categories

        foreach( $all_categories as $single_category ){
            //for each category, get the ID
            $catID = $single_category->cat_ID;

            echo '<li><a href=" ' . get_category_link( $catID ) . ' ">' . $single_category->name . '</a>'; //category name & link
            $get_children_cats = array(
                'child_of' => $catID //get children of this parent using the catID variable from earlier
            );

            $child_cats = get_categories( $get_children_cats );//get children of parent category
            echo '<ul class="children">';
                foreach( $child_cats as $child_cat ){
                    //for each child category, get the ID
                    $childID = $child_cat->cat_ID;

                    //for each child category, give us the link and name
                    echo '<a href=" ' . get_category_link( $childID ) . ' ">' . $child_cat->name . '</a>';

                }
            echo '</ul></li>';
        } //end of categories logic ?>
        <ul>
          <li><strong>Subcategorias:</strong></li>
          <li><a href="/blog/colaboracao">Colaboração</a></li>
          <li><a href="/blog/iot">IOT</a></li>
          <li><a href="/blog/mobilidade">Mobilidade</a></li>
          <li><a href="/blog/seguranca">Segurança</a></li>
          <li><a href="/blog/datacenter">Datacenter</a></li>
          <li><a href="/blog/institucional">Institucional</a></li>
          <li><a href="/blog/servicos">Serviços</a></li>
        </ul>
      </div>


      <?php
// Check if there are any posts to display
if ( have_posts() ) : ?>
      <div class="headline">
        <h2 style="font-size:25px"><?php single_cat_title(); ?></h2>
      </div>
      <div class="row news-v2 margin-bottom-60">
      <?php while ( have_posts() ) : the_post(); ?>
        <!-- .post -->

          <div class="col-md-4" style="margin-bottom:20px;">
            <a href="<?php the_permalink(); ?>" style="text-decoration:none">
              <div class="news-v2-badge">
                    <div class="img-responsive">
                      <?php if (has_post_thumbnail()): ?>
                        <?php the_post_thumbnail( 'blog-thumb' ); ?>
                      <?php endif ?>
                    </div>
                  <!-- </a> -->
                  <p>
                      <img class="img-responsive" src="/wp-content/themes/2s/assets/img/simbol-2s-box.png" alt="">
                  </p>
              </div>
              <!-- <a href="<?php the_permalink(); ?>"> -->
                <div class="news-v2-desc2 post-border" id="post-border">
                  <h2 class="entry-title" style="font-size: 19px;text-align: left;">
                      <?php the_title(); ?>
                  </h2>
                  <p>
                    <?php the_excerpt_max_charlength(70); ?></strong>
                  </p>
                </div>
                </a>
          </div>

        <!-- .post -->

      <?php endwhile;

    else:?>

    <p>Desculpe, nenhum conteúdo :(.</p>

    <?php endif; ?>

    </div>

    </div>


    <?php get_footer(); ?>
  </div>


<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
        LayerSlider.initLayerSlider();
        StyleSwitcher.initStyleSwitcher();
        OwlCarousel.initOwlCarousel();
        OwlRecentWorks.initOwlRecentWorksV2();
    });
</script>
