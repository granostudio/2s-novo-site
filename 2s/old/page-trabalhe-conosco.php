<?php if (have_posts()): ?>
<?php while ( have_posts() ) : the_post(); ?>
<style>
#capatcha {
    margin: 0 auto;
    display: block;
    width: 50%; 
    margin-top:10px;
    margin-bottom:15px;
}
</style>
<?php get_header(); ?>
     
    <?php the_content(); ?>

    <section class="video section-spacing text-center" id="video3">
      <div class="container">
        <header class="text-center">
          <h2>Quer trabalhar com a gente?</h2>
          <h3>Conheça as oportunidades no momento.</h3>
        </header>
        <div class="row">
          <div class="center-block col-md-12" style="padding: 0px;margin: 0px;margin-left: -10px;margin-right: -10px;">
          <?php
              $query= null;
              $query = new WP_Query(
                      array(
                          'post_type' => 'oportunidades',
                          'post_per_page' => 100)
                      );

          if ( $query->have_posts() ) { 
            while ( $query->have_posts() ) {
            $query->the_post();
            $custom                 = get_post_custom($post->ID);
            $oportunidade_titulo    =   $custom["oportunidade_titulo"][0];    
            $oportunidade_resumo    =   $custom["oportunidade_resumo"][0];
            $oportunidade_projeto    =   $custom["oportunidade_projeto"][0];
        ?>
            <div class="col-md-4">
              <div class="col-md-12" style="border: 3px solid #00aeef;padding:15px; ">
                <h4 style="color:#686868;font-weight: 600;"><?php echo $oportunidade_titulo;?></h4>
                <p><?php echo $oportunidade_projeto; ?></p>
                <!--<p><a href="<?php the_permalink(); ?>">Saiba mais sobre esta vaga</a></p>-->
              </div>
            </div>

        <?php } } ?>
          </div>
        </div>
      </div>
    </section>

    <section id="contact-us" class="twitter-feed section-spacing text-center">
      <div class="overlay-t"></div>
      <div class="container">
        <div class="row">
          <div class="col-md-7 center-block col-sm-11 col-xs-11">
            <h2>Entre em contato</h2>
          </div>
        </div>
      </div>
    </section>

    <section class="contact section-spacing">
      <div class="container">
        <header class="text-center">
          <h2>Não encontrou a vaga certa?</h2>
          <h3>Conte-nos o que você busca.</h3>
        </header>
        <div class="row">
          <div class="col-md-6 center-block"> 
            
            <div class="contact-form">
              <form role="form" action="/wp-content/themes/2s/oportunidades/php/contact-me.php" method="post" id="contact-form">
                <input type="text" class="form-control" name="name" id="name" placeholder="Seu nome" required>
                <input type="email" class="form-control" name="email" id="email" placeholder="E-mail" required>

                <select class="form-control" name="job" id="job" placeholder="Oportunidade" style="height:45px;">
                <option value="Nenhuma oportunidade selecionada"> Selecione uma oportunidade </option>
                <?php
                  while ( $query->have_posts() ) {
                    $query->the_post();
                      $custom                 = get_post_custom($post->ID);
                      $oportunidade_titulo    =   $custom["oportunidade_titulo"][0];    
                      $oportunidade_resumo    =   $custom["oportunidade_resumo"][0];
                      $oportunidade_projeto    =   $custom["oportunidade_projeto"][0];
                ?> 
                  <option value="<?php echo $oportunidade_titulo;?>"> <?php echo $oportunidade_titulo;?> </option>
                <?php } ?>
                </select>

                <textarea class="form-control" name="message" id="message" rows="4" placeholder="Envia uma mensagem" required></textarea>
                <div id="capatcha">
                <div class="g-recaptcha" data-sitekey="6LcrUQcUAAAAAJl_CzudrmPQYNTh79IqHxwYwNCx"></div>
                </div>
                <button type="submit" class="btn btn-default submit-btn" id="submit-btn" value="Send">Enviar</button>
              </form>
              <div class="success-cf">
                <p><i class="fa fa-envelope"></i> Obrigado. Sua mensagem foi enviada.</p>
              </div>
              <div class="error-cf">
                <p><i class="fa fa-exclamation-triangle"></i> .</p>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </section>
			
<?php get_footer(); ?>

<?php endwhile;?>
<?php endif ?>