<?php get_header(); ?>


<!-- Page Content -->
<div class="container">

  <?php  include get_template_directory(). '/menu-categorias.php'; ?>

            <div class="headline">
                <h2 style="font-size:25px">Resultados da busca para <?php the_search_query(); ?></h2>
            </div>


                <!-- First Blog Post -->
                <?php
                if( have_posts() ) {
                  while ( have_posts() ) {
                    the_post(); ?>

                    <div class="col-md-4" style="margin-bottom:20px;">
                      <a href="<?php the_permalink(); ?>" style="text-decoration:none">
                        <div class="news-v2-badge">
                              <div class="img-responsive">
                                <?php if (has_post_thumbnail()): ?>
                                  <?php the_post_thumbnail( 'blog-thumb' ); ?>
                                <?php else: ?>
                                  <div class="div-sfoto"></div>
                                <?php endif ?>
                              </div>
                            <!-- </a> -->
                            <p style="left: 15px;top: 50px;background: #fff;padding: 5px;text-align: center;position: absolute;">
                                <img class="img-responsive" src="/wp-content/themes/2s/assets/img/simbol-2s-box.png" alt="">
                            </p>
                        </div>
                        <!-- <a href="<?php the_permalink(); ?>"> -->
                          <div class="news-v2-desc2 post-border" id="post-border" style="padding: 10px;">
                            <h2 class="entry-title" style="font-size: 19px;text-align: left;">
                                <?php the_title(); ?>
                            </h2>
                            <p>
                              <?php the_excerpt_max_charlength(70); ?></strong>
                            </p>
                          </div>
                          </a>
                    </div>

                    <?php
                  }
                } else { ?>
                  <br>
                  <h3>Nada encontrado, tente outro assunto</h3>
                  <!-- <div class="box-buscar">
                      <form role="search" method="get" id="searchform">
                        <div class="input-group">
                          <input type="text" class="campo-buscar" name="s" id="s" placeholder="Digite o que está procurando ou escolha uma das categorias abaixo para filtrar os assuntos..." />
                          <!-- <div class="input-group-btn">
                            <button class="btn btn-default" type="submit" id="searchsubmit">
                            <i class="glyphicon glyphicon-search"></i>
                            </button>
                           </div> 
                         </div>
                      </form>
                    </div> -->
           <?php  }  ?>

                <!-- Pager -->
            </div>
            <div class="">
              <ul class="pager">
                <li class="previous"><?php next_posts_link( 'Posts Anteriores' ); ?></li>
                <li class="next"><?php previous_posts_link( 'Novos Posts' ); ?></li>
              </ul>
            </div>



            <!-- Blog Sidebar Widgets Column -->
            <!-- <?php get_sidebar(); ?> -->

        </div>
        <!-- /.row -->

        <hr>


    </div>
    <!-- /.container -->

<?php get_footer(); ?>
