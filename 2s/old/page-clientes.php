<?php
    $query= null;
    $query = new WP_Query(
            array(
                'post_type' => 'clientes',
                'post_per_page' => 100)
            );
?>

<?php get_header(); ?>
    <!--=== Breadcrumbs v3 ===-->
    <div class="breadcrumbs breadcrumbs-light img-cases">
        <div class="container">
            <div class="titulos-breadcrumbs">
                <h1><?php the_title(); ?></h1>
            </div>
        </div>
    </div>
    <!--=== End Breadcrumbs v3 ===-->

     <!--=== Content Part ===-->
    <div class="container">
		
        <!-- Cases Blocks -->
        <div class="headline">
            <h2>CONHEÇA ABAIXO NOSSOS CLIENTES</h2>
        </div>

        <div class="row team-v3 margin-bottom-30">
		<?php 
        if ( $query->have_posts() ) {
            while ( $query->have_posts() ) {
            $query->the_post();
            $custom             = get_post_custom($post->ID);
            $cases_texto_destaque   =   $custom["clientes_texto_destaque"][0];    
            $cases_foto             =   $custom["clientes_foto"][0];    
        ?>
            <div class="col-md-3 col-sm-6" style="margin-bottom:20px;">
                <div class="team-img">
                    <img class="img-responsive" src="<?php echo $cases_foto; ?>" alt="<?php the_title(); ?>">
                    <div class="team-hover">
                        <a href="<?php the_permalink(); ?>">
                            <span><?php the_title(); ?></span>
                            <p><?php echo $cases_texto_destaque; ?></p>
                        </a>
                        <a href="<?php the_permalink(); ?>" class="btn btn-default btn-u-sm">Saiba Mais</a>
                    </div>

                </div>
            </div>
			
		<?php } } ?>
        </div>
        <div class="margin-bottom-40"></div>
 		
        <!-- End Cases Blocks -->
    </div><!--/container-->
  	<!-- End Content Part -->
			
<?php get_footer(); ?>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
        StyleSwitcher.initStyleSwitcher();
    });
</script>
