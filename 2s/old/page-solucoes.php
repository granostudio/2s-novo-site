<?php if (have_posts()): ?>
<?php while ( have_posts() ) : the_post(); ?>

<?php get_header(); ?>
<?php the_content(); ?>
<?php get_footer(); ?>

<?php endwhile;?>
<?php endif ?>