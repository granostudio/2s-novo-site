<div class="box-buscar">
    <form role="search" method="get" id="searchform">
      <div class="input-group">
        <input type="text" class="campo-buscar" name="s" id="s" placeholder="Digite o que está procurando ou escolha uma das categorias abaixo para filtrar os assuntos..." />
        <!-- <div class="input-group-btn">
          <button class="btn btn-default" type="submit" id="searchsubmit">
          <i class="glyphicon glyphicon-search"></i>
          </button>
         </div> -->
       </div>
    </form>
  </div>

<div class="menu-categorias">
  <ul>
    <a href="/blog/tecnologia/#inicio-posts"><li style="background-color: #1F72B6;">Tecnologias</li></a>
    <a href="/blog/negocio/#inicio-posts"><li style="background-color: #01B59C;">Negócios</li></a>
    <a href="/blog/inovacao/#inicio-posts"><li style="background-color: #FFCB27;">Inovação</li></a>
    <a href="/blog/servicos/#inicio-posts"><li style="background-color: #FF7C2E;">Serviços</li></a>
    <a href="/blog/acontece-na-2s/#inicio-posts"><li style="background-color: #FF5A46;margin-right: 0px;">Acontece na 2s</li></a>
  </ul>
</div>
<div class="menu-subcategorias">
  <ul>
  <?php

  //
  $cat_id = get_query_var('cat');
  $cat_ant = get_ancestors( $cat_id, 'category' );
  $child_cats_ant = get_categories( array('child_of' => $cat_ant[0]));//get children of parent category
  $child_cats = get_categories( array('child_of' => $cat_id));//get children of parent category
  if( $cat_id != 0 ){
    if(! empty($cat_ant)){
      echo "<li><strong>Subcategorias:</strong></li>";
      foreach( $child_cats_ant as $child_cat ){

          // //for each child category, give us the link and name
          echo '<li><a href=" ' . get_category_link( $child_cat->cat_ID ) . '/#inicio-posts ">' . $child_cat->name . '</a></li> ';

      }
    } elseif(! empty($child_cats)) {
      echo "<li><strong>Subcategorias:</strong></li>";
      foreach( $child_cats as $child_cat ){

          // //for each child category, give us the link and name
          echo '<li><a href=" ' . get_category_link( $child_cat->cat_ID ) . ' ">' . $child_cat->name . '</a></li> ';

      }
    }
  }


  ?>
</ul>
</div>
