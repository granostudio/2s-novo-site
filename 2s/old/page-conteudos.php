<?php
    $query= null;
    $query = new WP_Query(
            array(
                'post_type' => 'conteudos',
                'post_per_page' => -1)
            );
?>

<?php get_header(); ?>
    <!--=== Breadcrumbs v3 ===-->
    <div class="breadcrumbs breadcrumbs-light img-cases">
        <div class="container">
            <div class="titulos-breadcrumbs">
                <h1>Vídeos e E-books</h1>
            </div>
        </div>
    </div>
    <!--=== End Breadcrumbs v3 ===-->

     <!--=== Content Part ===-->
    <div class="container">
		
        <div class="row team-v4">
		<?php 
        if ( $query->have_posts() ) {
            while ( $query->have_posts() ) {
            $query->the_post();
            $custom             = get_post_custom($post->ID);
            $conteudo_titulo    =   $custom["conteudo_titulo"][0];    
            $conteudo_imagem    =   $custom["conteudo_imagem"][0];
        ?>
            <div class="col-md-3 col-sm-6" style="margin-bottom:20px;">
                <div class="team-img">
                    <img class="img-responsive" src="<?php echo $conteudo_imagem; ?>" alt="<?php echo $conteudo_titulo; ?>">
                    <div class="team-hover">
                        <a href="<?php the_permalink(); ?>">
                            <span><?php echo $conteudo_titulo; ?></span>
                        </a><br/>
                        <a href="<?php the_permalink(); ?>" class="btn btn-default btn-u-sm">Acesse este conteúdo</a>
                    </div>

                </div>
            </div>
			
        <?php } ?>
        <?php } else { ?>
            <div class="col-md-12 text-center" style="margin-bottom:20px;">
            <h3>Nenhum conteúdo (ebooks ou vídeos) encontrados</h3>
            <h4>Aguarde, em breve conteúdos ricos para download nesta página.</h4>
            </div>
        <?php } ?>
        </div>
        <div class="margin-bottom-40"></div>
 		
        <!-- End Cases Blocks -->
    </div><!--/container-->
  	<!-- End Content Part -->
			
<?php get_footer(); ?>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
        StyleSwitcher.initStyleSwitcher();
    });
</script>
