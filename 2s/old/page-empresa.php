<?php get_header(); ?>
    <!--=== Breadcrumbs v3 ===-->
    <div class="breadcrumbs breadcrumbs-light img-empresa">
        <div class="container">
            <div class="titulos-breadcrumbs">
                <h1>Conheça Melhor a 2S</h1>
            </div>
        </div>
    </div>
    <!--=== End Breadcrumbs v3 ===-->

    <?php if (have_posts()): ?>
    <?php while ( have_posts() ) : the_post(); 
    ?>

    <!--=== Container Part ===-->
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                

                    
                    <?php the_content(); ?>



            </div><!-- End col-9 -->
            <div class="col-md-3">

<?php
/*
<style>
.lateralmenu {width:245px; }
.lateralmenu a:link, .lateralmenu a:visited, .lateralmenu a:active {text-decoration: none; color:#000}
.lateralmenu a:hover {text-decoration: underline; color: #0071BB}
.lateralmenu ul {list-style-type:none; float:left; padding-left:0%;}
.lateralmenu hr {border: none; background-color: #00ADEF; height: 1px; width:264px; margin-bottom:0%; float:left}
.lateralmenu .tabelamenu {width:264px; margin: 0 auto; text-align:left; display:table}
.lateralmenu .tabelamenu .iconecell {width:41px; display:table-cell; padding-top:1.25em; vertical-align:middle}
.lateralmenu .tabelamenu .menucell {width:100%; display:table-cell; padding-left:1.25em; padding-top:1.25em; vertical-align:middle}
</style>
<div class="lateralmenu">
<div class="headline">
<h2>SOLUÇÕES</h2>
</div>
<div class="tabelamenu">
<ul>
<li>
<div class="iconecell"> <a href="/solucoes/colaboracao"><img src="/wp-content/themes/2s/assets/img/icocolabora.gif" alt="Colaboração" /></a></div>
<div class="menucell"> <a href="/solucoes/colaboracao">Colaboração</a></div></li>
<li>
<div class="iconecell"> <a href="/solucoes/iot"><img src="/wp-content/themes/2s/assets/img/icoiot.gif" alt="Internet das coisas" /></a></div>
<div class="menucell"> <a href="/solucoes/iot">Internet das coisas</a></div></li>
<li>
<div class="iconecell"> <a href="/solucoes/mobilidade"><img src="/wp-content/themes/2s/assets/img/icomobilidade.gif" alt="Mobilidade" /></a></div>
<div class="menucell"> <a href="/solucoes/mobilidade">Mobilidade</a></div></li>
<li>
<div class="iconecell"> <a href="/solucoes/seguranca"><img src="/wp-content/themes/2s/assets/img/icoseguranca.gif" alt="Segurança" /></a></div>
<div class="menucell"> <a href="/solucoes/seguranca">Segurança</a></div></li>
<li>
<div class="iconecell"> <a href="/solucoes/datacenter"><img src="/wp-content/themes/2s/assets/img/icodatacenter.gif" alt="Datacenter" /></a></div>
<div class="menucell"> <a href="/solucoes/datacenter">Datacenter</a></div></li>
</ul></div>
<div class="headline">
<h2>SERVIÇOS</h2>
</div>
<div class="tabelamenu">
<ul>
<li>
<div class="iconecell"> <a href="/servicos"><img src="/wp-content/themes/2s/assets/img/icomanutencao.gif" alt="Manutenção" /></a></div>
<div class="menucell"> <a href="/servicos">Manutenção</a></div></li>
<li>
<div class="iconecell"> <a href="/servicos"><img src="/wp-content/themes/2s/assets/img/icorequisicao.gif" alt="Requisição de Serviços" /></a></div>
<div class="menucell"> <a href="/servicos">Requisição de Serviços</a></div></li>
<li>
<div class="iconecell"> <a href="/servicos"><img src="/wp-content/themes/2s/assets/img/icominitoramento.gif" alt="Monitoramento" /></a></div>
<div class="menucell"> <a href="/servicos">Monitoramento</a></div></li>
<li>
<div class="iconecell"> <a href="/servicos"><img src="/wp-content/themes/2s/assets/img/icogerenremoto.gif" alt="Gerenciamento Remoto" /></a></div>
<div class="menucell"> <a href="/servicos">Gerenciamento Remoto</a></div></li>
<li>
<div class="iconecell"> <a href="/servicos"><img src="/wp-content/themes/2s/assets/img/icogerenon.gif" alt="Gerenciamento Onsite" /></a></div>
<div class="menucell"> <a href="/servicos">Gerenciamento Onsite</a></div></li>
<li>
<div class="iconecell"> <a href="/servicos"><img src="/wp-content/themes/2s/assets/img/icomeraki.gif" alt="Meraki as a Service" /></a></div>
<div class="menucell"> <a href="/servicos">Meraki as a Service <small>(Nuvem)</small></a></div></li>
<li>
<div class="iconecell"> <a href="/servicos"><img src="/wp-content/themes/2s/assets/img/icoprime.gif" alt="Prime as a Service" /></a></div>
<div class="menucell"> <a href="/servicos">Prime as a Service <small>(Nuvem)</small></a></div></li>
</ul></div>
</div>
*/
?>
                <?php get_sidebar(); ?> 
            </div><!-- End col-3 -->

        </div> <!-- End row--> 
    </div>
    
    </div>
    <!--=== End Container Part ===-->
    <?php endwhile;?>
    <?php endif ?>

			
<?php get_footer(); ?>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
        StyleSwitcher.initStyleSwitcher();
    });
</script>
