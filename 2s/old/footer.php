<?php
    if (! (is_page("trabalhe-conosco") || is_page("blog")/* || is_category() || is_single()*/ ) || (is_single() && is_singular('clientes') ) || (is_single() && is_singular('conteudos') ) || (is_single() && is_singular('clippings') ) || (is_single() && is_singular('noticias') ) ) {
?>

        <!--=== Footer Version 1 ===-->
        <div class="footer-v1">
            <div class="footer">
                <div class="container">

                    <div class="row">
                        <div class="col-sm-2 col-sm-offset-5 text-center">
                            <img class="new-ident img-responsive" src="<?php bloginfo('template_url') ?>/assets/img/bolinhas2s.png" alt="new-ident">
                        </div>
                    </div>

                    <div class="row">
                        <!-- About -->
<?php
/*
<a href="/"><img id="logo-footer" class="footer-logo" src="/wp-content/themes/2s/assets/img/logo-2s-white.png" width="180px" alt=""></a>
<p>A 2S é uma <strong>integradora</strong> de soluções de infraestrutura, sendo uma das principais parceiras <strong>Gold e 1st Tier Cisco no Brasil</strong>.
*/
?>
                        <div class="col-md-3">
                            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("sidebar-3") ) : ?>
                            <?php get_sidebar("sidebar-3"); ?>
                            <?php endif; ?>
                        </div><!--/col-md-3-->
                        <!-- End About -->

                        <!-- gptw -->
  <?php
/*
<a href="javascript:void(0);"><img id="logo-footer" class="footer-logo img-responsive" src="/wp-content/themes/2s/assets/img/gptw.png" alt=""></a>
*/
?>
                        <div class="col-xs-4 col-md-3">
                            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("sidebar-4") ) : ?>
                            <?php get_sidebar("sidebar-4"); ?>
                            <?php endif; ?>
                        </div><!--/col-md-2-->
                        <!-- End gptw -->

                        <!-- Address -->
<?php
/*
<div class="headline"><h2>Contato</h2></div>
<address class="md-margin-bottom-40">
    Rua Arizona, 1366 – 10º andar<br>
    CEP 04567-900 | São Paulo - SP<br>
    +55 11 3305.1200 | <a href="mailto:atendimento@2s.com.br" class="">atendimento@2s.com.br</a>
</address>
*/
?>
                        <div class="col-xs-8 col-md-2">
                            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("sidebar-5") ) : ?>
                            <?php get_sidebar("sidebar-5"); ?>
                            <?php endif; ?>
                        </div><!--/col-md-3-->
                        <!-- End Address -->

                        <!-- Address -->
<?php
/*
<div class="headline"><h2>Redes Sociais</h2></div>
<ul class="footer-socials list-inline">
    <li>
        <a href="https://www.facebook.com/2SInovacoesTecnologicas" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook" target="_blank">
            <i class="fa fa-2x fa-facebook-square"></i>
        </a>
    </li>
    <li>
        <a href="https://www.youtube.com/user/2sinovacoes" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Youtube" target="_blank">
            <i class="fa fa-2x fa-youtube-square"></i>
        </a>
    </li>
    <li><a href="https://www.linkedin.com/company/2s-inova-es-tecnol-gicas" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Linkedin" target="_blank"><i class="fa fa-2x fa-linkedin-square"></i></a></li>
</ul>
*/
?>
                        <div class="col-xs-12 col-md-4">
                            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("sidebar-6") ) : ?>
                            <?php get_sidebar("sidebar-6"); ?>
                            <?php endif; ?>
                        </div><!--/col-md-3-->
                        <!-- End Address -->
                    </div>
                </div>
            </div><!--/footer-->

            <div class="copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-6 col-md-6">
                            <p>
                               2S Inovações Tecnológicas<span class="none-mobile"> - Todos os direitos reservados</span>
                            </p>
                        </div>

                        <div class="col-xs-6 col-md-6">
                            <p class="por">
                               <a href="http://www.yodesign.com.br/" target="_blank">OY!Design</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div><!--/copyright-->
        </div>
        <!--=== End Footer Version 1 ===-->
    </div><!--/wrapper-->

    <!-- JS Global Compulsory -->
    <script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/plugins/jquery/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>

    <!-- JS Implementing Plugins -->
    <script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/plugins/back-to-top.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/plugins/smoothScroll.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/plugins/layer-slider/layerslider/js/greensock.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/plugins/layer-slider/layerslider/js/layerslider.transitions.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/plugins/layer-slider/layerslider/js/layerslider.kreaturamedia.jquery.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/plugins/cube-portfolio/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/plugins/gmap/gmap.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/plugins/sky-forms-pro/skyforms/js/jquery.form.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/plugins/sky-forms-pro/skyforms/js/jquery.validate.min.js"></script>


    <!-- JS Customization -->
    <script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/custom.js"></script>

    <!-- JS Page Level -->
    <script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/app.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/plugins/layer-slider.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/plugins/style-switcher.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/plugins/owl-carousel.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/plugins/owl-recent-works.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/plugins/cube-portfolio/cube-portfolio-lightbox.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/forms/contact.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/pages/page_contacts.js"></script>

    <!-- For Validation -->
    <script src="<?php bloginfo('template_url') ?>/assets/js/plugins/validation.js"></script>

    <!--[if lt IE 9]>
        <script src="<?php bloginfo('template_url') ?>/assets/plugins/respond.js"></script>
        <script src="<?php bloginfo('template_url') ?>/assets/plugins/html5shiv.js"></script>
        <script src="<?php bloginfo('template_url') ?>/assets/plugins/placeholder-IE-fixes.js"></script>
    <!--[endif]-->

    <!--[if lt IE 9]>
        <script src="<?php bloginfo('template_url') ?>/assets/plugins/sky-forms-pro/skyforms/js/sky-forms-ie8.js"></script>
    <!--[endif]-->
    <!--[if lt IE 10]>
        <script src="<?php bloginfo('template_url') ?>/assets/plugins/sky-forms-pro/skyforms/js/jquery.placeholder.min.js"></script>
    <!--[endif]-->


    <!--[if lt IE 9]>
        <script src="<?php bloginfo('template_url') ?>/assets/plugins/sky-forms-pro/sky-forms/js/sky-forms-ie8.js"></script>
    <!--[endif]-->

    <!--[if lt IE 10]>
        <script src="<?php bloginfo('template_url') ?>/assets/plugins/sky-forms-pro/sky-forms/js/jquery.placeholder.min.js"></script>
    <!--[endif]-->

    <?php the_field("footer_scripts"); ?>

<?php } elseif(is_page("trabalhe-conosco")) { ?>

    <!--site-footer-->
    <footer class="site-footer section-spacing">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <p class="scroll-top"><a href="#"><i class="fa fa-chevron-up"></i></a></p>

            <!--social-->

            <ul class="social">
              <li><a href="https://www.linkedin.com/company/2s-inova-es-tecnol-gicas" target="_blank"><i class="fa fa-linkedin" style="margin-top: 10px;"></i></a></li>
              <li><a href="https://www.facebook.com/2SInovacoesTecnologicas" target="_blank"><i class="fa fa-facebook"  style="margin-top: 10px;"></i></a></li>
              <li><a href="https://www.youtube.com/user/2sinovacoes" target="_blank"><i class="fa fa-youtube"  style="margin-top: 10px;"></i></a></li>
            </ul>

            <!--social end-->

            <small>&copy; Copyright 2S. Todos os direitos reservados.</small>

          </div>
        </div>
      </div>
    </footer>
    <!--site-footer end-->

    <!--PRELOAD-->
    <div id="preloader">
      <div id="status"></div>
    </div>
    <!--end PRELOAD-->

</body>
</html>

    <script src="<?php bloginfo('template_url') ?>/oportunidades/js/plugins.min.js"></script>
    <script src="<?php bloginfo('template_url') ?>/oportunidades/js/main.js"></script>



<?php } else { ?>

      <!-- .site-footer -->
      <footer class="site-footer wrapper" role="contentinfo">
        <!-- .row -->
        <div class="row">

          <!-- .site-info -->
          <div class="site-info">2S Inovações Tecnológicas - Todos os direitos reservados</div>
          <!-- .site-info -->

        </div>
        <!-- .row -->
      </footer>
      <!-- .site-footer -->

    </div>
    <!-- #PAGE -->


    <!-- SCRIPTS -->
    <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?php bloginfo("template_url") ?>/blog/js/jquery-1.8.3.min.js"><\/script>')</script>

    <!-- SCRIPTS -->
    <script src="<?php bloginfo("template_url") ?>/blog/js/detectmobilebrowser.js"></script>
    <script src="<?php bloginfo("template_url") ?>/blog/js/modernizr.js"></script>
    <script src="<?php bloginfo("template_url") ?>/blog/js/jquery.imagesloaded.min.js"></script>
    <script src="<?php bloginfo("template_url") ?>/blog/js/jquery.fitvids.js"></script>
    <script src="<?php bloginfo("template_url") ?>/blog/js/google-code-prettify/prettify.js"></script>
    <script src="<?php bloginfo("template_url") ?>/blog/js/jquery.uniform.min.js"></script>

    <!-- InstanceBeginEditable name="body-end" -->
    <script src="<?php bloginfo("template_url") ?>/blog/js/jquery.isotope.min.js"></script>
    <script src="<?php bloginfo("template_url") ?>/blog/js/jquery.flexslider-min.js"></script>
    <script src="<?php bloginfo("template_url") ?>/blog/js/mediaelement/mediaelement-and-player.min.js"></script>
    <script src="<?php bloginfo("template_url") ?>/blog/js/jquery.fancybox-1.3.4.pack.js"></script>
    <!-- InstanceEndEditable -->

    <script src="<?php bloginfo("template_url") ?>/blog/js/main.js"></script>




<?php } ?>

    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-36467095-3', 'auto');
      ga('send', 'pageview');

      $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
      });

    </script>

</body>
</html>
