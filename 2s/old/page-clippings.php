<?php
    $query= null;
    $query = new WP_Query(
            array(
                'post_type' => 'clippings',
                'post_per_page' => -1)
            );
?>

<?php get_header(); ?>
    <!--=== Breadcrumbs v3 ===-->
    <div class="breadcrumbs breadcrumbs-light img-cases">
        <div class="container">
            <div class="titulos-breadcrumbs">
                <h1>Clippings</h1>
            </div>
        </div>
    </div>
    <!--=== End Breadcrumbs v3 ===-->

     <!--=== Content Part ===-->
    <div class="container">
		
        <div class="row team-v4">
		<?php 
        if ( $query->have_posts() ) {
            while ( $query->have_posts() ) {
            $query->the_post();
            $custom             = get_post_custom($post->ID);
            $conteudo_clipping    =   $custom["clipping_titulo"][0];
        ?>
            <div class="col-md-3 col-sm-6" style="margin-bottom:20px;">
                <div class="team-img">
                    <div class="team-hover">
                        <a href="<?php the_permalink(); ?>">
                            <span><?php echo get_the_title(); ?></span><br/>
                            <small><time class="entry-date" datetime="<?php echo get_the_date('d-m'); ?>"><?php echo get_the_date('d/m/Y'); ?></time></small>
                        </a><br/>
                        <a href="<?php the_permalink(); ?>" class="btn btn-default btn-u-sm">Mais sobre esta notícia</a>
                    </div>

                </div>
            </div>
			
		<?php } } ?>
        </div>
        <div class="margin-bottom-40"></div>
 		
        <!-- End Cases Blocks -->
    </div><!--/container-->
  	<!-- End Content Part -->
			
<?php get_footer(); ?>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
        StyleSwitcher.initStyleSwitcher();
    });
</script>
