<?php if (have_posts()): ?>
<?php while ( have_posts() ) : the_post(); ?>

<?php get_header(); ?>

<div class="pagina-nova">

<div class="header">
  <h1>Tecnologia em áreas de negócio<img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/icon_negocios.png" alt=""></h1>
</div>

<!-- <div class="video">
  <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/video_negocios.png" alt="">
</div> -->

<div class="banner-area-tecnologia">
  <p>A criação de processos mais dinâmicos e eficientes para alavancar o desempenho e os resultados de sua empresa.</p>
</div>

<div class="container">
  <div class="row col-md-10 col-md-offset-1">
    <div class="col-sm-12">
      <div class="headline">
        <h2>Em quais áreas os benefícios da Transformação Digital se aplicam?</h2>
      </div>
      <div class="col-sm-12 box-opcoes">
      <div class="item item1">
        <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/logistica2.png" alt="">
      </div>
      <div class="item item2">
        <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/rh2.png" alt="">
      </div>
      <div class="item item3">
        <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/seguranca_do_trabalho2.png" alt="">
      </div>
      <div class="item item4">
        <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/fabricas2.png" alt="">
      </div>
      <div class="item item5">
        <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/marketing2.png" alt="">
      </div>
      <div class="content-out2">
        <div class="areas content1">
          <div class="triangulo1"></div>
          <div class="col-sm-12">
            <p>As indústrias de diversos setores são as mais afetadas pelo desempenho logístico. A boa notícia é que a TI pode direcionar grande parte dos desafios da área, centralizando e gerenciando as informações geradas por diferentes máquinas, sensores e profissionais em um único painel de controle.</p>
            <p>As soluções tecnológicas desenhadas pela 2S permitem à empresa:</p>
          </div>
          <div class="col-sm-6">
            <!-- <ul class="lista2">
              <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/caminhao.png" alt="" style="margin-left:-10px;"><font color="#2072B7"><p>Gerenciar de forma mais efetiva o recebimento de matéria prima</font>, a partir de um sistema inteligente de descarregamento e despacho, evitando filas de caminhões;</p></li>
              <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/prancheta.png" alt="" style="margin-left:10px;"><font color="#2072B7"><p>Monitorar o comportamento de operadores</font> de máquinas e empilhadeiras e disparar avisos e orientações quando necessário, em tempo real;</p></li>
              <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/caminhos.png" alt=""><font color="#2072B7"><p>Prover informações integradas</font> ao gestor sobre as frotas. Por exemplo, se os veículos estão sendo usados de forma adequada, se estão circulando nas áreas corretas e se a quantidade de veículos está adequada à demanda do setor;</p></li>
            </ul> -->
            <div class="col-md-4 lista2"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/caminhao.png" alt=""></div>
            <div class="col-md-8 lista3"><font color="#2072B7"><p>Gerenciar de forma mais efetiva o recebimento de matéria prima</font>, a partir de um sistema inteligente de descarregamento e despacho, evitando filas de caminhões;</p></div>
            <div class="col-md-4 lista2"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/prancheta.png" alt=""></div>
            <div class="col-md-8 lista3"><font color="#2072B7"><p>Monitorar o comportamento de operadores</font> de máquinas e empilhadeiras e disparar avisos e orientações quando necessário, em tempo real;</p></div>
            <div class="col-md-4 lista2"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/caminhos.png" alt=""></div>
            <div class="col-md-8 lista3"><font color="#2072B7"><p>Prover informações integradas</font> ao gestor sobre as frotas. Por exemplo, se os veículos estão sendo usados de forma adequada, se estão circulando nas áreas corretas e se a quantidade de veículos está adequada à demanda do setor;</p></div>
          </div>
          <div class="col-sm-6">
            <!-- <ul class="lista3">
              <li style="margin-top=45px;"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/cartao.png" alt="" style="margin-top:-34px;"><font color="#2072B7"><p>Interligar a gestão de logística</font> com o sistema de faturamento e emissão de nota;</p></li>
              <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/container.png" alt="" style="margin-top:-36px;"><font color="#2072B7"><p>Otimizar</font> o armazenamento e picking de matéria-prima e produtos acabados;</p></li>
              <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/mundo.png" alt="" style="margin-top:-35px;"><font color="#2072B7"><p>Rastrear e monitorar os veículos</font> que estão em deslocamento, dentro e fora da planta;</p></li>
              <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/atendente.png" alt=""><font color="#2072B7"><p>Fornecer informações em tempo real</font> sobre o destino e as condições de entrada do produto;</p></li>
            </ul> -->
            <div class="col-md-4 lista2"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/cartao.png" alt=""></div>
            <div class="col-md-8 lista3"><font color="#2072B7"><p>Interligar a gestão de logística</font> com o sistema de faturamento e emissão de nota;</p></div>
            <div class="col-md-4 lista2"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/container.png" alt=""></div>
            <div class="col-md-8 lista3"><font color="#2072B7"><p>Otimizar</font> o armazenamento e picking de matéria-prima e produtos acabados;</p></div>
            <div class="col-md-4 lista2"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/mundo.png" alt=""></div>
            <div class="col-md-8 lista3"><font color="#2072B7"><p>Rastrear e monitorar os veículos</font> que estão em deslocamento, dentro e fora da planta;</p></div>
            <div class="col-md-4 lista2"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/atendente.png" alt=""></div>
            <div class="col-md-8 lista3"><font color="#2072B7"><p>Fornecer informações em tempo real</font> sobre o destino e as condições de entrada do produto;</p></div>
          </div>
        </div>
        <div class="areas content2">
          <div class="triangulo2"></div>
          <div class="col-sm-12">
            <p>Nunca TI e pessoas tiveram tanta intimidade. Mais integrados do que nunca, esses dois recursos preciosos da companhia, quando combinados, podem aprimorar diversos processos da empresa e trazer benefícios valiosos:</p>
          </div>
          <div class="col-sm-6">
            <!-- <ul class="lista3">
              <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/24h.png" alt="" style="width: 21%;" ><p><font color="#01B59D">Melhorar a produtividade dos colaboradores</font>, por meio do uso de ferramentas de mobilidade e colaboração;</p></li>
              <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/turnover.png" alt="" style="margin-top:-34px; width: 21%;"><p><font color="#01B59D">Reduzir a taxa de turnover</font>, a partir da criação de ambientes mais flexíveis e colaborativos de trabalho;</p></li>
            </ul> -->
            <div class="col-md-4 lista2 icones2"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/24h.png" alt=""></div>
            <div class="col-md-8 lista3"><p><font color="#01B59D">Melhorar a produtividade dos colaboradores</font>, por meio do uso de ferramentas de mobilidade e colaboração;</p></div>
            <div class="col-md-4 lista2 icones2"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/turnover.png" alt=""></div>
            <div class="col-md-8 lista3"><p><font color="#01B59D">Reduzir a taxa de turnover</font>, a partir da criação de ambientes mais flexíveis e colaborativos de trabalho;</p></div>
          </div>
          <div class="col-sm-6">
            <!-- <ul class="lista2">
              <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/forca.png" alt="" style="margin-top:-35px;width:16%;left:5%"><p><font color="#01B59D">Capacitar a força de trabalho e</font> garantir uma<font color="#01B59D"> comunicação mais unificada e assertiva</font>, independentemente da localização dos colaboradores, a partir de uma plataforma integrada e dinâmica de comunicação e treinamento;</p></li>
              <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/custos.png" alt="" style="margin-top:-40px;width:21%;left:3%"><p><font color="#01B59D">Reduzir custos</font> com deslocamento para entrevistas e reuniões, utilizando tecnologias avançadas de videoconferência;</p></li>
            </ul> -->
            <div class="col-md-4 lista2 icones2"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/forca.png" alt=""></div>
            <div class="col-md-8 lista3"><p><font color="#01B59D">Capacitar a força de trabalho e</font> garantir uma<font color="#01B59D"> comunicação mais unificada e assertiva</font>, independentemente da localização dos colaboradores, a partir de uma plataforma integrada e dinâmica de comunicação e treinamento;</p></div>
            <div class="col-md-4 lista2 icones2"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/custos.png" alt=""></div>
            <div class="col-md-8 lista3"><p><font color="#01B59D">Reduzir custos</font> com deslocamento para entrevistas e reuniões, utilizando tecnologias avançadas de videoconferência;</p></div>
          </div>
        </div>
        <div class="areas content3">
          <div class="triangulo3"></div>
          <div class="col-sm-12">
            <p>Muitas indústrias buscam se transformar para fazer com que todos os colaboradores estejam em segurança em suas atividades. A Transformação Digital pode atuar nesse sentido garantindo:</p>
          </div>
          <div class="col-sm-6">
            <!-- <ul class="lista3">
              <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/leitor.png" alt="" style="margin-top:-36px;" width="21%"><p><font color="#FFCB26">Uso correto de EPIs </font>em áreas de risco, por meio de leitores de verificação;</p></li>
              <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/mapa.png" alt="" style="margin-top:-30px;" width="21%"><p><font color="#FFCB26">Visibilidade e rastreabilidade de colaboradores </font>em áreas de risco;</p></li>
            </ul> -->
            <div class="col-md-4 lista2 icones2"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/leitor.png" alt=""></div>
            <div class="col-md-8 lista3"><p><font color="#FFCB26">Uso correto de EPIs </font>em áreas de risco, por meio de leitores de verificação;</p></div>
            <div class="col-md-4 lista2 icones2"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/mapa.png" alt=""></div>
            <div class="col-md-8 lista3"><p><font color="#FFCB26">Visibilidade e rastreabilidade de colaboradores </font>em áreas de risco;</p></div>
          </div>
          <div class="col-sm-6">
            <!-- <ul class="lista2">
              <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/caminhao_amarelo.png" alt="" style="margin-top:-30px;" width="29%"><p><font color="#FFCB26">Visibilidade e rastreabilidade de veículos, máquinas e ativos</font>, com alertas de circulação quando em áreas proibidas e/ou de risco;</p></li>
              <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/contrato.png" alt="" style="margin-top:-31px;margin-left:22px;" width="20%"><p><font color="#FFCB26">Controle total dos veículos </font>que transitam nas dependências da empresa (acesso, localização, velocidade, perfil do motorista, regras de condução);</p></li>
            </ul> -->
            <div class="col-md-4 lista2 icones2"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/caminhao_amarelo.png" alt="" width="100%"></div>
            <div class="col-md-8 lista3"><p><font color="#FFCB26">Visibilidade e rastreabilidade de veículos, máquinas e ativos</font>, com alertas de circulação quando em áreas proibidas e/ou de risco;</p></div>
            <div class="col-md-4 lista2 icones2"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/contrato.png" alt=""></div>
            <div class="col-md-8 lista3"><p><font color="#FFCB26">Controle total dos veículos </font>que transitam nas dependências da empresa (acesso, localização, velocidade, perfil do motorista, regras de condução);</p></div>
          </div>
          <div class="col-sm-12" style="margin-top:50px;">
            <p>Tudo integrado a um sistema de gerenciamento e comunicação que permite a emissão de avisos e alertas, sonoros e visuais, para que condutas fora dos padrões de segurança sejam corrigidas em tempo real.</p>
          </div>
        </div>
        <div class="areas content4">
          <div class="triangulo4"></div>
          <div class="col-sm-12">
            <p>Com foco na indústria 4.0, a 2S desenvolve soluções capazes de digitalizar processos e integrar máquinas, dispositivos, áreas e pessoas, permitindo a comunicação, compartilhamento de informações e a tomada de ações em tempo real.</p>
            <p>Conecte sua linha de produção e aumente a eficiência dos processos, garantindo:</p>
          </div>
          <div class="col-sm-6">
            <!-- <ul class="lista2">
              <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/computador.png" alt="" style="margin-top:-35px;" width="20%"><p><font color="#FF7C2F">Redução de custos operacionais</font>, por meio de um sistema de sensoriamento integrado à rede, permitindo a troca de informações e emissão de alertas em tempo real;</p></li>
              <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/relogio_laranja.png" alt="" style="margin-top:-35px;" width="20%"><p><font color="#FF7C2F">Aumento de receita</font>, a partir da redução do tempo de parada das máquinas. Com um sistema de rastreamento inteligente, é possível localizar e administrar os recursos de forma mais rápida, com maior visibilidade dos processos. Formulários e dashboards e apoio de especialistas remotos, via soluções de colaboração, também auxiliam nesse sentido;</p></li>
            </ul> -->
            <div class="col-md-4 lista2 icones2"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/computador.png" alt=""></div>
            <div class="col-md-8 lista3"><p><font color="#FF7C2F">Redução de custos operacionais</font>, por meio de um sistema de sensoriamento integrado à rede, permitindo a troca de informações e emissão de alertas em tempo real;</p></div>
            <div class="col-md-4 lista2 icones2"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/relogio_laranja.png" alt=""></div>
            <div class="col-md-8 lista3"><p><font color="#FF7C2F">Aumento de receita</font>, a partir da redução do tempo de parada das máquinas. Com um sistema de rastreamento inteligente, é possível localizar e administrar os recursos de forma mais rápida, com maior visibilidade dos processos. Formulários e dashboards e apoio de especialistas remotos, via soluções de colaboração, também auxiliam nesse sentido;</p></div>
          </div>
          <div class="col-sm-6">
            <!-- <ul class="lista2">
              <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/escudo.png" alt="" style="margin-top:-35px;" width="20%"><p><font color="#FF7C2F">Aumento da segurança física</font>, com a integração de sistemas de videovigilância, catracas de acesso, sensores e cercas eletrônicas, além do sistema de controle e gestão de veículos dentro das áreas das fábricas.</p></li>
              <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/notebook.png" alt="" style="margin-top:-29px;margin-left:11px;" width="20%"><p><font color="#FF7C2F">Aumento da segurança lógica</font>, com a identificação e s autenticação de usuários e dispositivos conectados ao ambiente e sistema de proteção contra ataques cibernéticos.</p></li>
            </ul> -->
            <div class="col-md-4 lista2 icones2"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/escudo.png" alt=""></div>
            <div class="col-md-8 lista3"><p><font color="#FF7C2F">Aumento da segurança física</font>, com a integração de sistemas de videovigilância, catracas de acesso, sensores e cercas eletrônicas, além do sistema de controle e gestão de veículos dentro das áreas das fábricas.</p></div>
            <div class="col-md-4 lista2 icones2"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/notebook.png" alt=""></div>
            <div class="col-md-8 lista3"><p><font color="#FF7C2F">Aumento da segurança lógica</font>, com a identificação e s autenticação de usuários e dispositivos conectados ao ambiente e sistema de proteção contra ataques cibernéticos.</p></div>
          </div>
        </div>
        <div class="areas content5">
          <div class="triangulo5"></div>
          <div class="col-sm-12">
            <p>A Transformação Digital está revolucionando empresas de todos os setores e portes com os mesmos objetivos: criar experiências personalizadas ao cliente, remodelar os negócios e proporcionar a inovação para se destacar frente à concorrência.</p>
            <p>Em uma estratégia de marketing e promoção de vendas, as ações possíveis são numerosas:</p>
            <!-- <ul class="lista3">
              <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/melhoria.png" alt="" style="margin-top:-35px;" width="8%"><p><font color="#FF5A45">Melhoria da experiência do consumidor</font>, a partir de comunicação e promoções personalizadas, criação de pontos de venda interativos e canais de autoatendimento;</p></li>
              <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/interatividade.png" alt="" style="margin-top:-35px;" width="8%"><p><font color="#FF5A45">Interatividade com os clientes</font>, possibilitando direcioná-los aos setores que sejam de seu maior interesse;</p></li>
              <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/dinamica.png" alt="" style="margin-top:-25px;" width="8%"><p><font color="#FF5A45">Adequação inteligente e dinâmica do posicionamento de produtos nas gôndolas</font>, promovendo incremento das vendas (cross e upselling), aumento do ticket médio por cliente e mais facilidade de apresentação no retorno do investimento das ações de marketing;</p></li>
            </ul> -->
            <div class="row">
              <div class="col-sm-2 lista2"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/melhoria.png" alt="" style="width:60%"></div>
              <div class="col-sm-10 lista3"><p><font color="#FF5A45">Melhoria da experiência do consumidor</font>, a partir de comunicação e promoções personalizadas, criação de pontos de venda interativos e canais de autoatendimento;</p></div>
            </div>
            <div class="row">
              <div class="col-sm-2 lista2"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/interatividade.png" alt="" style="width:60%"></div>
              <div class="col-sm-10 lista3"><p><font color="#FF5A45">Interatividade com os clientes</font>, possibilitando direcioná-los aos setores que sejam de seu maior interesse;</p></div>
            </div>
            <div class="row">
              <div class="col-sm-2 lista2"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/dinamica.png" alt="" style="width:70%"></div>
              <div class="col-sm-10 lista3"><p><font color="#FF5A45">Adequação inteligente e dinâmica do posicionamento de produtos nas gôndolas</font>, promovendo incremento das vendas (cross e upselling), aumento do ticket médio por cliente e mais facilidade de apresentação no retorno do investimento das ações de marketing;</p></div>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>
</div>

<script>
  jQuery(' .item1 ').click(function() {
    $( ".content1" ).fadeIn( "slow" );
    $( ".content2" ).fadeOut( "slow" );
    $( ".content3" ).fadeOut( "slow" );
    $( ".content4" ).fadeOut( "slow" );
    $( ".content5" ).fadeOut( "slow" );
  });
  jQuery(' .item2 ').click(function() {
    $( ".content1" ).fadeOut( "slow" );
    $( ".content2" ).fadeIn( "slow" );
    $( ".content3" ).fadeOut( "slow" );
    $( ".content4" ).fadeOut( "slow" );
    $( ".content5" ).fadeOut( "slow" );
  });
  jQuery(' .item3 ').click(function() {
    $( ".content1" ).fadeOut( "slow" );
    $( ".content2" ).fadeOut( "slow" );
    $( ".content3" ).fadeIn( "slow" );
    $( ".content4" ).fadeOut( "slow" );
    $( ".content5" ).fadeOut( "slow" );
  });
  jQuery(' .item4 ').click(function() {
    $( ".content1" ).fadeOut( "slow" );
    $( ".content2" ).fadeOut( "slow" );
    $( ".content3" ).fadeOut( "slow" );
    $( ".content4" ).fadeIn( "slow" );
    $( ".content5" ).fadeOut( "slow" );
  });
  jQuery(' .item5 ').click(function() {
    $( ".content1" ).fadeOut( "slow" );
    $( ".content2" ).fadeOut( "slow" );
    $( ".content3" ).fadeOut( "slow" );
    $( ".content4" ).fadeOut( "slow" );
    $( ".content5" ).fadeIn( "slow" );
  });
</script>

<div class="container">
  <div class="row col-md-10 col-md-offset-1">
    <div class="headline">
      <h2>Por que falar sobre transformação digital em áreas de negócios?</h2>
    </div>
    <p>Dentro das áreas de negócio de uma organização - marketing, recursos humanos e comunicação interna, segurança do trabalho, linha de produção, logística, finanças e vendas - existe um mundo de processos que são profundamente impactados pela tecnologia. A Transformação Digital afeta positivamente a dinâmica das empresas como um todo, criando caminhos cada vez mais rápidos para o sucesso. </p>
  </div>
</div>

<div class="container">
  <div class="row col-md-10 col-md-offset-1">
  <div class="headline">
    <h2>Por que é um caminho mandatório?</h2>
  </div>
    <div class="col-sm-6">
      <ul class="mandatorio">
        <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/conquista.png" alt=""><p>A Transformação Digital cria <font color="#01B59D">diferencial competitivo</font>, aumentando as chances da empresa de se destacar no mercado;</p><hr style="border-top: 2px solid #01B59D;"></li>
        <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/procura.png" alt=""><p>A pesquisa "Antes da TI, a estratégia", realizada pela IT Mídia com CIOs das 500 maiores empresas do Brasil, mostra que, nos processos de decisão para aquisição de produtos e serviços, 56% dos respondentes dizem que <font color="#FF5A45">TI e áreas de negócios entram em consenso</font> sobre as melhores soluções a serem adquiridas;</p><hr style="border-top: 2px solid #FF5A45;"></li>
        <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/evolucao.png" alt=""><p>Pesquisas mostram que empresas que investem em inovação saem na <font color="#FFCB26">frente de seus concorrentes</font>;</p><hr style="border-top: 2px solid #FFCB26;"></li>
      </ul>
    </div>
    <div class="col-sm-6">
      <ul class="mandatorio">
        <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/tablet.png" alt=""><p>O mesmo estudo aponta que <font color="#014D5D">posicionar a TI como uma área estratégica do negócio</font> está entre os dois maiores desafios da área para 2017 para 35% dos CIOs;</p><hr style="border-top: 2px solid #014D5D;"></li>
        <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/relogio.png" alt=""><p>A aplicação da tecnologia em processos das áreas de negócios torna as <font color="#2072B7">atividades mais ágeis e dinâmicas</font>, permitido a geração de informações e a tomada de ações em tempo real sobre os recursos e as necessidades da companhia;</p><hr style="border-top: 2px solid #2072B7;"></li>
        <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/investimento.png" alt=""><p>Segundo o Gartner, <font color="#FF7C2F">90% dos investimentos de TI serão deslocados para as áreas de negócios</font> até 2020;</p><hr style="border-top: 2px solid #FF7C2F;"></li>
      </ul>
    </div>
  </div>
</div>



<div class="container">
  <div class="row col-md-10 col-md-offset-1">
    <div class="col-sm-12">
      <div class="headline">
        <h2>Como a 2S pode ajudar a sua empresa nessa transformação?</h2>
      </div>
    </div>
    <div class="col-sm-6 transformacao">
      <p>A partir do fornecimento de soluções tecnológicas que garantam a infraestrutura necessária para o movimento da Transformação Digital</p>
      <ul>
        <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/internet.png" alt=""><p>Internet das Coisas</p></li>
        <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/wifi.png" alt=""><img src="" alt=""><p>Mobilidade</p></li>
        <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/data-center.png" alt=""><p>Data Center</p></li>
        <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/seguranca.png" alt=""><p>Segurança</p></li>
        <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/colaboracao.png" alt=""><p>Colaboração</p></li>
      </ul>
    </div>
    <div class="col-sm-6 transformacao">
      <p style="margin-bottom:67px;">A partir da abordagem direcionada a desafios de negócios em algumas das principais verticais de mercado</p>
      <ul>
        <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/sacola.png" alt="" width=12% style="margin-top:-7px;"><p>Varejo</p></li>
        <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/papel.png" alt="" width=10% style="margin-top:-5px;"><p>Papel & Celulose</p></li>
        <li><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/negocios/ferramenta.png" alt="" width=10% style="margin-top:-3px;"><p>Mineração & matalurgia</p></li>
      </ul>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <div class="relatedposts">
        <h1 class="tit7 text-center" style="margin-top:30px; margin-bottom:30px">Artigos Relacionados</h1>
      </div>
    </div>
  </div>
</div>

<div class="container">

    <div class="margin-bottom-60"></div>

    <?php
    $query= null;
    $query = new WP_Query(
    array(
    'post_type' => 'destaque',
    'posts_per_page' => 6)
    );
    ?>

    <div class="row news-v2 margin-bottom-80">

    <?php
    if ( $query->have_posts() ) {
        while ( $query->have_posts() ) {
        $query->the_post();
        $custom             = get_post_custom($post->ID);
        $destaque_descricao    =   $custom["destaque_descricao"][0];
        $destaque_url          =   $custom["destaque_link"][0];
        $destaque_foto         =   $custom["destaque_foto"][0];
        // print_r($custom);

    ?>

        <div class="col-md-4" style="margin-bottom:20px;">
          <a href="<?php echo $destaque_url; ?>" style="text-decoration:none">
            <div class="news-v2-badge">
                <img class="img-responsive" style="width:100%;height: 227px;" src="<?php echo $destaque_foto; ?>" alt="">
                <p>
                    <img class="img-responsive" src="/wp-content/themes/2s/assets/img/simbol-2s-box.png" alt="">
                </p>
            </div>
            <div class="news-v2-desc2 post-border" id="post-border">
              <h2 style="margin-bottom:-50px"><?php echo $destaque_descricao; ?></strong></h2>
              <!-- <h2 class="entry-title" style="font-size: 19px;text-align: left;">
                  <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a>
              </h2> -->
            </div>
          </a>
        </div>

    <?php } } ?>
    </div>
</div>

<div class="container contato">
  <div class="row">
    <div class="col-sm-12">
      <div class="headline">
        <h2>Cadastre-se para ficar atualizado com a 2S</h2>
      </div>
      <div class="formulario">
        <?php echo do_shortcode('[contact-form-7 id="948" title="Cadastro Verticais"]'); ?>
      </div>
    </div>
  </div>
</div>

</div>
<?php the_content(); ?>
<?php get_footer(); ?>

<?php endwhile;?>
<?php endif ?>
