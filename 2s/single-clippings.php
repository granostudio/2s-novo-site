
<?php get_header(); ?>
    <!--=== Breadcrumbs v3 ===-->
    <div class="breadcrumbs breadcrumbs-light img-cases">
        <div class="container">
            <div class="titulos-breadcrumbs">
                <h1>Clipping</h1>
            </div>
        </div>
    </div>
    <!--=== End Breadcrumbs v3 ===-->

    <?php if (have_posts()): ?>
    <?php while ( have_posts() ) : the_post(); 
    ?>

    <!--=== Container Part ===-->
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                

                <div class="headline">
                    <h2><?php echo get_the_title(); ?></h2>
                </div>

                <div class="row cases">
                    <div style="float:left;">
                        <img class="img-responsive cases" src="<?php the_field("clipping_imagem"); ?>">
                    </div>
                    
                    <?php echo the_field("clipping_texto"); ?>

                </div>


            </div><!-- End col-9 -->
            <div class="col-md-3">
                <?php get_sidebar(); ?> 
            </div><!-- End col-3 -->

        </div> <!-- End row--> 
    </div>
    
    </div>
    <!--=== End Container Part ===-->
    <?php endwhile;?>
    <?php endif ?>
	
    <?php get_footer(); ?>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            App.init();
            StyleSwitcher.initStyleSwitcher();

            $("#formEdit").submit(function(event) {
                event.preventDefault(); 

                if($("#g-recaptcha-response").val()){
                     $("#resposta").html("<img src='/wp-content/themes/2s/assets/img/loader.gif' align='absmiddle'> Mensagem sendo enviada, por favor aguarde... ");

                     $.post("/wp-content/themes/2s/sendcontent.php", $("#formEdit").serialize(), function(data) {
                        $('#resposta').html(data);
                     }); 
                } else {
                    $("#resposta").html("<div class='panel-body'><div class='alert alert-danger fade in text-center' style='margin:0px;'><h4 style='margin:0px;'>Preencha os campos e marque o checkbox para acessar o conteúdo.</h4></div></div>");
                }
                return false;
            }); 
    });
    </script>
