<?php if (have_posts()): ?>
<?php while ( have_posts() ) : the_post(); ?>

<?php get_header(); ?>
<div class="pagina-nova">


  <header class="header-matalurgia">
    <h1>Mineração & Metalurgia<img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/verticais/Metalurgia.png">
    </h1>
  </header>

  <div class="video-background" style="position:relative" height="500px;" id="video-background-id">

    <video autoplay loop muted width="100%" id="video-banner">
      <source src="<?php echo get_stylesheet_directory_uri();?>/2S-Transformação-Digital-Metalurgia-e-Mineração.mp4" type="video/mp4">
    </video>


    <div class="btn-banner">
    </div>
    <div class="btn-banner2"><p style="text-align: center;color: white;font-size: 20px;padding-top: 7px;">Assista ao video</p></div>

  </div>

  <div class="container">
    <div class="row col-sm-10 col-sm-offset-1">
      <div class="col-sm-12" style="margin-top: 50px;">
        <p>
          <b>São muitos os desafios a que estão expostas as empresas de metalurgia e mineração - desaquecimento da economia, pressão por atividades mais sustentáveis e conciliação de diferentes interesses (governo, comunidades, agências reguladoras) são alguns deles. A transformação digital endereça grande parte dessas problemáticas, trazendo excelência operacional e ganhos de produtividade, além de contribuir para a criação de alternativas.
          </b>
        </p>

        <!-- Sessão problemas  -->
        <div class="headline" style="margin-top: 50px;margin-bottom: 20px;">
          <h2 style="font-size: 24px;">Problema</h2>
        </div>
        <p style="margin-bottom: 15px;">O setor de metalurgia e mineração enfrenta um dos períodos mais difíceis nos últimos 20 anos, em função da <font color="#2977BA">queda dos preços e da diminuição das margens de lucro.</font> Isso porque, sendo o Brasil um grande exportador de commodities, produtos com valor definido pelo mercado internacional, há queda de demanda em períodos de recessão e os preços ficam ainda mais reduzidos.</p>

        <p style="margin-bottom: 15px;">Além disso, a preocupação em torno do <font color="#2977BA">impacto ambiental</font> gerado pelas atividades de metalurgia e mineração é uma constante. Ao mesmo tempo em que a chamada quarta revolução industrial impulsiona o desenvolvimento e a inovação em diversos setores produtivos, o movimento de promoção à <font color="#2977BA">sustentabilidade e à criação de novas tecnologias</font> torna-se urgente.</p>

        <p style="margin-bottom: 15px;">A <font color="#2977BA">mudança no mix energético global</font> também pressiona as companhias do setor a diversificar a atuação.</p>

        <p>No primeiro trimestre de 2017, a indústria brasileira apresentou tímido crescimento de 0,9%, segundo o IBGE. Porém, as <font color="#2977BA">estimativas de aumento de produção e venda</font> pelos mercados automotivo, de construção civil e linha branca, por exemplo, criam expectativas de aquecimento para o setor. É o que mostra a edição de junho da revista ABM (Associação Brasileira de Metalurgia, Materiais e Mineração).</p>
      </div>
        <!-- Sessão problemas  -->

        <!-- Sessão soluções  -->
      <div class="col-sm-12">
        <div class="headline" style="margin-bottom: 20px;">
          <h2 style="font-size: 24px;">Soluções</h2>
        </div>
        <p>As soluções tecnológicas da 2S para a vertical de metalurgia e mineração foram pensadas a partir do entendimento dos principais pilares e desafios de negócio do setor, de forma a usar a <font color="#2977BA">tecnologia e a transformação digital</font> como meio para a retomada do crescimento.</p>
        <div class="col-sm-12 box-opcoes">
          <div class="item-p item1">
            <h4>Logística Interna<br/>Inteligente</h4>
            <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/papel-e-celulose/logistica.png" alt="">
          </div>
          <div class="item-p item2">
            <h4>Ambiente<br/>Seguro</h4>
            <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/verticais/ambienteseguro.png" alt="">
          </div>
          <div class="item-p item3">
            <h4>Rastreamento<br/>de Ativos</h4>
            <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/verticais/rastreamentoativos.png" alt="">
          </div>
          <div class="item-p item4">
            <h4>Back<br/>Office</h4>
            <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/papel-e-celulose/back-office.png" alt="">
          </div>
          <div class="content-out3 content-out4">
            <div class="content content1">
              <div class="triangulo"></div>
              <p>Há empresas do setor que recebem, diariamente, mais de 200 caminhões de terceiros em suas plantas, sem ter qualquer controle da movimentação desses veículos. Outras possuem um vasto parque de máquinas e equipamentos, mas que não estão corretamente dimensionados.</p>

              <p>Para dar mais inteligência e controle à logística interna, a 2S utiliza a solução AXIS, propondo a <font color="#2072B7">otimização do tempo e total controle da circulação de veículos</font>; a readequação de frota e a redução de custos com combustível e desgaste de pneus; o <font color="#2072B7">controle fim a fim</font> da extração dos minérios; e melhorias de diversos processos.</p>

              <p>Por meio da funcionalidade de despacho, por exemplo, é possível fazer toda a gestão dos veículos, <font color="#2072B7">reduzir as filas e tornar o transporte interno mais ágil e eficiente</font>. Nos processos de extração, a solução permite identificar e controlar o volume extraído X volume carregado X volume descarregado, <font color="#2072B7">reduzindo perdas e gerando informações em tempo real</font> para a produção.</p>

              <p>Ainda entre as <font color="#2072B7">melhorias de processo</font>, figuram a definição de rotinas, a otimização de ciclos, o gerenciamento de status, a geração de relatórios e KPIs em tempo real e a integração de processos de sistemas.</p>

            </div>
            <div class="content content2">
              <div class="triangulo"></div>
              <p>Dentro de uma área restrita, com altas temperaturas, por exemplo, é primordial mapear a <font color="#01B59D">localização dos colaboradores, o seu perfil e tempo de permanência</font> em áreas perigosas, além de verificar se está portando todos os equipamentos de proteção individual (EPIs) exigidos. </p>

              <p>Com a solução que batizamos de “Workplace Safety”, buscamos <font color="#01B59D">minimizar os riscos e acidentes de trabalho</font>, a partir da total visibilidade das operações, máquinas e pessoas, evitando o choque entre elas.</p>

              <p>A solução permite a liberação de catraca em tempo real, o <font color="#01B59D">bloqueio do acesso a pessoas que não estão portando os EPIs</font> necessários, a setorização dos locais de trabalho e, ainda, a emissão de alertas (visuais e sonoros) mediante a aproximação entre veículos e pessoas. </p>

            </div>
            <div class="content content3">
              <div class="triangulo"></div>
              <p>O controle de estoque e a movimentação do produto final, principalmente os de maior valor agregado, são preocupações, em especial, das empresas de metalurgia.</p>

              <p>Por isso, a 2S utiliza a tecnologia de RFID, identificação por radiofrequência, para a  <font color="#FFCB26">localização de equipamentos e ferramentas</font> que circulam dentro da planta, durante todo o percurso até seu destino final.</p>

              <p>Os benefícios incluem, ainda, <font color="#FFCB26">redução no tempo de inventário</font> e maior<font color="#FFCB26"> agilidade na identificação e coleta dos produtos</font> (picking).</p>

            </div>
            <div class="content content4">
              <div class="triangulo"></div>
              <p>Além das soluções de negócio, a 2S olha para dentro da casa do cliente, a fim de compor projetos para <font color="#FF5A45">otimização dos recursos internos, eficiência de processos e qualidade da tomada de decisão.</font> As soluções incluem:</p>

              <p><font color="#FF5A45">Engajamento de times,</font> a partir da criação de ambientes de trabalho colaborativos e flexíveis, treinamentos on demand e comunicação estreita entre gestores e times; eficiência dos processos de recrutamento e seleção; e mais segurança em ambientes restritos.</p>

              <p><font color="#FF5A45">Domínio do ambiente</font>, com soluções que otimizam processos, a utilização dos recursos e a disponibilidade de rede; permitem a visão, em tempo real, da variação de produtos e processos, o planejamento end-to-end e a colaboração entre áreas; entre outras funções.</p>

              <p><font color="#FF5A45">Ambientes mais seguros</font>, por meio de soluções de segurança que bloqueiam invasões e vazamento de informações; monitoram e gerenciam as ameaças, reduzindo custos e esforços; e garantem segurança à inovação, aos processos e às estratégias da empresa.</p>

            </div>
          </div>

        </div>
      </div>
      <!-- Sessão soluções  -->

       </div>

       <!-- Sessão Ganhos -->
       <div class="row col-sm-10 col-sm-offset-1">
       <div class="col-sm-12">
         <div class="headline">
           <h2 style="font-size:24px;">Ganhos</h2>
         </div>
       </div>
       <div class="col-sm-6">
         <ul class="lista">
           <li>Criação de empresas digitais, que <font color="#2977BA">tomam melhores decisões e se diferenciam dos concorrentes</font>, a partir de informações em tempo real;</li>
           <li>Maior <font color="#075557">previsibilidade ao negócio</font>, a partir da geração de informações, em tempo real, de todos os processos produtivos, do plantio à extração da matéria-prima e fabricação do produto final;</li>
           <li><font color="#01B59D">Otimização do tempo e da utilização dos veículos</font>, próprios e de terceiros, que transitam na planta, contribuindo para a criação de <font color="#01B59D">ambientes seguros</font>, a redução de acidentes de trabalho e a <font color="#01B59D">melhor gestão da frota</font>;</li>
           <li>Mobilidade e dinamismo, promovendo <font color="#FFCB26">interação entre áreas e plantas</font>;</li>
         </ul>
       </div>
       <div class="col-sm-6">
         <ul class="lista lista-metalurgia">
           <li class="paragrafo1">Redução de turnover e de custos com viagens, recrutamento e treinamento, além de maior <font color="#FF7C2F">transparência na comunicação </font>com o time;</li>
           <li class="paragrafo2"><font color="#FFCB26">Aumento do desempenho das equipes</font>, a partir da realização de treinamentos sob demanda para cada perfil de colaborador;</li>
           <li><font color="#01B59D">Proteção a ativos e à propriedade intelectual</font> da empresa, com visibilidade da rede e controle de acessos, contribuindo, assim, para a continuidade do negócio e a construção de uma boa reputação interna e externa.</li>
         </ul>
       </div>
       <!-- Sessão Ganhos -->

       <!-- <div class="col-sm-12 video-titulo">
         <p style="font-size:16px;"> <strong>Assista ao vídeo a seguir e saiba mais sobre como a tecnologia auxilia a área de papel e celulose</strong> </p>
       </div>
       <div class="col-sm-6">
         <div class="myIframe">
           <iframe src="//www.youtube.com/embed/VY1DMCx4XLg#action=share" allowfullscreen></iframe>
         </div>
       </div>
       <div class="col-sm-6 video-lista">

         <ul>
           <p style="font-size:16px;"><strong>Esse vídeo fala sobre:</strong></p>
           <li>Tecnologia + Marketing</li>
           <li>Conexão com clientes</li>
           <li>Transformação Digital</li>
           <li>Varejo Inovador</li>
           <li>Infraestrutura de TI</li>
           <li>Atendimento personalizado</li>
         </ul>
         <div style="clear:both;"></div>
       </div> -->


       <!-- <div class="col-sm-12 video-saibamais">
         <p style="font-size:16px;"><strong>Saiba mais</strong></p>
         <ul>
           <li>
             <div class="row">
               <div class="col-xs-8"><a href="http://www.2s.com.br/conteudo/e-book-transformacao-digital-o-futuro-das-empresas-e-as-empresas-do-futuro/" target="_blank">Ebook exclusivo: transformação digital e o varejo conectado</a></div>
               <div class="col-xs-4"><a href="" class="btn">Download</a></div>
             </div>
             </li>
           <li>
             <div class="row">
               <div class="col-xs-8"><a href="http://www.2s.com.br/transformacao-digital-3-frentes-de-atuacao-no-varejo/" target="_blank">Transformação digital: 3 frentes de atuação no varejo</a></div>
             </div>
           </li>
         </ul>
       </div> -->
     </div>

  </div>

  <script>
    jQuery(' .item1 ').click(function() {
      $( ".content1" ).fadeIn( "slow" );
      $( ".content2" ).fadeOut( "slow" );
      $( ".content3" ).fadeOut( "slow" );
      $( ".content4" ).fadeOut( "slow" );
      $( ".content5" ).fadeOut( "slow" );
    });
    jQuery(' .item2 ').click(function() {
      $( ".content1" ).fadeOut( "slow" );
      $( ".content2" ).fadeIn( "slow" );
      $( ".content3" ).fadeOut( "slow" );
      $( ".content4" ).fadeOut( "slow" );
      $( ".content5" ).fadeOut( "slow" );
    });
    jQuery(' .item3 ').click(function() {
      $( ".content1" ).fadeOut( "slow" );
      $( ".content2" ).fadeOut( "slow" );
      $( ".content3" ).fadeIn( "slow" );
      $( ".content4" ).fadeOut( "slow" );
      $( ".content5" ).fadeOut( "slow" );
    });
    jQuery(' .item4 ').click(function() {
      $( ".content1" ).fadeOut( "slow" );
      $( ".content2" ).fadeOut( "slow" );
      $( ".content3" ).fadeOut( "slow" );
      $( ".content4" ).fadeIn( "slow" );
      $( ".content5" ).fadeOut( "slow" );
    });
    jQuery(' .item5 ').click(function() {
      $( ".content1" ).fadeOut( "slow" );
      $( ".content2" ).fadeOut( "slow" );
      $( ".content3" ).fadeOut( "slow" );
      $( ".content4" ).fadeOut( "slow" );
      $( ".content5" ).fadeIn( "slow" );
    });

    jQuery(' .btn-banner2 ').click(function() {
      $( ".btn-banner" ).remove();
      $( ".btn-banner2" ).remove();
      document.getElementById("video-banner").controls = true;
      document.getElementById("video-banner").currentTime = 0;
      document.getElementById("video-banner").muted = false;

    });
  </script>

  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="relatedposts">
          <h1 class="tit7 text-center" style="margin-top:30px; margin-bottom:30px">Artigos Relacionados</h1>
        </div>
      </div>
    </div>
  </div>

  <div class="container">

      <div class="margin-bottom-60"></div>

      <?php
      $query= null;
      $query = new WP_Query(
      array(
      'post_type' => 'destaque',
      'posts_per_page' => 6)
      );
      ?>

      <div class="row news-v2 margin-bottom-80">

      <?php
      if ( $query->have_posts() ) {
          while ( $query->have_posts() ) {
          $query->the_post();
          $custom             = get_post_custom($post->ID);
          $destaque_descricao    =   $custom["destaque_descricao"][0];
          $destaque_url          =   $custom["destaque_link"][0];
          $destaque_foto         =   $custom["destaque_foto"][0];
          // print_r($custom);

      ?>

          <div class="col-md-4" style="margin-bottom:20px;">
            <a href="<?php echo $destaque_url; ?>" style="text-decoration:none">
              <div class="news-v2-badge">
                  <img class="img-responsive" style="width:100%;height: 227px;" src="<?php echo $destaque_foto; ?>" alt="">
                  <p>
                      <img class="img-responsive" src="/wp-content/themes/2s/assets/img/simbol-2s-box.png" alt="">
                  </p>
              </div>
              <div class="news-v2-desc2 post-border" id="post-border">
                <h2 style="margin-bottom:-50px"><?php echo $destaque_descricao; ?></strong></h2>
                <!-- <h2 class="entry-title" style="font-size: 19px;text-align: left;">
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a>
                </h2> -->
              </div>
            </a>
          </div>

      <?php } } ?>
      </div>
  </div>

  <div class="container contato">
    <div class="row">
      <div class="col-sm-12">
        <div class="headline">
          <h2>Cadastre-se para ficar atualizado com a 2S</h2>
        </div>
        <div class="formulario">
          <?php echo do_shortcode('[contact-form-7 id="948" title="Cadastro Verticais"]'); ?>
        </div>
      </div>
    </div>
  </div>

</div>


<?php the_content(); ?>
<?php get_footer(); ?>

<?php endwhile;?>
<?php endif ?>

<style>
ul.lista-metalurgia .paragrafo1:before{
  background-color: #FF7C2F !important;
}
ul.lista-metalurgia .paragrafo2:before{
  background-color: #FFCB26 !important;
}
.content-out4 .content1{  
     border-bottom: 2px solid #2072B7; 
}
.content-out4 .content2{
     border-bottom: 2px solid #01B59D; 
}
.content-out4 .content3{
     border-bottom: 2px solid #FFCB26; 
}
.content-out4 .content4{
     border-bottom: 2px solid #FF5A45; 
}
.header-matalurgia h1:before{
  background-color: #FFCB26;
  width: 270px;
  margin-left: -133px;
}
.header-matalurgia h1{
  color: #FFCB26;
}
@media(max-width: 414px){
  .header-matalurgia h1 img{
    margin-top: 80px;
    left: 50%;
    margin-left: -40px;
    }  
}

</style>