<?php
/**
* Category Page
*/

get_header(); ?>


    <div class="banner-home">
      <p>A transformação digital é agora - e para TODOS! Saiba como a tecnologia aplicada ao seu negócio pode aumentar a competitividade e acelerar o crescimento de sua empresa.
      </p>
      <a href="http://www.2s.com.br/contato/"><button type="button" name="button">Fale com a 2S</button></a>
    </div>

  <div id="inicio-posts" class="home-posts">

    <div class="container">

      <!-- MENU CATEGORIAS -->
      <?php  include get_template_directory(). '/menu-categorias.php'; ?>

      <?php
// Check if there are any posts to display
if ( have_posts() ) : ?>
      <div class="headline">
        <h2 style="font-size:25px"><?php single_cat_title(); ?></h2>
      </div>
      <div class="row news-v2 margin-bottom-60">
      <?php while ( have_posts() ) : the_post(); ?>
        <!-- .post -->

          <div class="col-md-4" style="margin-bottom:20px;">
            <a href="<?php the_permalink(); ?>" style="text-decoration:none">
              <div class="news-v2-badge">
                    <div class="img-responsive" style="height: 227px;">
                      <?php if (has_post_thumbnail()): ?>
                        <?php the_post_thumbnail( 'blog-thumb' ); ?>
                      <?php endif ?>
                    </div>
                  <!-- </a> -->
                  <p>
                      <img class="img-responsive" src="/wp-content/themes/2s/assets/img/simbol-2s-box.png" alt="">
                  </p>
              </div>
              <!-- <a href="<?php the_permalink(); ?>"> -->
                <div class="news-v2-desc2 post-border" id="post-border">
                  <h2 class="entry-title" style="font-size: 19px;text-align: left;">
                      <?php the_title(); ?>
                  </h2>
                  <p>
                    <?php the_excerpt_max_charlength(70); ?></strong>
                  </p>
                </div>
                </a>
          </div>

        <!-- .post -->

      <?php endwhile; ?>
</div>
    <?php else:?>

      <p>Desculpe, nenhum conteúdo no momento :( </br></br></br></br></br></br></p>


    <?php endif; ?>

    </div>

    <?php get_footer(); ?>
  </div>


<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
        LayerSlider.initLayerSlider();
        StyleSwitcher.initStyleSwitcher();
        OwlCarousel.initOwlCarousel();
        OwlRecentWorks.initOwlRecentWorksV2();
    });
</script>
