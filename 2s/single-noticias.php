
<?php get_header(); ?>
    <!--=== Breadcrumbs v3 ===-->
    <div class="breadcrumbs breadcrumbs-light img-cases">
        <div class="container">
            <div class="titulos-breadcrumbs">
                <h1>Notícias</h1>
            </div>
        </div>
    </div>
    <!--=== End Breadcrumbs v3 ===-->

    <?php if (have_posts()): ?>
    <?php while ( have_posts() ) : the_post(); 
    ?>

    <!--=== Container Part ===-->
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                

                <div class="headline">
                    <h2><?php echo get_the_title(); ?></h2>
                </div>

                <div class="row cases">
                    <div style="float:left;">
                        <img class="img-responsive cases" src="<?php the_field("noticia_imagem"); ?>">
                    </div>
                    
                    <?php echo the_field("noticia_texto"); ?>

                </div>
                <div class="row text-center" style="margin-top:20px;">
                    <a href="<?php echo the_field("noticia_titulo"); ?>" target="_blank" class="btn btn-lg btn-primary btn-u-sm">Visite a notícia na íntegra</a>
                </div>


            </div><!-- End col-9 -->
            <div class="col-md-3">
                <?php get_sidebar(); ?> 
            </div><!-- End col-3 -->

        </div> <!-- End row--> 
    </div>
    
    </div>
    <!--=== End Container Part ===-->
    <?php endwhile;?>
    <?php endif ?>
	
    <?php get_footer(); ?>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            App.init();
            StyleSwitcher.initStyleSwitcher();
    });
    </script>
