
<?php get_header(); ?>
    <!--=== Breadcrumbs v3 ===-->
    <div class="breadcrumbs breadcrumbs-light img-cases">
        <div class="container">
            <div class="titulos-breadcrumbs">
                <h1>Cases</h1>
            </div>
        </div>
    </div>
    <!--=== End Breadcrumbs v3 ===-->

    <?php if (have_posts()): ?>
    <?php while ( have_posts() ) : the_post(); 
    ?>

    <!--=== Container Part ===-->
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                

                <div class="headline">
                    <h2><?php the_field("cases_titulo_case"); ?></h2>
                </div>

                <div class="row cases">
                    <div style="float:left;">
                        <img class="img-responsive cases" src="<?php the_field("cases_foto"); ?>">
                        <h5><small><?php the_field("cases_texto_legenda"); ?></small></h5>
                    </div>
                    
                    <?php the_content(); ?>

                    <br/>
                    <br/>
                    <div class="margin-bottom-40"></div>
                    <a href="/cases" class="btn btn-primary" title="Voltar para página de Cases">Voltar para página de Cases</a>
                    <div class="margin-bottom-40"></div>
                </div>


            </div><!-- End col-9 -->
            <div class="col-md-3">
                <?php get_sidebar(); ?> 
            </div><!-- End col-3 -->

        </div> <!-- End row--> 
    </div>
    
    </div>
    <!--=== End Container Part ===-->
    <?php endwhile;?>
    <?php endif ?>

			
<?php get_footer(); ?>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
        StyleSwitcher.initStyleSwitcher();
    });
</script>
